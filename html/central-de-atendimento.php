  <?php include('header.php') ?>

  <!-- CENTRO -->
  <div class="centralizando content">

    <div class="comofunciona">

  		<h1>Ajuda</h1>
       
  		<p>Veja aqui todos os nossos tópicos e artigos que podem te ajudar a solucionar dúvidas e problemas!</p>

      <br /><br />
  		<strong>Dúvidas Gerais</strong>
      <br /><br />

      <strong class="accordion">• Por que preciso cadastrar meus dados pessoais/empresariais para criar uma loja?</strong>
      <div class="accordion">
        <p>A VIsP precisa ter em seu banco de dados todas as informações necessárias para a realização de atividades administrativas, por motivos legais e contábeis.<br />
        Também é uma forma de proteger nosos clientes e garantir que podemos oferecer a melhor assistência possível.<br />
        Não se preocupe em fornecer seus dados: eles não serão divulgados publicamente.<br />
        Caso tenha qualquer dúvida, entre em contato conosco pelo email: contato@vispstore.com</p>
      </div>

      <br />

      <strong class="accordion">• Como inserir/alterar minha foto?</strong>
      <div class="accordion">
        <p>Ao fazer o login no site, você entrará no ambiente do usuário, chamado de “Minha Vitrine VIsP”. Nesse ambiente, você deve clicar em “Meus Dados”. Caso ainda não tenha inserido a sua foto, haverá o desenho do menininho ou da menininha da VIsP. Para inserir ou alterar a foto, basta clicar nesse desenho ou da sua foto, caso já tenha sido inserida.</p>
      </div>

      <br />

      <strong class="accordion">• Como é calculado a comissão da VIsP?</strong>
      <div class="accordion">
        <p>A VIsP só ganha quando você também ganha: a comissão da VIsP é 15% do valor do produto, sem o frete e a taxa do Moip, que é nosso  intermediário financeiro.</p>
      </div>

      <br />

      <strong class="accordion">• Como é calculado o preço final do produto?</strong>
      <div class="accordion">
        <p>O preço final do produto é a soma do preço determinado pelo vendedor + frete + a comissão VIsP.<br />
        Além do preço do produto, há uma taxa de comissão do moip, que é o parceiro financeiro da VIsP e garantirá total segurança dos dados do comprador. <br />
        <br /><br />
        Por exemplo: se você quer vender um produto e conseguir R$100, o produto custará ao comprador R$100 (preço do vendedor) + R$15 (comissão VIsP)  + a taxa Moip + frete.<br /> 
        Se o comprador optar por pagar pelo cartão de crédito, a taxa Moip é de 4,90% + R$0,69 por transação. Caso opte por boleto ou transferência bancária, é de 2,90% + R$0,69 por transação.</p>
      </div>

      <br />

      <strong class="accordion">• Esqueci a minha senha. O que devo fazer?</strong>
      <div class="accordion">
        <p>No ambiente do login, clique em “esqueci a minha senha”. Você deverá informar o seu CPF para recuperá-la. </p>
      </div>

      <br /><br />
      <strong>Quero Vender</strong>
      <br /><br />

      <strong class="accordion">• Há alguma taxa para criar uma Vitrine com produtos para vender?</strong>
      <div class="accordion">
        <p>Não há taxa para começar a vender. A VIsP  só ganha a comissão de 15% no ato da venda do seu produto. O cadastro é feito gratuitamente.</p>
      </div>

      <br />

      <strong class="accordion">• Como cadastrar os produtos para venda?</strong>
      <div class="accordion">
        <p>Antes de tudo, você precisa:<br />

        - Criar a sua vitrine na VIsP – para isso, você precisa estar logado, deve clicar em seu nome ( alto à direita) e então em “Minha Vitrine VIsP”;<br />
        - Criar a sua conta no Moip, caso ainda não tenha.
        <br />
        Após isso, para cadastrar um novo produto, você deve, dentro da sua página de usuário, acessar o link “Inserir produtos”. 
      </p>
      </div>

      <br />

      <strong class="accordion">• Como alterar informações de produtos que já estão na vitrine?</strong>
      <div class="accordion">
        <p> Vá até o ambiente “Minha Vitrine VIsP” e clique em “Minhas Vendas”. Lá você poderá realizar as seguintes alterações: o preço do produto e a descrição. Você também poderá excluir o produto de sua vitrine.</p>
      </div>

      <br />

      <strong class="accordion">• Como ter os meus produtos em destaque na home?</strong>
      <div class="accordion">
        <p>Todos os produtos, ao serem inseridos no site, passam por uma inspeção. Se ele for bem bacana e estiver com um preço especial, certamente será selecionado e terá um destaque especial.</p>
      </div>

      <br />

      <strong class="accordion">• Como funcionam as avaliações?</strong>
      <div class="accordion">
        <p>Após cada venda finalizada o comprador deverá avaliar, através da quantidade de estrelas, o vendedor. A avaliação informada em cada loja é a média das avaliações sobre o vendedor, feitas por cada comprador.<br/><br/>
          <u>Por exemplo:</u> se o comprador ficar muito satisfeito com a venda, ele avaliará o vendedor com 5 estrelas. Caso tenha ficado apenas satisfeito, com 4 estrelas; e assim por diante. Esse sistema de avaliação é importante para estimular e dar segurança ao comprador; por isso é importante que o vendedor esteja sempre preocupado em realizar uma venda com qualidade.
        </p>
      </div>

      <br />

      <strong class="accordion">• Como é feito o pagamento ao vendedor?</strong>
      <div class="accordion">
        <p>A compra só é realizada se o vendedor tiver cadastro no Moip (site.moip.com.br). O Moip, então, encaminha o pagamento do comprador à conta do vendedor. Caso o comprador informe à VIsP o não recebimento de um produto no prazo de 30 dias, o pagamento é adiado ao vendedor até pelo menos os próximos 30 dias. Caso o comprador não se pronuncie com relação à entrega, o pagamento ao vendedor será feito automaticamente pelo Moip em 30 dias corridos após a venda.</p>
      </div>

      <br />

      <strong class="accordion">• Qual é o prazo para o pagamento do vendedor?</strong>
      <div class="accordion">
        <p>O pagamento é realizado 30 dias corridos após a venda, caso não haja reclamação do comprador sobre o não recebimento do produto. Por isso, guarde sempre os comprovantes de entrega, pois a apresentação deles pode ser necessária.</p>
      </div>

      <br />

      <strong class="accordion">• Após a conclusão da venda, qual é o prazo do vendedor para enviar o produto pelos Correios?</strong>
      <div class="accordion">
        <p>O vendedor deve enviar o produto o mais rápido possível, porque, caso o comprador não o receba em até 30 dias após a venda, o pagamento será bloqueado. </p>
      </div>

      <br />

      <strong class="accordion">• O que o vendedor deve fazer após entregar os produtos nos correios?</strong>
      <div class="accordion">
        <p>O vendedor deve acessar no ambiente “Minha Vitrine VIsP”  o link de “Minhas Vendas” . Lá, ao lado de cado produto, há um ícone em formato de caminhão onde deverão ser inseridas as informações da data de envio e o número de rastreamento do produto dos correios.<br />
        O vendedor não deve se esquecer de que quanto mais eficiente for a venda, maior será a sua avaliação e assim mais facilmente ele venderá os produtos na VIsP.</p>
      </div>

      <br />

      <strong class="accordion">• Não consigo inserir a foto do produto.</strong>
      <div class="accordion">
        <p>Verifique se a imagem que você está tentando enviar segue os requisitos abaixo:<br />
        1. Deve estar no formato JPEG (.jpg).<br />
        2. Para as imagens de produto, o tamanho mínimo é de 300 px em ambas as dimensões.<br />
        3. O tamanho de arquivo máximo permitido é de 2 MB por imagem.<br />
        Para diminuir suas fotos, você pode usar:<br />
        www.reduzfoto.com.br<br />
        www.scrapee.net/redimensionar-fotos.htm<br />
        www.reduzirfotos.com
        </p>
      </div>

      <br />

      <strong class="accordion">• Tenho 20 produtos para vender mas estou com preguiça de inserir um a um na minha vitrine VIsP...</strong>
      <div class="accordion">
        <p>Se você tiver 20 ou mais produtos para inserir e está com preguiça, entre em contato conosco através do email preguicinha@vispstore.com que te ajudaremos ;)</p>
      </div>

      <br /><br />
      <strong>Quero Comprar</strong>
      <br /><br />

      <strong class="accordion">• Comprei um produto há 30 dias atrás e ainda não o recebi.</strong>
      <div class="accordion">
        <p>Nesse caso, confirme que a compra foi realizada há 30 dias. Caso sim, você precisa
        sinalizar o não recebimento: vá até o ambiente “Minhas Compras” e clique no ícone
        de mão para baixo. Dessa forma, o pagamento do vendedor será bloqueado.</p>
      </div>

      <br />

      <strong class="accordion">• Qual é o prazo de devolução do produto?</strong>
      <div class="accordion">
        <p>Caso você tenha se arrependido da compra, tem até 7 dias para devolvê-lo. Para isso, deve entrar em contato com a VIsP através do email querodevolver@vispstore.com. É importante ressaltar que o comprador arcará com os custos de frete do produto devolvido.</p>
      </div>

      <br />

      <strong class="accordion">• Como é feito o pagamento ao vendedor?</strong>
      <div class="accordion">
        <p>A compra só é realizada se o vendedor tiver cadastro no Moip (site.moip.com.br). O Moip, então, encaminha o pagamento do comprador à conta do vendedor. Por isso, é ESSENCIAL que o comprador informe à VIsP o não recebimento de um produto, para que assim possamos adiar o pagamento ao vendedor. Caso o comprador não se pronuncie com relação à entrega, o pagamento ao vendedor será feito automaticamente pelo Moip em 30 dias corridos após a venda.</p>
      </div>

      <br /><br />
      <strong>Frete</strong>
      <br /><br />

      <strong class="accordion">• Quais são as opções de frete?</strong>
      <div class="accordion">
        <p>Quando o vendedor cadastrar um produto para a venda, poderá optar por 3 tipos de frete:<br />
          Frete Grátis: o vendedor se compromete em pagar o frete;<br />
          Frete via Correios: o preço é enviado pelos Correios pelo vendedor e o comprador paga o valor, que é calculado de acordo com o peso do produto (peso pena, leve, médio e pesado) e o CEP do comprador;<br />
          Frete a combinar: o vendedor seleciona essa opção se aceita combinar com o comprador a entrega do produto pessoalmente.
        </p>
      </div>

      <br />

      <strong class="accordion">• O valor do frete dos Correios não é calculado. O que está acontecendo?</strong>
      <div class="accordion">
        <p>A VIsP  utiliza o sistema que os Correios disponibilizam para realizar o cálculo do frete baseado no peso do pedido e os CEPs do remetente e do destinatário. Sendo assim, a VIsP  não tem controle sobre o seu funcionamento.<br />
        Caso isso aconteça, recomendamos que o comprador adicione o produto na lista de favoritos e espere um pouco para finalizar a comprar.</p>
      </div>

      <br />

      <strong class="accordion">• Como funciona o cálculo de frete pelos Correios?</strong>
      <div class="accordion">
        <p>Para calcular o preço do frete do produto, o sistema dos Correios utiliza a distância origem/destino, o peso e as dimensões do objeto, aplicando a seguinte fórmula: (Comprimento x Altura x Largura)/6000cm³. O preço adotado pelo sistema será o maior entre o peso cúbico e peso real.<br />
        Veja abaixo os limites de dimensões e pesos, de acordo com o site dos Correios:<br />
        O peso máximo admitido para encomendas SEDEX, SEDEX 10, e-SEDEX, SEDEX a cobrar, PAC e Reembolso Postal é de 30 kg.<br />
        Para o SEDEX Hoje, o peso máximo admitido é 10 kg.<br />
        Para o serviço de encomenda PAC os limites máximos de peso são:<br />
        30 kg – remessa individual em âmbito nacional;<br />
        50 kg – remessa individual em nível estadual, entre localidades previstas em contrato;<br />
        600 kg - remessa agrupada em âmbito nacional.<br />
        As dimensões mínimas e máximas admitidas para os serviços de encomendas SEDEX, e-SEDEX, PAC e Reembolso Postal são:<br />
        Caso queira mais informações, acesse o site dos Correios: correios.com.br
        </p>
      </div>

      <br />

      <strong class="accordion">• Como funciona o cálculo do prazo de entrega dos Correios?</strong>
      <div class="accordion">
        <p>Os prazos de entrega são calculados a partir do primeiro dia útil seguinte ao da postagem e variam de acordo com as localidades de origem e de destino do produto.<br />
          Caso queira mais informações, acesse o site dos Correios: correios.com.br
        </p>
      </div>

      <br />

      <strong class="accordion">• Como o vendedor informa a data de envio do produto e o número de rastreamento?</strong>
      <div class="accordion">
        <p>Vá até o ambiente “Minha Vitrine VIsP” e clique em “Minhas Vendas”.  Lá, ao lado de cada produto, você verá um ícone de caminhão, que, ao ser clicado, você poderá inserir a data de envio e o número de rastreamento do produto. <br />
          Esse número de rastreamento será enviado para o email cadastrado do comprador.
        </p>
      </div>

      <br /><br />
      <strong>Sobre o Moip</strong>
      <br /><br />

      <strong class="accordion">• Por que é necessário ter conta no Moip para vender na VIsP?</strong>
      <div class="accordion">
        <p>Sabemos que muitas pessoas não se sentem à vontade para inserir dados financeiros em sites da internet; o que é uma postura muito correta. Por isso, a VIsP escolheu como intermediador financeiro o Moip, que possui a certificação máxima do PCI-DSS, a principal certificação de segurança da indústria de pagamentos, garantindo total segurança dos seus dados. </p>
      </div>

      <br />

      <strong class="accordion">• Quanto é a taxa cobrada pelo Moip?</strong>
      <div class="accordion">
        <p>Caso sua venda seja feita por Cartão de Crédito será cobrada uma tarifa de 4,90% + R$ 0,69 por transação.<br />
Caso sua venda seja realizada via Boleto Bancário ou Transferência Bancária, será cobrada uma tarifa de 2,90% + R$ 0,69 por transação.</p>
      </div>

      <br />

      <strong class="accordion">• O que é e o que significa uma reclamação?</strong>
      <div class="accordion">
        <p>A reclamação ocorre quando um comprador questiona, utilizando a Central de Resolução do Moip, o não recebimento ou a não conformidade de um produto recebido. A abertura da reclamação significa um pedido de ajuda do comprador ao vendedor para a resolução de determinado problema na transação comercial. A Central de Resolução permite que ambas as partes se comuniquem diretamente até que o problema seja resolvido.<br />
        Caso as partes não cheguem a uma solução, a reclamação pode ser transformada em uma disputa. Neste caso, o comprador ou o vendedor pedem a interferência do Moip na resolução do problema. Neste ponto o Moip entra em contato com ambas as partes, coleta as informações necessárias (comprovantes de pagamento, comprovantes de postagem e outros) e encerra a disputa idoneamente em favor de uma das partes.</p>
      </div>

      <br />

      <strong class="accordion">• Qual o prazo para a abertura da reclamação?</strong>
      <div class="accordion">
        <p>A reclamação pode ser aberta até 45 dias após a aprovação do pagamento.</p>
      </div>

      <br />

      <strong class="accordion">• O que é e o que significa uma disputa?</strong>
      <div class="accordion">
        <p>A disputa é o segundo passo do processo de resolução de conflitos. Caso o conflito não seja resolvido na etapa de reclamação, o comprador pode transformá-la em uma disputa.</p>
      </div>

      <br />

      <strong class="accordion">• Em quais situações o comprador é coberto pelo Compra Protegida?</strong>
      <div class="accordion">
        <p>i) o comprador não recebe o produto; ou<br />
        ii) o comprador recebe o produto, porém o mesmo apresenta-se significativamente diferente da descrição apresentada pelo vendedor (item não conforme).</p>
      </div>

      <br />

      <strong class="accordion">• O que é considerado um item com não conformidades?</strong>
      <div class="accordion">
        <p>Um item não conforme é um item que apresenta diferenças materiais significativas em relação ao que foi descrito pelo vendedor em sua Vitrine da VIsP. O item pode ser considerado não conforme quando trata-se de:<br />
          Versão ou edição errada.<br />
          Condição do produto (usado ou novo) ou detalhes do produto diferentes da descrição.<br />
          Produto errado.<br />
          Peças ou componentes faltantes.<br />
          Produto com defeito.<br />
          Produto danificado
        </p>
      </div>

      <br />

      <strong class="accordion">• Um item não pode ser considerado não conforme quando:</strong>
      <div class="accordion">
        <p>O defeito apresentado no produto é descrito corretamente pelo vendedor.<br />
        O produto foi corretamente descrito, mas você desistiu da compra depois de tê-lo recebido.<br />
        O produto foi corretamente descrito, mas não atendeu as suas expectativas.
        </p>
      </div>

      <br />

      <strong class="accordion">• Quais são os requisitos para cobertura?</strong>
      <div class="accordion">
        <p>O produto deve ter sido pago por meio de uma única transação. Transações realizadas por meio de múltiplos pagamentos não serão elegíveis ao programa.<br />
        O comprador deve ter iniciado o Processo de Reclamação e Disputa dentro de 45 dias corridos contatos a partir da data do pagamento.<br />
        A compra deve conter apenas produtos tangíveis. Serviços e produtos intangíveis não são elegíveis ao programa.<br />
        O comprador deve fornecer detalhadamente todas as informações requisitadas pelo Moip durante o período de mediação.
        </p>
      </div>

      <br />

      <strong class="accordion">• Quais são as limitações do programa?</strong>
      <div class="accordion">
        <p>Somente será coberta uma transação a cada 12 meses (por usuário). O prazo de 12 meses para que um comprador possa iniciar a requisição é contado a partir da data da efetivação do ressarcimento.<br />
        O programa somente se aplica a transações que envolvam produtos tangíveis.<br />
        O valor do ressarcimento limita-se exclusivamente ao valor do produto comprado e não inclui gastos adicionais (frete, embalagem, despesas de envio e outros).<br />
        O programa de Compra Protegida se aplica apenas aos casos de produtos não recebidos e de recebimento de produtos significativamente distintos da descrição fornecida pelo vendedor. O comprovante de entrega da mercadoria apresentado pelo vendedor poderá ser considerado prova suficiente para que o processo de resolução de reclamações e disputa seja encerrado em favor do vendedor. Os casos em que os produtos tenham sidos danificados ou extraviados pela transportadora, ou por terceiros, não serão cobertos pelo programa.</p>
      </div>

      <br />

      <strong class="accordion">• Qual o valor da cobertura?</strong>
      <div class="accordion">
        <p>Compradores que realizem o pagamento de suas compras por meio do Moip na VIsP poderão ser ressarcidos, caso a transação seja elegível ao programa, em até R$ 1.000,00 (mil reais).</p>
      </div>

      <br />

      <strong class="accordion">• Como iniciar uma reclamação?</strong>
      <div class="accordion">
        <p>Para iniciar o processo de reclamação, pré-requisito para a qualificação da transação no programa de Compra Protegida, o comprador deve acessar a “Central de resolução” na área interna de sua Conta Moip e seguir o procedimento descrito. Também é possível abrir a reclamação diretamente na página de “mais detalhes” da transação clicando em “Problemas com esta transação?”.</p>
      </div>

      <br />

      <strong class="accordion">• Como acompanhar o processo de resolução?</strong>
      <div class="accordion">
        <p>Todo o acompanhamento deve ser realizado por meio da Central de Resolução disponível no ambiente interno de sua Conta Moip.</p>
      </div>

      <br />

      <strong class="accordion">• Todas as transações realizadas por meio do MoIP estão cobertas pelo programa de Compra Protegida?</strong>
      <div class="accordion">
        <p>Sim. Se a sua transação atender aos requisitos apresentados acima, você está protegido!</p>
      </div>

      <br />

      <strong class="accordion">• Qual o custo do serviço de Compra Protegida?</strong>
      <div class="accordion">
        <p>Não há custos para os compradores.</p>
      </div>

      <br /><br />
      <strong>Ainda precisa de Ajuda? <a href="contato">Clique Aqui</a> e te damos uma mão!</strong>
      
    	
    </div>

    <script type="text/javascript">
    //PODE COLOCAR ESSE SCRIPT NA FOLHA COMMON.
    $(document).ready(function(){
      $('strong.accordion').click(function(){
        $(this).next('div.accordion').slideToggle("slow");
      });
    });
    </script>

    <style type="text/css">
    strong.accordion{ cursor: pointer; }
    div.accordion{ display: none; }
    </style>


  <?php include('footer.php') ?>
