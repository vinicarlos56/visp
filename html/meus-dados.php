  <?php include('header.php') ?>

  <!-- CENTRO -->
  <div class="centralizando content minhavitrine meusdados">

    <h2>MINHA VITRINE VIsP</h2>

    <?php include('barra-de-vitrine.php') ?>

    <?php include('sidebar-minha-vitrine.php') ?>

    <!-- RIGHT -->
    <div id="right">
      <h1>MEUS DADOS</h1>

      <form>

        <input type="text" class="nome" id="nome" name="nome" placeholder="Gustavo Roma" />

        <div class="envolveasinputs">
          <input type="text" class="datadenascimento" id="datadenascimento" name="datadenascimento" placeholder="28/02/1980" />

          <div class="mascaraselectsexo">Masculino</div>
          <select name="sexo" class="sexo" id="sexo">
            <option></option>
            <option value="Masculino" selected>Masculino</option>
            <option value="Feminino">Feminino</option>
          </select>

          <label class="cpf">CPF - 059.030.285-21</label>
        </div>

        <div class="envolveasinputs">
          <input type="text" name="telefone" id="telefone" class="telefone" placeholder="(21) 2580-0000">

          <input type="text" name="celular" id="celular" class="celular" placeholder="(21) 8222-0000">
        </div>

        <div class="envolveasinputs">
          <input type="text" name="cep" id="cep" class="cep" placeholder="20771-400">

          <input type="text" name="endereco" id="endereco" class="endereco" placeholder="Avenida Barata Ribeiro">
        </div>

        <div class="envolveasinputs">
          <input type="text" name="numero" id="numero" class="numero" placeholder="100" onkeypress="return SomenteNumero(event)">

          <input type="text" name="complemento" id="complemento" class="complemento" placeholder="Apartamento 101">

          <input type="text" name="bairro" id="bairro" class="bairro" placeholder="Copacabana">
        </div>

        <div class="envolveasinputs">
          <div class="mascaraselectuf">RJ</div>
          <select name="uf" class="uf" id="uf"></select>
          <div class="mascaraselectcidade">Rio de Janeiro</div>
          <select name="cidade" class="cidade" id="cidade"></select>
        </div>
      
        <div class="filhos">
          <h1>FILHOS (AS)</h1>

          <div class="mensagemtabela"></div>

          <table cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td align="right" colspan="5"><input type="button" id="add" alt="Adicionar" title="Adicionar" /></td>
            </tr>
            <tbody id="repetir">
                <tr id="linha_1">
                    <td><input type="text" name="dia[]" id="nome" class="nome" placeholder="Nome" /></td>
                    <td><input type="text" name="datadenascimento[]" id="datadenascimento" class="datadenascimento" placeholder="Data de Nascimento" /></td>
                    <td>
                      <div class="mascaraselectsexofilho">Sexo</div>
                      <select name="sexofilho" class="sexofilho" id="sexofilho">
                        <option></option>
                        <option value="Masculino">Masculino</option>
                        <option value="Feminino">Feminino</option>
                      </select>
                    </td>
                    <td><input type="button" id="remove" onclick="$.removeLinha(this);" alt="Remover" title="Remover" /></td>
                </tr>
            </tbody>
          </table>
        </div>

        <div class="alterarsenha">
          <h1>Alterar Senha</h1>

          <input name="novasenha" type="password" placeholder="Digite sua nova senha" id="novasenha" class="novasenha"/>
          <input name="novasenha" type="password" placeholder="Confirme sua nova senha" id="novasenha" class="novasenha"/>
          <input type="button" id="alterar" class="alterar" value="Alterar" />

          <div class="confirmarsenha">

            <div class="close">Fechar</div>

            <!-- Se a senha for incorreta, basta dar um display:block nessa div mensagem erro -->
            <div class="mensagemdeerro">Senha incorreta!</div>
            <input name="senhaatual" type="password" id="senhaatual" class="senhaatual" placeholder="Digite sua senha atual" />
            <input type="button" id="confirmar" class="confirmar" value="Confirmar" />

            <!-- Se a senha for correta, basta dar um display:block nessa mensagem e um display:none nas inputs -->
            <div class="sucesso">
              <div class="iconcorreto"></div>
              <div class="mensagemsucesso">Senha alterada com sucesso!</div>
            </div>
          </div>  

        </div>

        <input type="submit" name="salvar" id="salvar" class="botao salvar" />

      </form>



    </div><!-- FIM RIGHT -->

    <!-- UF E CIDADE PAG CADASTRO -->
    <script language="JavaScript" type="text/javascript" src="js/cidades-estados.js"></script>
    <script type="text/javascript">
    new dgCidadesEstados({
      cidade: document.getElementById('cidade'),
      estado: document.getElementById('uf')
    });
    //ADIDIONAR E REMOVER FILHOS
    $.removeLinha = function (element){
      var linha_total = $('tbody#repetir tr').length;
            
      if (linha_total > 1){
        $(element).parent().parent().remove();
      }else{
        $('.mensagemtabela').html("Desculpe, mas você não pode remover esta última linha!");
      }       
    };
    $(document).ready(function(){     
      var limite_linhas = 10;           
      $('#add').click(function(){
            
        var linha_total = $('tbody#repetir tr').length;
                
        if (limite_linhas && limite_linhas > linha_total){
                  
          var linha = $('tbody#repetir tr').html();
                    
          var linha_total = $('tbody#repetir tr').length;
                    
          var linha_id = $('tbody#repetir tr').attr('id');
          
          $('tbody#repetir').append('<tr id="linha_' + (linha_total + 1) + '">' + linha + '</tr>');
    
        }else{
          $('.mensagemtabela').html("Desculpe, mas você só pode adicionar até " + limite_linhas + " linhas!");
        }       
      });
    });
    </script>
    <!-- FIM UF E CIDADE PAG CADASTRO -->

  <?php include('footer.php') ?>
