  <?php include('header.php') ?>

  <!-- CENTRO -->
  <div class="centralizando content minhavitrine minhascompras">

    <h2>MINHA VITRINE VIsP</h2>

    <?php include('barra-de-vitrine.php') ?>

    <?php include('sidebar-minha-vitrine.php') ?>

    <!-- RIGHT -->
    <div id="right">
      <h1>MINHAS COMPRAS</h1>

      <ul class="produtos">

        <!-- PRODUTO -->
        <li>

          <div class="foto">
            <img src="images/thumb-produto-1.jpg" alt="" title="" />
          </div>

          <!-- FIM MEIO -->
          <div class="meio">
            <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
            <div class="ref">ref. 34562</div>

            <!-- PREÇO -->
            <div id="preco">

              <div class="de">de: <span>R$ 199,00</span></div>
              <div class="atual">Por: <span>R$ 169,00</span></div>

            </div><!-- FIM PREÇO -->

            <!-- AVALIE VENDEDOR -->
            <div id="avalievendedor">

              <span>avalie o vendedor</span>

              <ol class="estrelinhas"><li></li><li></li><li></li><li></li><li></li></ol>

            </div><!-- FIM AVALIE VENDEDOR -->

            <!-- RECEBEU O PRODUTO -->
            <div id="recebeuoproduto">

              <span>recebeu o produto</span>

              <div class="sim"><a href="#" alt="Sim" title="Sim">Sim</a></div>

              <div class="nao"><a href="#" alt="Não" title="Não">Não</a></div>

            </div><!-- FIM RECEBEU O PRODUTO -->

          </div><!-- FIM MEIO -->

          <!-- DIREITA -->
          <div class="direita">

            <!-- ADOREI -->
            <div id="adorei">
              <span>adorei</span>
              <div class="botao like">
                <div class="icon"></div>
              </div>
              <span>123 likes</span>
            </div><!-- FIM ADOREI -->

          </div><!-- FIM DIREITA -->

        </li><!-- FIM PRODUTO -->

        <!-- PRODUTO -->
        <li>

          <div class="foto">
            <img src="images/thumb-produto-1.jpg" alt="" title="" />
          </div>

          <!-- FIM MEIO -->
          <div class="meio">
            <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
            <div class="ref">ref. 34562</div>

            <!-- PREÇO -->
            <div id="preco">

              <div class="de">de: <span>R$ 199,00</span></div>
              <div class="atual">Por: <span>R$ 169,00</span></div>

            </div><!-- FIM PREÇO -->

            <!-- AVALIE VENDEDOR -->
            <div id="avalievendedor">

              <span>avalie o vendedor</span>

              <ol class="estrelinhas"><li></li><li></li><li></li><li></li><li></li></ol>

            </div><!-- FIM AVALIE VENDEDOR -->

            <!-- RECEBEU O PRODUTO -->
            <div id="recebeuoproduto">

              <span>recebeu o produto</span>

              <div class="sim"><a href="#" alt="Sim" title="Sim">Sim</a></div>

              <div class="nao"><a href="#" alt="Não" title="Não">Não</a></div>

            </div><!-- FIM RECEBEU O PRODUTO -->

          </div><!-- FIM MEIO -->

          <!-- DIREITA -->
          <div class="direita">

            <!-- ADOREI -->
            <div id="adorei">
              <span>adorei</span>
              <div class="botao like">
                <div class="icon"></div>
              </div>
              <span>123 likes</span>
            </div><!-- FIM ADOREI -->

          </div><!-- FIM DIREITA -->

        </li><!-- FIM PRODUTO -->

        <!-- PRODUTO -->
        <li>

          <div class="foto">
            <img src="images/thumb-produto-1.jpg" alt="" title="" />
          </div>

          <!-- FIM MEIO -->
          <div class="meio">
            <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
            <div class="ref">ref. 34562</div>

            <!-- PREÇO -->
            <div id="preco">

              <div class="de">de: <span>R$ 199,00</span></div>
              <div class="atual">Por: <span>R$ 169,00</span></div>

            </div><!-- FIM PREÇO -->

            <!-- AVALIE VENDEDOR -->
            <div id="avalievendedor">

              <span>avalie o vendedor</span>

              <ol class="estrelinhas"><li></li><li></li><li></li><li></li><li></li></ol>

            </div><!-- FIM AVALIE VENDEDOR -->

            <!-- RECEBEU O PRODUTO -->
            <div id="recebeuoproduto">

              <span>recebeu o produto</span>

              <div class="sim"><a href="#" alt="Sim" title="Sim">Sim</a></div>

              <div class="nao"><a href="#" alt="Não" title="Não">Não</a></div>

            </div><!-- FIM RECEBEU O PRODUTO -->

          </div><!-- FIM MEIO -->

          <!-- DIREITA -->
          <div class="direita">

            <!-- ADOREI -->
            <div id="adorei">
              <span>adorei</span>
              <div class="botao like">
                <div class="icon"></div>
              </div>
              <span>123 likes</span>
            </div><!-- FIM ADOREI -->

          </div><!-- FIM DIREITA -->

        </li><!-- FIM PRODUTO -->

        <!-- PRODUTO -->
        <li>

          <div class="foto">
            <img src="images/thumb-produto-1.jpg" alt="" title="" />
          </div>

          <!-- FIM MEIO -->
          <div class="meio">
            <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
            <div class="ref">ref. 34562</div>

            <!-- PREÇO -->
            <div id="preco">

              <div class="de">de: <span>R$ 199,00</span></div>
              <div class="atual">Por: <span>R$ 169,00</span></div>

            </div><!-- FIM PREÇO -->

            <!-- AVALIE VENDEDOR -->
            <div id="avalievendedor">

              <span>avalie o vendedor</span>

              <ol class="estrelinhas"><li></li><li></li><li></li><li></li><li></li></ol>

            </div><!-- FIM AVALIE VENDEDOR -->

            <!-- RECEBEU O PRODUTO -->
            <div id="recebeuoproduto">

              <span>recebeu o produto</span>

              <div class="sim"><a href="#" alt="Sim" title="Sim">Sim</a></div>

              <div class="nao"><a href="#" alt="Não" title="Não">Não</a></div>

            </div><!-- FIM RECEBEU O PRODUTO -->

          </div><!-- FIM MEIO -->

          <!-- DIREITA -->
          <div class="direita">

            <!-- ADOREI -->
            <div id="adorei">
              <span>adorei</span>
              <div class="botao like">
                <div class="icon"></div>
              </div>
              <span>123 likes</span>
            </div><!-- FIM ADOREI -->

          </div><!-- FIM DIREITA -->

        </li><!-- FIM PRODUTO -->

        <!-- PRODUTO -->
        <li>

          <div class="foto">
            <img src="images/thumb-produto-1.jpg" alt="" title="" />
          </div>

          <!-- FIM MEIO -->
          <div class="meio">
            <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
            <div class="ref">ref. 34562</div>

            <!-- PREÇO -->
            <div id="preco">

              <div class="de">de: <span>R$ 199,00</span></div>
              <div class="atual">Por: <span>R$ 169,00</span></div>

            </div><!-- FIM PREÇO -->

            <!-- AVALIE VENDEDOR -->
            <div id="avalievendedor">

              <span>avalie o vendedor</span>

              <ol class="estrelinhas"><li></li><li></li><li></li><li></li><li></li></ol>

            </div><!-- FIM AVALIE VENDEDOR -->

            <!-- RECEBEU O PRODUTO -->
            <div id="recebeuoproduto">

              <span>recebeu o produto</span>

              <div class="sim"><a href="#" alt="Sim" title="Sim">Sim</a></div>

              <div class="nao"><a href="#" alt="Não" title="Não">Não</a></div>

            </div><!-- FIM RECEBEU O PRODUTO -->

          </div><!-- FIM MEIO -->

          <!-- DIREITA -->
          <div class="direita">

            <!-- ADOREI -->
            <div id="adorei">
              <span>adorei</span>
              <div class="botao like">
                <div class="icon"></div>
              </div>
              <span>123 likes</span>
            </div><!-- FIM ADOREI -->

          </div><!-- FIM DIREITA -->

        </li><!-- FIM PRODUTO -->

        <!-- PRODUTO -->
        <li>

          <div class="foto">
            <img src="images/thumb-produto-1.jpg" alt="" title="" />
          </div>

          <!-- FIM MEIO -->
          <div class="meio">
            <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
            <div class="ref">ref. 34562</div>

            <!-- PREÇO -->
            <div id="preco">

              <div class="de">de: <span>R$ 199,00</span></div>
              <div class="atual">Por: <span>R$ 169,00</span></div>

            </div><!-- FIM PREÇO -->

            <!-- AVALIE VENDEDOR -->
            <div id="avalievendedor">

              <span>avalie o vendedor</span>

              <ol class="estrelinhas"><li></li><li></li><li></li><li></li><li></li></ol>

            </div><!-- FIM AVALIE VENDEDOR -->

            <!-- RECEBEU O PRODUTO -->
            <div id="recebeuoproduto">

              <span>recebeu o produto</span>

              <div class="sim"><a href="#" alt="Sim" title="Sim">Sim</a></div>

              <div class="nao"><a href="#" alt="Não" title="Não">Não</a></div>

            </div><!-- FIM RECEBEU O PRODUTO -->

          </div><!-- FIM MEIO -->

          <!-- DIREITA -->
          <div class="direita">

            <!-- ADOREI -->
            <div id="adorei">
              <span>adorei</span>
              <div class="botao like">
                <div class="icon"></div>
              </div>
              <span>123 likes</span>
            </div><!-- FIM ADOREI -->

          </div><!-- FIM DIREITA -->

        </li><!-- FIM PRODUTO -->

        <!-- PRODUTO -->
        <li>

          <div class="foto">
            <img src="images/thumb-produto-1.jpg" alt="" title="" />
          </div>

          <!-- FIM MEIO -->
          <div class="meio">
            <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
            <div class="ref">ref. 34562</div>

            <!-- PREÇO -->
            <div id="preco">

              <div class="de">de: <span>R$ 199,00</span></div>
              <div class="atual">Por: <span>R$ 169,00</span></div>

            </div><!-- FIM PREÇO -->

            <!-- AVALIE VENDEDOR -->
            <div id="avalievendedor">

              <span>avalie o vendedor</span>

              <ol class="estrelinhas"><li></li><li></li><li></li><li></li><li></li></ol>

            </div><!-- FIM AVALIE VENDEDOR -->

            <!-- RECEBEU O PRODUTO -->
            <div id="recebeuoproduto">

              <span>recebeu o produto</span>

              <div class="sim"><a href="#" alt="Sim" title="Sim">Sim</a></div>

              <div class="nao"><a href="#" alt="Não" title="Não">Não</a></div>

            </div><!-- FIM RECEBEU O PRODUTO -->

          </div><!-- FIM MEIO -->

          <!-- DIREITA -->
          <div class="direita">

            <!-- ADOREI -->
            <div id="adorei">
              <span>adorei</span>
              <div class="botao like">
                <div class="icon"></div>
              </div>
              <span>123 likes</span>
            </div><!-- FIM ADOREI -->

          </div><!-- FIM DIREITA -->

        </li><!-- FIM PRODUTO -->

        <!-- PRODUTO -->
        <li>

          <div class="foto">
            <img src="images/thumb-produto-1.jpg" alt="" title="" />
          </div>

          <!-- FIM MEIO -->
          <div class="meio">
            <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
            <div class="ref">ref. 34562</div>

            <!-- PREÇO -->
            <div id="preco">

              <div class="de">de: <span>R$ 199,00</span></div>
              <div class="atual">Por: <span>R$ 169,00</span></div>

            </div><!-- FIM PREÇO -->

            <!-- AVALIE VENDEDOR -->
            <div id="avalievendedor">

              <span>avalie o vendedor</span>

              <ol class="estrelinhas"><li></li><li></li><li></li><li></li><li></li></ol>

            </div><!-- FIM AVALIE VENDEDOR -->

            <!-- RECEBEU O PRODUTO -->
            <div id="recebeuoproduto">

              <span>recebeu o produto</span>

              <div class="sim"><a href="#" alt="Sim" title="Sim">Sim</a></div>

              <div class="nao"><a href="#" alt="Não" title="Não">Não</a></div>

            </div><!-- FIM RECEBEU O PRODUTO -->

          </div><!-- FIM MEIO -->

          <!-- DIREITA -->
          <div class="direita">

            <!-- ADOREI -->
            <div id="adorei">
              <span>adorei</span>
              <div class="botao like">
                <div class="icon"></div>
              </div>
              <span>123 likes</span>
            </div><!-- FIM ADOREI -->

          </div><!-- FIM DIREITA -->

        </li><!-- FIM PRODUTO -->

        <!-- PRODUTO -->
        <li>

          <div class="foto">
            <img src="images/thumb-produto-1.jpg" alt="" title="" />
          </div>

          <!-- FIM MEIO -->
          <div class="meio">
            <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
            <div class="ref">ref. 34562</div>

            <!-- PREÇO -->
            <div id="preco">

              <div class="de">de: <span>R$ 199,00</span></div>
              <div class="atual">Por: <span>R$ 169,00</span></div>

            </div><!-- FIM PREÇO -->

            <!-- AVALIE VENDEDOR -->
            <div id="avalievendedor">

              <span>avalie o vendedor</span>

              <ol class="estrelinhas"><li></li><li></li><li></li><li></li><li></li></ol>

            </div><!-- FIM AVALIE VENDEDOR -->

            <!-- RECEBEU O PRODUTO -->
            <div id="recebeuoproduto">

              <span>recebeu o produto</span>

              <div class="sim"><a href="#" alt="Sim" title="Sim">Sim</a></div>

              <div class="nao"><a href="#" alt="Não" title="Não">Não</a></div>

            </div><!-- FIM RECEBEU O PRODUTO -->

          </div><!-- FIM MEIO -->

          <!-- DIREITA -->
          <div class="direita">

            <!-- ADOREI -->
            <div id="adorei">
              <span>adorei</span>
              <div class="botao like">
                <div class="icon"></div>
              </div>
              <span>123 likes</span>
            </div><!-- FIM ADOREI -->

          </div><!-- FIM DIREITA -->

        </li><!-- FIM PRODUTO -->

        <!-- PRODUTO -->
        <li>

          <div class="foto">
            <img src="images/thumb-produto-1.jpg" alt="" title="" />
          </div>

          <!-- FIM MEIO -->
          <div class="meio">
            <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
            <div class="ref">ref. 34562</div>

            <!-- PREÇO -->
            <div id="preco">

              <div class="de">de: <span>R$ 199,00</span></div>
              <div class="atual">Por: <span>R$ 169,00</span></div>

            </div><!-- FIM PREÇO -->

            <!-- AVALIE VENDEDOR -->
            <div id="avalievendedor">

              <span>avalie o vendedor</span>

              <ol class="estrelinhas"><li></li><li></li><li></li><li></li><li></li></ol>

            </div><!-- FIM AVALIE VENDEDOR -->

            <!-- RECEBEU O PRODUTO -->
            <div id="recebeuoproduto">

              <span>recebeu o produto</span>

              <div class="sim"><a href="#" alt="Sim" title="Sim">Sim</a></div>

              <div class="nao"><a href="#" alt="Não" title="Não">Não</a></div>

            </div><!-- FIM RECEBEU O PRODUTO -->

          </div><!-- FIM MEIO -->

          <!-- DIREITA -->
          <div class="direita">

            <!-- ADOREI -->
            <div id="adorei">
              <span>adorei</span>
              <div class="botao like">
                <div class="icon"></div>
              </div>
              <span>123 likes</span>
            </div><!-- FIM ADOREI -->

          </div><!-- FIM DIREITA -->

        </li><!-- FIM PRODUTO -->

      </ul>

      
    </div><!-- FIM RIGHT -->

  <?php include('footer.php') ?>
