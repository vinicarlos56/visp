  <?php include('header.php') ?>

<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- Bootstrap CSS fixes for IE6 -->
<!--[if lt IE 7]><link rel="stylesheet" href="css/bootstrap-ie6.min.css"><![endif]-->

<link rel="stylesheet" href="css/jquery.fileupload-ui.css">

<noscript><link rel="stylesheet" href="css/jquery.fileupload-ui-noscript.css"></noscript>
<!--[if lt IE 9]><script src="css/html5.js"></script><![endif]-->

  <!-- CENTRO -->
  <div class="centralizando content dadosdoproduto">

    <form id="fileupload" action="server/php/files/" method="POST" enctype="multipart/form-data">

      <!-- PRIMEIRO NÍVEL -->
      <div class="primeironivel">

        <h3>DADOS DO PRODUTO</h3>

        <div class="tituloedescricao">

          <input type="text" placeholder="Título do produto" name="titulodoproduto" class="titulodoproduto" id="titulodoproduto" />

          <textarea placeholder="Descrição do produto (conte uma história)" name="descricaodoproduto"></textarea>

        </div>

        <div class="categoriatamanhocormarca">

          <div class="mascaracategoria">Categoria</div>
          <select id="categoria">
            <option>Roupas</option>
            <option>Sapatos</option>
            <option>Acessórios</option>
            <option>Brinquedos</option>
            <option>Livros e DVDs</option>
            <option>Enxoval</option>
            <option>Móveis e Decoração</option>
            <option>Cadeiras e Carrinhos</option>
          </select>

          <input type="text" placeholder="Tamanho" name="tamanho" id="tamanho" maxlength="3" />

          <span id="tooltip" class="tooltip">
            <div class="nome">Tamanho do Produto</div>
            <div class="usadoounovo">lorem ipsum lorem ipsum</div>
          </span>

          <input type="text" placeholder="Cor" name="cor" id="cor" />

          <input type="text" placeholder="Marca" name="marca" id="marca" />

        </div>

        <div class="subcategoriacondicaoorigem">

          <div class="mascarasubcategoria">Sub-Categoria</div>
          <select id="subcategoria">
            <option>Meninos</option>
            <option>Meninas</option>
            <option>Unissex</option>
            <option>Gêmeos</option>
          </select>

          <div class="condicao">
            <h4>Condição</h4>
            <input type="radio" name="condicao" value="Nunca usado" id="nuncausado" class="nuncausado" />
            <label>Nunca usado</label>


            <input type="radio" name="condicao" value="Usado" id="usado" class="usado" />
            <label>Usado</label>
          </div>

          <div class="origem">
            <h4>Origem</h4>
            <input type="radio" name="origem" value="Nacional" id="nacional" class="nacional" />
            <label>Nacional</label>

            <input type="radio" name="origem" value="Internacional" id="internacional" class="internacional" />
            <label>Internacional</label>
          </div>

        </div>

      </div><!-- FIM PRIMEIRO NIVEL -->

      <!-- SEGUNDO NÍVEL -->
      <div class="segundonivel">

        <h3>PESO E FRETE</h3>

        <ul class="tabeladefrete">

          <li>
            <div class="mascarainput">Frete Grátis</div>
            <input type="radio" name="pesoefrete" value="Frete grátis - O frete será por sua conta" />
            <label>O frete será por sua conta</label>
          </li>

          <li>
            <div class="mascarainput">Peso Pena</div>
            <input type="radio" name="pesoefrete" value="Peso pena - 0,3 a 2kg" />
            <label>0,3 a 2kg</label>
          </li>

          <li>
            <div class="mascarainput">Peso Leve</div>
            <input type="radio" name="pesoefrete" value="Peso leve - 2 a 5kg" />
            <label>2 a 5kg</label>
          </li>

          <li>
            <div class="mascarainput">Peso Médio</div>
            <input type="radio" name="pesoefrete" value="Peso médio - 5 a 12kg" />
            <label>5 a 12kg</label>
          </li>

          <li>
            <div class="mascarainput">Peso Pesado</div>
            <input type="radio" name="pesoefrete" value="Peso pesado - acima de 12kg" />
            <label>acima de 12kg</label>
          </li>

          <li>
            <div class="mascarainput">A Combinar</div>
            <input type="radio" name="pesoefrete" value="A Combinar - Aceito marcar retirada" />
            <label>Aceito marcar retirada</label>
          </li>

        </ul>

      </div><!-- FIM SEGUNDO NÍVEL -->

      <!-- TERCEIRO NÍVEL -->
      <div class="terceironivel">

        <h3>INSERIR FOTOS</h3>

        <!-- UPLOAD -->
        <div class="row fileupload-buttonbar">
            <div class="span7">

                <span class="btn-success fileinput-button">
                    <input type="file" name="files[]" multiple>
                </span>
                <button type="submit" class="btn btn-primary start">
                    <i class="icon-upload icon-white"></i>
                    <span>Start upload</span>
                </button><br />
                <button type="reset" class="btn btn-warning cancel">
                    <i class="icon-ban-circle icon-white"></i>
                    <span>Cancelar upload</span>
                </button><br />
                <button type="button" class="btn btn-danger delete">
                    <i class="icon-trash icon-white"></i>
                    <span>Deletar</span>
                </button>
            </div>

            <div class="span5 fileupload-progress fade">

                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="bar" style="width:0%;"></div>
                </div>

                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>

        <div class="fileupload-loading"></div>

        <table role="presentation" class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>

        <div id="modal-gallery" class="modal modal-gallery hide fade" data-filter=":odd" tabindex="-1"></div>

        <script id="template-upload" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
            <tr class="template-upload fade">
                <td class="preview"><span class="fade"></span></td>
                <td class="name"><span>{%=file.name%}</span></td>
                <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
                {% if (file.error) { %}
                    <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
                {% } else if (o.files.valid && !i) { %}
                    <td>
                        <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
                    </td>
                    <td class="start">{% if (!o.options.autoUpload) { %}
                        <button class="btn btn-primary">
                            <i class="icon-upload icon-white"></i>
                            <span>Enviar</span>
                        </button>
                    {% } %}</td>
                {% } else { %}
                    <td colspan="2"></td>
                {% } %}
                <td class="cancel">{% if (!i) { %}
                    <button class="btn btn-warning">
                        <i class="icon-ban-circle icon-white"></i>
                        <span>Cancelar</span>
                    </button>
                {% } %}</td>
            </tr>
        {% } %}
        </script>

        <script id="template-download" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
            <tr class="template-download fade">
                {% if (file.error) { %}
                    <td></td>
                    <td class="name"><span>{%=file.name%}</span></td>
                    <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
                    <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
                {% } else { %}
                    <td class="preview">{% if (file.thumbnail_url) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
                    {% } %}</td>
                    <td class="name">
                        <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
                    </td>
                    <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
                    <td colspan="2"></td>
                {% } %}
                <td class="delete">
                    <button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                        <i class="icon-trash icon-white"></i>
                        <span>Deletar</span>
                    </button>
                    <input type="checkbox" name="delete" value="1">
                </td>
            </tr>
        {% } %}
        </script><!-- FIM UPLOAD -->

      </div><!-- FIM TERCEIRO NÍVEL -->

      <!-- QUARTO NÍVEL -->
      <div class="quartonivel">
        <h3>PREÇO</h3>

        <div class="precoorigial">
          <label>Preço Original</label>
          <input type="text" name="precooriginal" id="precooriginal" class="precooriginal" placeholder="R$ 0,00" />
        </div>

        <div class="seupreco">
          <label>Seu Preço</label>
          <input type="text" name="seupreco" id="seupreco" class="seupreco" placeholder="R$ 0,00" />
        </div>

        <div class="inputdequantidade">
          <div class="mascaraquantidade">Quantidade</div>
          <select id="quantidade">
            <option>01</option>
            <option>02</option>
            <option>03</option>
            <option>04</option>
            <option>05</option>
            <option>06</option>
            <option>07</option>
            <option>08</option>
            <option>09</option>
            <option>10</option>
          </select>
        </div>
      </div><!-- FIM QUARTO NÍVEL -->

      <!-- QUINTO NÍVEL -->
      <div class="quintonivel">

        <p>Ao clicar em “Enviar” você está concordando com os termos de serviço</p>

        <div class="botao enviarproduto"><a href="#">Enviar</a></div>

      </div><!-- FIM QUINTO NÍVEL -->

  <?php include('footer.php') ?>

<script src="js/upload-de-fotos/vendor/jquery.ui.widget.js"></script>
<script src="js/upload-de-fotos/tmpl.min.js"></script>
<script src="js/upload-de-fotos/load-image.min.js"></script>
<script src="js/upload-de-fotos/canvas-to-blob.min.js"></script>
<script src="js/upload-de-fotos/bootstrap.min.js"></script>

<script src="js/upload-de-fotos/jquery.iframe-transport.js"></script>
<script src="js/upload-de-fotos/jquery.fileupload.js"></script>
<script src="js/upload-de-fotos/jquery.fileupload-fp.js"></script>
<script src="js/upload-de-fotos/jquery.fileupload-ui.js"></script>
<script src="js/upload-de-fotos/main.js"></script>
<!--[if gte IE 8]><script src="js/upload-de-fotos/cors/jquery.xdr-transport.js"></script><![endif]-->

