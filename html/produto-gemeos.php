  <?php include('header.php') ?>

  <!-- BREADCRUMB -->
  <div id="breadcrumb" class="gemeos">

    <a href="#">ROUPAS</a> > GEMEOS

  </div><!-- FIM BREADCRUMB -->

  <link rel="stylesheet" href="css/global.css" />

  <!-- CENTRO -->
  <div class="centralizando content produto gemeos">

    <?php include('sidebar.php') ?>

    <!-- RIGHT -->
    <div id="right">

      <!-- VENDEDOR -->
      <div class="vendedor">

        <a href="#">
          <div class="avatar"></div>

          <div class="dadosvendedor">
            <div class="titulovendedor">vendedor</div>
            <h2 class="gemeos">Nome do vendedor</h2>
            <div class="urlvendedor">www.nomedovendedor.vispstore.com</div>
          </div>
        </a>

      </div><!-- FIM VENDEDOR -->

      <!-- PRIMEIRO NÍVEL -->
      <div class="primeiro-nivel">    

        <!-- PRODUTO IMAGEM -->
        <div id="produtoimagem">

          <div class="flag-nunca-usado">Nunca usado</div>

          <div id="imagemapresentacao" class="slides_container">
            <span><img src="images/imagem-produto-apresentacao-1.jpg" /></span>
            <span><img src="images/imagem-produto-apresentacao-2.jpg" /></span>
            <span><img src="images/imagem-produto-apresentacao-3.jpg" /></span>
          </div>

          <ul class="thumbproduto pagination">
            <li>
              <a href="images/imagem-produto-apresentacao-1">
                <img src="images/thumb-produto-1.jpg" />
              </a>
            </li>
            <li>
              <a href="images/imagem-produto-apresentacao-2">
                <img src="images/thumb-produto-2.jpg" />
              </a>
            </li>
            <li>
              <a href="images/imagem-produto-apresentacao-3">
                <img src="images/thumb-produto-3.jpg" />
              </a>
            </li>
          </ul>

          <!-- REDES SOCIAIS -->
          <div id="redessociais">

            <h3>compartilhe</h3>

            <!-- AddThis Button BEGIN -->
            <div class="addthis_toolbox addthis_default_style ">
              <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
              <a class="addthis_button_tweet"></a>
              <a class="addthis_button_pinterest_pinit"></a>
              <a class="addthis_counter addthis_pill_style"></a>
            </div>
            <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50e72de94b4351f6"></script>
            <!-- AddThis Button END -->

          </div><!-- FIM REDES SOCIAIS -->

        </div><!-- FIM PRODUTO IMAGEM -->

        <!-- INFORMAÇÕES PRODUTOS -->
        <div id="informacoesproduto">

          <!-- ADOREI -->
          <div id="adorei">
            <span>adorei</span>
            <div class="botao like">
              <div class="icon"></div>
            </div>
            <span>123 likes</span>
          </div><!-- FIM ADOREI -->

          <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
          <div class="ref">ref. 34562</div>

          <!-- PREÇO -->
          <div id="preco">

            <div class="de">de: <span>R$ 199,00</span></div>
            <div class="atual">Por: <span>R$ 169,00</span></div>
            <div class="condicao">ou em 10x de <span>R$399</span></div>

          </div><!-- FIM PREÇO -->

          <!-- FLAG DE FRETE E BOTAO COMPRAR -->
          <div id="flagfrete-e-botaocomprar">

            <div class="flagfrete">Frete Grátis</div>

            <div class="botao comprar"><a href="#">Comprar</a></div>

          </div><!-- FIM FLAG DE FRETE E BOTAO COMPRAR -->

          <!-- DESCRICAO DO PRODUTO -->
          <div id="descricaodoproduto">

            <h3>Descrição do produto</h3>

            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quam libero, tincidunt nec placerat sollicitudin, cursus ac massa. Suspendisse potenti. In mattis lorem in lacus semper vitae elementum quam elementum. Etiam ac enim felis, vel pellentesque velit. Nulla feugiat semper sapien eu vehicula. Nunc ullamcorper rhoncus sollicitudin. </p>

          </div><!-- FIM INFORMAÇÕES PRODUTOS -->

          <!-- ORIGEM DO PRODUTO -->
          <div id="origemdoproduto">

            <h3>origem do produto</h3>
            <div class="retangulo">São Paulo</div>

          </div><!-- FIM ORIGEM DO PRODUTO -->

          <!-- QUALIFICACAO DO VENDEDOR -->
          <div id="qualificacaodovendedor">

            <h3>qualificação do vendedor</h3>

            <ol class="estrelinhas"><li></li><li></li><li></li><li></li><li></li></ol>
            <div class="quantidade"><span>192</span> avaliações</div>

          </div><!-- FIM QUALIFICACAO DO VENDEDOR -->

        </div>

      </div><!-- FIM PRIMEIRO NÍVEL -->

      <!-- SEGUNDO NÍVEL -->
      <div id="segundo-nivel">

        <!-- COMPRA SEGURA -->
        <div id="comprasegura">

          <h2>COMPRA SEGURA</h2>

          <div class="entrega">
            <div class="icon"></div>
            <h3>Entrega</h3>
            <p>O pagamento so é liberado ao vendedor<br />
            após você confirmar que recebeu tudo certo</p>
          </div>

          <div class="compraprotegida">
            <div class="icon"></div>
            <h3>Compra protegida</h3>
            <p>A compra é feita em ambiente seguro.<br />
            os dados do cartao não são armazenados</p>
          </div>

        </div><!-- FIM COMPRA SEGURA -->

        <!-- OUTROS PRODUTOS -->
        <div id="outrosprodutos">

          <h2>Outros produtos do vendedor</h2>

          <ul class="produtos">

            <li>
              <a href="#">
                <img src="images/thumb-produto-3.jpg" />
              </a>
            </li>

            <li>
              <a href="#">
                <img src="images/thumb-produto-3.jpg" />
              </a>
            </li>

            <li>
              <a href="#">
                <img src="images/thumb-produto-3.jpg" />
              </a>
            </li>

            <li>
              <a href="#">
                <img src="images/thumb-produto-3.jpg" />
              </a>
            </li>

            <li>
              <a href="#">
                <img src="images/thumb-produto-3.jpg" />
              </a>
            </li>

            <li>
              <a href="#">
                <img src="images/thumb-produto-3.jpg" />
              </a>
            </li>

          </ul>

        </div><!-- FIM OUTROS PRODUTOS -->

      </div><!-- FIM SEGUNDO NÍVEL -->

      <!-- SLIDE IMAGEM -->
      <script type="text/javascript" src="js/slides.min.jquery.js"></script><!-- FIM SLIDE IMAGEM -->

    </div><!-- FIM RIGHT -->

  <?php include('footer.php') ?>
