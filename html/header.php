<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

  <meta http-equiv="content-language" content="pt-br" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="refresh" content="1080" />
  <meta http-equiv="Content-Script-Type" content="text/javascript" />
  <meta http-equiv="Content-Style-Type" content="text/css" />

  <meta name="description" content="lorem lorem lorem lorem lorem"/>
  <meta name="keywords" content="lorem lorem lorem lorem lorem"/>

  <title>VIsP</title>

  <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

  <link rel="stylesheet" href="css/style.css" />

  <script type="text/javascript" src="js/jquery.min.js"></script>

</head>

<body>

<!-- CONTAINER -->
<div id="container">

  <!-- HEADER-->
  <div id="header">

    <!-- TOP -->
    <div class="top">

      <div class="centralizando">

        <a href="#" class="atendimento">Atendimento - Ajuda</a>

        <ul class="entrada">

          <li class="first">
            olá, visitante
          </li>

          <li>
            <a href="#">login</a>
          </li>

          <li>
            <a href="#">cadastro</a>
          </li>

        </ul>

        <div class="carrinho">

          <a href="#">

            <div class="minhascompras">Minhas compras <span>(0)</span></div>

            <div class="icon"></div>

          </a>

        </div>

      </div>

    </div><!-- FIM TOP -->

    <div class="centralizando">

      <h1>
        <a href="#">
          <img src="images/logo-visp.png" title="Logo VIsP" alt="Logo VIsP" />
        </a>
      </h1>

      <div class="direitologo">

        <form>
          <input type="text" class="busca" placeholder="O que você quer encontrar?" />
          <input type="submit" class="btnbuscar botao" />
        </form>

        <ul class="redesocial">
          <li class="compartilhe"></li>
          <li class="facebook"><a href="#" target="_blank" title="Facebook" alt="Facebook">Facebook</a></li>
          <li class="gplus"><a href="#" target="_blank" title="Google Plus" alt="Google Plus">Google Plus</a></li>
          <li class="pinterest"><a href="#" target="_blank" title="Pinterest" alt="Pinterest">Pinterest</a></li>
        </ul>

        <ul class="menu">

          <li>
            <a href="#" title="COMO FUNCIONA" alt="COMO FUNCIONA">
              COMO FUNCIONA
            </a>
          </li>

          <li>
            <a href="#" title="QUERO VENDER" alt="QUERO VENDER">
              QUERO VENDER
            </a>
          </li>

          <li>
            <a href="#" title="QUERO COMPRAR" alt="QUERO COMPRAR">
              QUERO COMPRAR
            </a>
          </li>
        </ul>

      </div>

    </div>

  </div><!-- FIM HEADER -->