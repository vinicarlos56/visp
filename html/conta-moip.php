  <?php include('header.php') ?>

  <!-- CENTRO -->
  <div class="centralizando content minhavitrine moip">

    <h2>MINHA VITRINE VIsP</h2>

    <?php include('barra-de-vitrine.php') ?>

    <?php include('sidebar-minha-vitrine.php') ?>

    <!-- RIGHT -->
    <div id="right">
      <h1>CONTA MOIP</h1>

      <h3>Forma de recebimento de suas vendas</h3>

      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur purus sapien, fringilla quis eleifend condimentum, laoreet et mauris. Nullam consectetur varius nisi, quis sollicitudin elit dignissim id. Nullam egestas dolor non ligula varius a bibendum mi fringilla. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus fermentum est vel nibh ornare ultricies. </p>

      <div class="acessomoip">
        <div class="left">
          <div class="icon"></div>
          <label>E-mail da conta</label>
          <!-- Deixar o email cadastrado marcado -->
          <input placeholder="email@gmail.com" type="text" class="contamoip" id="contamoip" name="contamoip" />
          <!-- Esse trocar só aparece depois de preenchido. -->
          <div class="trocar"><a href="#">Trocar</a></div>
        </div>

        <div class="right">
          <div class="emailencontrado">
            <h4>Email encontrado no MoIP.</h4>
            <p>Sua conta está ativa.</p>
            <div class="ok"></div>
          </div>
        </div>
      </div>

      <input type="submit" name="salvar" id="salvar" class="botao salvar" />

    </div><!-- FIM RIGHT -->

  <?php include('footer.php') ?>
