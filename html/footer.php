    <!-- FOOTER -->
    <div id="footer">
      
      <ul class="menufooter">

          <li>
            <a href="#" title="QUEM SOMOS" alt="QUEM SOMOS">
              QUEM SOMOS
            </a>
          </li>

          <li>
            <a href="#" title="PRECISA DE AJUDA?" alt="PRECISA DE AJUDA?">
              PRECISA DE AJUDA?
            </a>
          </li>

          <li>
            <a href="#" title="QUERO VENDER" alt="QUERO VENDER">
              QUERO VENDER
            </a>
          </li>

          <li>
            <a href="#" title="QUERO COMPRAR" alt="QUERO COMPRAR">
              QUERO COMPRAR
            </a>
          </li>
        </ul>

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quam libero, tincidunt nec placerat sollicitudin, cursus ac massa. Suspendisse potenti. In mattis lorem in lacus semper vitae elementum quam elementum. Etiam ac enim felis, vel pellentesque velit. Nulla feugiat semper sapien eu vehicula. Nunc ullamcorper rhoncus sollicitudin. Pellentesque placerat dui ut ipsum semper id varius justo suscipit. Donec consequat purus at metus facilisis fringilla.</p>

        <!-- SUBFOOTER -->
        <div class="subfooter">

          <div class="moip"></div>

          <ul class="formas-de-pagamento">
            <li class="formas">Formas de pagamento</li>
            <li class="master">MasterCard</li>
            <li class="visa">Visa</li>
            <li class="diners">Diners</li>
            <li class="amex">Amex</li>
            <li class="bb">Banco do Brasil</li>
            <li class="itau">Itaú</li>
            <li class="bradesco">Bradesco</li>
            <li class="boleto">Boleto Bancário</li>
          </ul>

          <div class="certificados">aqui fica os certificados de segurança</div>

        </div><!-- FIM SUBFOOTER -->

        <!-- COPYRIGHT -->
        <div class="copyright-politica-e-termo">

          <div class="copyright">
            copyright 2012 - todos os direitos reservados
          </div>

          <ul class="politica-e-termo">
            <li>
              <a href="#">Politica de privacidade</a>
            </li>
            <li>
              <a href="#">Termos de uso</a>
            </li>
          </ul>

        </div><!-- FIM COPYRIGHT -->

    </div><!-- FIM FOOTER -->

  </div><!-- CENTRO -->

</div><!-- CONTAINER -->

</body>
</html>

<!-- SLIDER -->
<script type="text/javascript" src="js/jquery.easing.min.js"></script> 
<script type="text/javascript" src="js/royal-slider-8.1.js"></script>
<!-- FIM SLIDER -->

<!-- PLACEHOLDER PARA IE -->
<script type="text/javascript" src="js/placeholder.js"></script> 
<!-- FIM PLACEHOLDER PARA IE -->

<!-- ACCORDION -->
<script type="text/javascript" src="js/jquery.msAccordion.js"></script> 
<!-- FIM ACCORDION -->

<!-- MASCARA DINHEIRO -->
<script type="text/javascript" src="js/jquery.maskMoney.js"></script> 
<!-- FIM MASCARA DINHEIRO -->

<!-- TOOLTIP PRATELEIRA -->
<script type="text/javascript" src="js/jquery.ezpz_tooltip.min.js"></script> 
<!-- FIM TOOLTIP PRATELEIRA -->

<!-- MASCARAS -->
<script type="text/javascript" src="js/jquery.maskedinput-1.1.4.pack.js"></script>
<!-- FIM MASCARAS -->

<!-- SCRIPTS GERAL DO SITE -->
<script type="text/javascript" src="js/scripts.js"></script>
<!-- FIM SCRIPTS GERAL DO SITE -->

