  <?php include('header.php') ?>

  <!-- CENTRO -->
  <div class="centralizando content teladelogin">

      
      <ul class="teladelogin">

        <li class="first">
          <h1>
            já possui conta?<br />
            <span>acesse agora!</span>
          </h1>

          <form>

            <input type="text" name="login" class="login" placeholder="Login" />
            <input type="password" name="email" class="email" placeholder="Email" />

            <input type="checkbox" name="mantenhaconectado" id="mantenhaconectado" />
            <label>Manter conectado</label>

            <input type="submit" name="acessar" class="acessar botao" />

          </form>

          <div class="esquecisenha">
            <a href="#">Ops... Esqueci minha senha</a>
          </div>

        </li>

        <li class="last">
          <h1>SUA PRIMEIRA VEZ?</h1>

          <div class="conectefacebook">
            <a href="#">conecte com facebook</a>
          </div>

          <p>*Temos um compromisso com você de  nunca publicar nada no seu perfil sem a sua autorização</p>

          <div class="ou">
            <span>ou</span>
          </div>

          <h1><span>Crie sua conta</span></h1>

          <p class="crie">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse non magna id diam lobortis laoreet ac nec lorem.</p>

          <div class="criesuaconta botao">
            <a href="#">crie sua conta</a>
          </div>

        </li>

      </ul>

      <div class="esqueciminhasenha">

        <div class="close">Fechar</div>

        <!-- Se o dado for incorreto, basta dar um display:block nessa div mensagem erro -->
        <div class="mensagemdeerro">Dados não encontrados!</div>
        <div class="cpfoucnpj">
          <input type="radio" name="tipo" id="radiocpf" checked />
          <label class="cpf">CPF</label>

          <input type="radio" name="tipo" id="radiocnpj" />
          <label class="cnpj">CNPJ</label>      
        </div>

        <input type="text" name="cpf" id="cpf" class="cpf" placeholder="CPF" />
        <input type="text" name="cnpj" id="cnpj" class="cnpj" placeholder="CNPJ" style="display: none" />

        <input type="button" id="confirmar" class="confirmar" value="Confirmar" />

        <!-- Sucesso!!! basta dar um display:block nessa mensagem e um display:none nas inputs -->
        <div class="sucesso">
          <div class="iconcorreto"></div>
          <div class="mensagemsucesso">Sua nova senha foi enviada para o email de cadatro!</div>
        </div>
      </div>

  <?php include('footer.php') ?>
