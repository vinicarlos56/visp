  <?php include('header.php') ?>

  <!-- CENTRO -->
  <div class="centralizando content minhavitrine minhasvendas">

    <h2>MINHA VITRINE VIsP</h2>

    <?php include('barra-de-vitrine.php') ?>

    <?php include('sidebar-minha-vitrine.php') ?>

    <!-- RIGHT -->
    <div id="right">
      <h1>MINHAS VENDAS</h1>

      <ul class="produtos">

        <!-- PRODUTO -->
        <li>

          <a href="#">
            <div class="flag-nunca-usado-thumb">Nunca usado</div>

            <div class="foto">
              <img src="images/thumb-produto-1.jpg" alt="" title="" />
            </div>
          </a>

          <!-- FIM MEIO -->
          <div class="meio">
            <a href="#">
              <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
              <div class="ref">ref. 34562</div>
            </a>

            <!-- PREÇO -->
            <div id="preco">

              <a href="#">

                <div class="de">de: <span>R$ 199,00</span></div>
                <div class="atual">Por: <span>R$ 169,00</span></div>

              </a>

            </div><!-- FIM PREÇO -->

            <!-- RASTREAMENTO -->
            <div id="rastreamento">

              <span id="tooltip-rastreamento" class="tooltip-rastreamento">
                
                <form>
                  <div class="data">Data de envio</div>
                  <input type="text" class="dataenvio" id="dataenvio" name="dataenvio" />

                  <div class="numero">Número de restreamento</div>
                  <input type="text" class="numerorastreamento" id="numerorastreamento" name="numerorastreamento" />
                
                  <input type="submit" class="enviar" id="enviar" />
                </form>
              </span>
              <div class="icon"></div>

            </div><!-- FIM RASTREAMENTO -->

          </div><!-- FIM MEIO -->

        </li><!-- FIM PRODUTO -->

        <!-- PRODUTO -->
        <li>

          <a href="#">
            <div class="foto">
              <img src="images/thumb-produto-1.jpg" alt="" title="" />
            </div>
          </a>

          <!-- FIM MEIO -->
          <div class="meio">
            <a href="#">
              <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
              <div class="ref">ref. 34562</div>
            </a>

            <!-- PREÇO -->
            <div id="preco">

              <a href="#">

                <div class="de">de: <span>R$ 199,00</span></div>
                <div class="atual">Por: <span>R$ 169,00</span></div>

              </a>

            </div><!-- FIM PREÇO -->

          </div><!-- FIM MEIO -->

          <!-- DIREITA -->
          <div class="direita">

            <!-- EDITAR -->
            <div id="editar" class="botao">

              <a href="#">
                Editar
              </a>
              
            </div><!-- FIM EDITAR -->

            <!-- DELETAR -->
            <div id="close" class="botao">Deletar</div><!-- FIM DELETAR -->

          </div><!-- FIM DIREITA -->

        </li><!-- FIM PRODUTO -->   

        <!-- PRODUTO -->
        <li>

          <a href="#">
            <div class="flag-nunca-usado-thumb">Nunca usado</div>

            <div class="foto">
              <img src="images/thumb-produto-1.jpg" alt="" title="" />
            </div>
          </a>

          <!-- FIM MEIO -->
          <div class="meio">
            <a href="#">
              <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
              <div class="ref">ref. 34562</div>
            </a>

            <!-- PREÇO -->
            <div id="preco">

              <a href="#">

                <div class="de">de: <span>R$ 199,00</span></div>
                <div class="atual">Por: <span>R$ 169,00</span></div>

              </a>

            </div><!-- FIM PREÇO -->

            <!-- RASTREAMENTO -->
            <div id="rastreamento">

              <span id="tooltip-rastreamento" class="tooltip-rastreamento">
                
                <form>
                  <div class="data">Data de envio</div>
                  <input type="text" class="dataenvio" id="dataenvio" name="dataenvio" />

                  <div class="numero">Número de restreamento</div>
                  <input type="text" class="numerorastreamento" id="numerorastreamento" name="numerorastreamento" />
                
                  <input type="submit" class="enviar" id="enviar" />
                </form>
              </span>
              <div class="icon"></div>

            </div><!-- FIM RASTREAMENTO -->

          </div><!-- FIM MEIO -->

        </li><!-- FIM PRODUTO -->

        <!-- PRODUTO -->
        <li>

          <a href="#">
            <div class="foto">
              <img src="images/thumb-produto-1.jpg" alt="" title="" />
            </div>
          </a>

          <!-- FIM MEIO -->
          <div class="meio">
            <a href="#">
              <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
              <div class="ref">ref. 34562</div>
            </a>

            <!-- PREÇO -->
            <div id="preco">

              <a href="#">

                <div class="de">de: <span>R$ 199,00</span></div>
                <div class="atual">Por: <span>R$ 169,00</span></div>

              </a>

            </div><!-- FIM PREÇO -->

          </div><!-- FIM MEIO -->

          <!-- DIREITA -->
          <div class="direita">

            <!-- EDITAR -->
            <div id="editar" class="botao">

              <a href="#">
                Editar
              </a>
              
            </div><!-- FIM EDITAR -->

            <!-- DELETAR -->
            <div id="close" class="botao">Deletar</div><!-- FIM DELETAR -->

          </div><!-- FIM DIREITA -->

        </li><!-- FIM PRODUTO -->   

        <!-- PRODUTO -->
        <li>

          <a href="#">
            <div class="flag-nunca-usado-thumb">Nunca usado</div>

            <div class="foto">
              <img src="images/thumb-produto-1.jpg" alt="" title="" />
            </div>
          </a>

          <!-- FIM MEIO -->
          <div class="meio">
            <a href="#">
              <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
              <div class="ref">ref. 34562</div>
            </a>

            <!-- PREÇO -->
            <div id="preco">

              <a href="#">

                <div class="de">de: <span>R$ 199,00</span></div>
                <div class="atual">Por: <span>R$ 169,00</span></div>

              </a>

            </div><!-- FIM PREÇO -->

            <!-- RASTREAMENTO -->
            <div id="rastreamento">

              <span id="tooltip-rastreamento" class="tooltip-rastreamento">
                
                <form>
                  <div class="data">Data de envio</div>
                  <input type="text" class="dataenvio" id="dataenvio" name="dataenvio" />

                  <div class="numero">Número de restreamento</div>
                  <input type="text" class="numerorastreamento" id="numerorastreamento" name="numerorastreamento" />
                
                  <input type="submit" class="enviar" id="enviar" />
                </form>
              </span>
              <div class="icon"></div>

            </div><!-- FIM RASTREAMENTO -->

          </div><!-- FIM MEIO -->

        </li><!-- FIM PRODUTO -->

        <!-- PRODUTO -->
        <li>

          <a href="#">
            <div class="foto">
              <img src="images/thumb-produto-1.jpg" alt="" title="" />
            </div>
          </a>

          <!-- FIM MEIO -->
          <div class="meio">
            <a href="#">
              <h2 class="tituloproduto">Lorem ipsum dolor sit amet consectur lorem</h2>
              <div class="ref">ref. 34562</div>
            </a>

            <!-- PREÇO -->
            <div id="preco">

              <a href="#">

                <div class="de">de: <span>R$ 199,00</span></div>
                <div class="atual">Por: <span>R$ 169,00</span></div>

              </a>

            </div><!-- FIM PREÇO -->

          </div><!-- FIM MEIO -->

          <!-- DIREITA -->
          <div class="direita">

            <!-- EDITAR -->
            <div id="editar" class="botao">

              <a href="#">
                Editar
              </a>
              
            </div><!-- FIM EDITAR -->

            <!-- DELETAR -->
            <div id="close" class="botao">Deletar</div><!-- FIM DELETAR -->

          </div><!-- FIM DIREITA -->

        </li><!-- FIM PRODUTO -->      

      </ul>

      
    </div><!-- FIM RIGHT -->

  <?php include('footer.php') ?>
