<!-- SIDEBAR -->
    <div id="sidebar">
      
      <!-- AVATAR E NOME -->
      <div class="avatar"></div>
      <h3 class="nome">Mariana dos Santos Villar de Magalhães</h3><!-- FIM AVATAR E NOME -->

      <!-- PRODUTOS COMPRADOS -->
      <div class="retangulorosa">

        <div class="numero">000</div>
        <div class="traco"></div>
        <div class="texto">PRODUTOS<br />COMPRADOS</div>

      </div><!-- FIM PRODUTOS COMPRADOS -->

      <!-- PRODUTOS VENDIDOS -->
      <div class="retangulorosa">

        <div class="numero">000</div>
        <div class="traco"></div>
        <div class="texto">PRODUTOS<br />VENDIDOS</div>

      </div><!-- FIM PRODUTOS VENDIDOS -->

      <!-- AVALIACAO -->
      <div class="avaliacao">

        <div class="qualificacao">Sua qualificação</div>

        <ul class="estrelinhas"><li class="active"></li><li class="active"></li><li></li><li></li><li></li></ul>

        <div class="avaliacoes">135 avaliações</div>

      </div><!-- FIM AVALIACAO -->

      <!-- ORIGEM DO PRODUTO -->
      <div id="origemdoproduto">

        <h3>origem do produto</h3>
        <div class="retangulo">São Paulo, SP</div>

      </div><!-- FIM ORIGEM DO PRODUTO -->

    </div><!-- FIM SIDEBAR -->