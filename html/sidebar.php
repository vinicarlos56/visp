<!-- SIDEBAR -->
    <div id="sidebar">

      <h1>NAVEGUE POR AQUI</h1>
      <!-- ACCORDION CATEGORIAS -->
      <div id="accordiondepartamento" class="accordionWrapper" style="width:230px;">

        <div class="set roupas">
          <div class="title">
            <div class="icon"></div>
            <h2>ROUPAS</h2>
          </div>
          <div class="content">
            <ul>
              <li><a href="#">Meninos</a></li>
              <li><a href="#">Meninas</a></li>
              <li><a href="#">Unissex</a></li>
              <li><a href="#">Gêmeos</a></li>
            </ul>
          </div>
        </div>

        <div class="set sapatos">
          <div class="title">
            <div class="icon"></div>
            <h2>SAPATOS</h2>
          </div>
          <div class="content">
            <ul>
              <li><a href="#">Meninos</a></li>
              <li><a href="#">Meninas</a></li>
              <li><a href="#">Unissex</a></li>
              <li><a href="#">Gêmeos</a></li>
            </ul>
          </div>
        </div>

        <div class="set acessorios">
          <div class="title">
            <div class="icon"></div>
            <h2>ACESSÓRIOS</h2>
          </div>
          <div class="content">
            <ul>
              <li><a href="#">Meninos</a></li>
              <li><a href="#">Meninas</a></li>
              <li><a href="#">Unissex</a></li>
              <li><a href="#">Gêmeos</a></li>
            </ul>
          </div>
        </div>

        <div class="set brinquedos">
          <div class="title">
            <div class="icon"></div>
            <h2>BRINQUEDOS</h2>
          </div>
          <div class="content">
            <ul>
              <li><a href="#">Meninos</a></li>
              <li><a href="#">Meninas</a></li>
              <li><a href="#">Unissex</a></li>
              <li><a href="#">Gêmeos</a></li>
            </ul>
          </div>
        </div>

        <div class="set livrosedvds">
          <div class="title">
            <div class="icon"></div>
            <h2>LIVROS E DVDS</h2>
          </div>
          <div class="content">
            <ul>
              <li><a href="#">Meninos</a></li>
              <li><a href="#">Meninas</a></li>
              <li><a href="#">Unissex</a></li>
              <li><a href="#">Gêmeos</a></li>
            </ul>
          </div>
        </div>

        <div class="set enxoval">
          <div class="title">
            <div class="icon"></div>
            <h2>ENXOVAL</h2>
          </div>
          <div class="content">
            <ul>
              <li><a href="#">Meninos</a></li>
              <li><a href="#">Meninas</a></li>
              <li><a href="#">Unissex</a></li>
              <li><a href="#">Gêmeos</a></li>
            </ul>
          </div>
        </div>

        <div class="set moveisedecoracao">
          <div class="title">
            <div class="icon"></div>
            <h2>MÓVEIS E DECORAÇÃO</h2>
          </div>
          <div class="content">
            <ul>
              <li><a href="#">Meninos</a></li>
              <li><a href="#">Meninas</a></li>
              <li><a href="#">Unissex</a></li>
              <li><a href="#">Gêmeos</a></li>
            </ul>
          </div>
        </div>

        <div class="set cadeirasecarrinhos">
          <div class="title">
            <div class="icon"></div>
            <h2>CADEIRAS E CARRINHOS</h2>
          </div>
          <div class="content">
            <ul>
              <li><a href="#">Meninos</a></li>
              <li><a href="#">Meninas</a></li>
              <li><a href="#">Unissex</a></li>
              <li><a href="#">Gêmeos</a></li>
            </ul>
          </div>
        </div>

      </div><!--FIM DE ACCORDEON DE DEPARTAMENTOS -->

      <h1>FILTRAR POR</h1>
      <!-- FILTRAR POR -->
      <div class="filtrar-por">

        <h2>TIPOS DE ENVIO</h2>
        <ul>
          <li><a href="#">Frete Grátis</a></li>
          <li><a href="#">A combinar</a></li>
          <li><a href="#">Frete Normal</a></li>
        </ul>

        <h2>POR ESTADO</h2>
        <div class="mascaraestados"><span>selecione o estado</span></div>
        <select name="estados">
          <option>selecione o estado</option>
          <option value="AC">Acre</option>
          <option value="AL">Alagoas</option>
          <option value="AM">Amazonas</option>
          <option value="AP">Amapá</option>
          <option value="BA">Bahia</option>
          <option value="CE">Ceará</option>
          <option value="DF">Distrito Federal</option>
          <option value="ES">Espirito Santo</option>
          <option value="GO">Goiás</option>
          <option value="MA">Maranhão</option>
          <option value="MG">Minas Gerais</option>
          <option value="MS">Mato Grosso do Sul</option>
          <option value="MT">Mato Grosso</option>
          <option value="PA">Pará</option>
          <option value="PB">Paraíba</option>
          <option value="PE">Pernambuco</option>
          <option value="PI">Piauí</option>
          <option value="PR">Paraná</option>
          <option value="RJ">Rio de Janeiro</option>
          <option value="RN">Rio Grande do Norte</option>
          <option value="RO">Rondônia</option>
          <option value="RR">Roraima</option>
          <option value="RS">Rio Grande do Sul</option>
          <option value="SC">Santa Catarina</option>
          <option value="SE">Sergipe</option>
          <option value="SP">São Paulo</option>
          <option value="TO">Tocantins</option>
        </select>

        <h2>PREÇO</h2>
        <form class="de-por">
          <label class="de">de R$</label>
          <label class="por">até R$</label>

          <input type="text" class="de" name="de" />
          <input type="text" class="por" name="por" />
        </form>
        <ul>
          <li><a href="#">Até R$ 50</a></li>
          <li><a href="#">R$ 50 a R$ 100</a></li>
          <li><a href="#">R$ 100 a R$ 200</a></li>
          <li><a href="#">Acima de R$ 200</a></li>
        </ul>

        <h2>CONDIÇÃO</h2>
        <ul>
          <li><a href="#">Novo</a></li>
          <li><a href="#">Usado</a></li>
        </ul>

      </div><!-- FIM FILTRAR POR -->

      <!-- FACEBOOK -->
      <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FVispStore&amp;width=250&amp;height=258&amp;show_faces=true&amp;colorscheme=light&amp;stream=false&amp;border_color&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:250px; height:291px; margin-top: 20px;" allowTransparency="true"></iframe>
      <!-- FIM FACEBOOK -->

      <!-- NEWSLETTER -->
      <div class="newsletter">
        <h2>RECEBA A VIsP POR EMAIL</h2>
        <p>lorem ipsum dolor sit amet consectur</p>
        <form class="news">
          <input type="text" name="email" class="email" placeholder="Digite seu email" />
          <input type="submit" class="assinar" />
      </div><!-- FIM NEWSLETTER -->

      <!-- BANNER -->
      <div class="banner">
        <a href="#">
          <img src="images/bannerexemplo.jpg" alt="" title="" />
        </a>
      </div><!-- FIM BANNER -->

      <!-- BANNER -->
      <div class="banner">
        <a href="#">
          <img src="images/bannerexemplo.jpg" alt="" title="" />
        </a>
      </div><!-- FIM BANNER -->

    </div><!-- FIM SIDEBAR -->