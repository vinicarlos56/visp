  <?php include('header.php') ?>

  <!-- CENTRO -->
  <div class="centralizando content minhavitrine">

    <h2>MINHA VITRINE VIsP</h2>

    <?php include('barra-de-vitrine.php') ?>

    <?php include('sidebar-minha-vitrine.php') ?>

    <!-- MAIN -->
    <div id="main">

      <h2>Sua loja ainda está desabilitada.</h2>
      <h3>Por favor, registre-se no MoIP ou cadastre sua conta na VIsP.</h3>
      <p>Sem uma conta MoIP você nao pode habilitar sua loja e seus produtos não estarão visíveis na VIsP. Para validar uma conta existente ou cadastrar uma nova, <a href="#">Clique aqui.</a></p>

    </div><!-- FIM MAIN -->   

  <?php include('footer.php') ?>
