  <?php include('header.php') ?>

  <!-- CENTRO -->
  <div class="centralizando content">

    <div class="querovender">

      <div class="a-1">

      	<div class="imagesprite querovenderimg1"></div>

        <h1>QUERO VENDER</h1>

        <h2 class="oqueeavisp">O QUE É A VIsP</h2>

        <p>Os filhos, sobrinhos, enteados, enfim todas as crianças a nossa volta são pessoinhas extremamente importantes  e que consomem tanto. Nossa!</p>

        <p>Além disso, eles crescem tão rápido que as vezes nem dá tempo de usar tudo que compramos ou usam poucas vezes e aí? São coisinhas tão lindas e quase novas? Ou são “coisonas” ocupando o canto do quarto, da sala ou guardadas no baú.</p>

        <p>Que tal fazer uma graninha extra aquele brinquedo diferente? A VIsP vai te ajudar!</p>

        <p>Somos uma comunidade de consumo colaborativo para compra e venda totalmente voltada para o público Infantil.  Aqui você poderá anunciar de tudo ligado a crianças (carrinhos, berço, roupas, sapatos, acessórios, cadeira de alimentação, brinquedos, tudo que você achar que vale a pena vender).</p>

      </div>

      <div class="a-2">

        <div class="imagesprite querovenderimg2"></div>

        <h2 class="quantocusta">QUANTO CUSTA PARA VENDER NA VIsP?</h2>

        <p>Você não paga para se cadastrar e nem para fazer a sua página com os seus produtos. Se vender, a comissão é de 15% + R$2,15.</p>

        <p>P.s. Avaliamos todos os produtos cadastrados antes de ir ao ar, assim conseguimos garantir que todos são produtos voltados para o público infantil e o padrão do site. ;)  Para vender, <a href="#">clique aqui</a> e faça seu cadastro, é rápido!</p>

        <p>Você também poderá comprar produtos seminovos (as vezes novos) por um bom preço, além de estar contribuindo com o meio ambiente. Viva o consumo sustentável!</p>

      </div>

      <ul class="a-3">

        <h2 class="seismotivos">SEIS MOTIVOS PARA VOCÊ MONTAR A SUA vitrine de produtos</h2>
 
        <li>
          <div class="icon primeiro"></div>
          <span>
            Facilidades para<br />
            você criar a<br />
            sua vitrine de<br />
            produtos
          </span>
        </li>
        <li>
          <div class="icon segundo"></div>
          <span>
            O cadastro<br />
            é gratuito
          </span>
        </li>
        <li>
          <div class="icon terceiro"></div>
          <span>
            e ainda ganhe<br /> 
            um dinheirinho
          </span>
        </li>
        <li>
          <div class="icon quarto"></div>
          <span>
            Segurança. Seus<br />
            dados estão<br />
            protegidos
          </span>
        </li>
        <li>
          <div class="icon quinto"></div>
          <span>
            AJUDAMOS VOCÊ<br />
            QUANDO PRECISAR
          </span>
        </li>
        <li>
          <div class="icon sexto"></div>
          <span>
            Você faz parte de<br />
            uma comunidade<br />
            que apoia o<br />
            consumo<br />
            sustentável
          </span>
        </li>

      </ul>

    </div>


  <?php include('footer.php') ?>
