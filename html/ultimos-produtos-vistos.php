  <?php include('header.php') ?>

  <!-- CENTRO -->
  <div class="centralizando content minhavitrine">

    <h2>MINHA VITRINE VIsP</h2>

    <?php include('barra-de-vitrine.php') ?>

    <?php include('sidebar-minha-vitrine.php') ?>

    <!-- PRATELEIRA -->
    <div id="right">
      <h1>últimos produtos vistos</h1>
      <ul class="prateleira">

        <li>
          <span id="tooltip" class="tooltip">
            <div class="nome">Nome do produto</div>
            <div class="usadoounovo">Produto usado</div>
          </span>
          <a href="#">
            <div class="informacao">
              <div class="tamanho menino">
                P
              </div>
              <div class="preco menino">
                R$ 199,00
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho menina">
                PP
              </div>
              <div class="preco menina">
                R$ 19,00
              </div>
            </div>
            <img src="images/produto-exemplo-2.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho unisses">
                GG
              </div>
              <div class="preco unisses">
                R$ 1.999,00
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho semresposta">
                sem resposta
              </div>
              <div class="preco unisses">
                R$ 9,30
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho menino">
                M
              </div>
              <div class="preco menino">
                R$ 10,00
              </div>
            </div>
            <img src="images/produto-exemplo-2.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho unisses">
                G
              </div>
              <div class="preco unisses">
                R$ 939,00
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho menino">
                P
              </div>
              <div class="preco menino">
                R$ 199,00
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho menina">
                PP
              </div>
              <div class="preco menina">
                R$ 19,00
              </div>
            </div>
            <img src="images/produto-exemplo-2.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho unisses">
                GG
              </div>
              <div class="preco unisses">
                R$ 1.999,00
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho semresposta">
                sem resposta
              </div>
              <div class="preco unisses">
                R$ 9,30
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho menino">
                M
              </div>
              <div class="preco menino">
                R$ 10,00
              </div>
            </div>
            <img src="images/produto-exemplo-2.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho unisses">
                G
              </div>
              <div class="preco unisses">
                R$ 939,00
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho semresposta">
                sem resposta
              </div>
              <div class="preco unisses">
                R$ 9,30
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho menino">
                M
              </div>
              <div class="preco menino">
                R$ 10,00
              </div>
            </div>
            <img src="images/produto-exemplo-2.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho unisses">
                G
              </div>
              <div class="preco unisses">
                R$ 939,00
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho semresposta">
                sem resposta
              </div>
              <div class="preco unisses">
                R$ 9,30
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho menino">
                M
              </div>
              <div class="preco menino">
                R$ 10,00
              </div>
            </div>
            <img src="images/produto-exemplo-2.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho unisses">
                G
              </div>
              <div class="preco unisses">
                R$ 939,00
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

      </ul>

      <div class="exibirmais">
        <div class="mais">exibir mais produtos</div>
      </div>

    </div><!-- FIM MAIN -->   

  <?php include('footer.php') ?>
