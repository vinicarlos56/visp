  <?php include('header.php') ?>

  <!-- CENTRO -->
  <div class="centralizando content">

    <div class="comofunciona">

		<h1>CONTATO</h1>
     
		<p>Tem dúvidas ou sugestões sobre os nossos serviços? Precisa da nossa ajuda? Ou tem interesse em alguma parceria? Confira aqui os nossos contatos.</p>

		<p>Se tiver dúvida sobre alguma compra específica no site, sugerimos entrar em contato diretamente com o vendedor, ok?</p>
    	

    	<br /><br />
		<strong>Ajuda e suporte</strong>

		<p>Antes de entrar em contato conosco para pedir ajuda, veja se na nossa área de Ajuda já não tem alguma informação sobre o assunto. Possuimos uma grande quantidade de tópicos detalhados que vão te ajudar na maioria dos problemas.</p>
    	
    	<br /><br />
    	<strong>Contato Direto</strong>

    	<p>Se você não conseguiu resolver seu problema ou precisa falar com a gente, você pode nos mandar um email: <a href="mailto:contato@vispstore.com">contato@vispstore.com</a></p>

    	<br />
    	<p>Só não se esqueça de colocar os suas informações:<br/>
		•	Seu nome:<br />
		•	Seu email:<br />
		•	Qual é o assunto?<br /><br />

		Faremos o possível para responder o mais rápido possível! ;)</p>
    </div>


  <?php include('footer.php') ?>
