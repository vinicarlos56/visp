  <?php include('header.php') ?>

  <!-- CENTRO -->
  <div class="centralizando content">

    <form class="criarconta">
      
      <ul class="teladecadastro">

        <li>

          <div class="numeroli">1</div>

          <div class="linhadivisoria"></div>

          <h1>Seus dados para acesso</h1>

          <div class="inputs">
            <input type="text" name="email" id="email" class="email" placeholder="Email" />
            <input type="text" name="confirmaremail" id="confirmaremail" class="confirmaremail" placeholder="Confirmar email" />
            <input type="password" name="senha" id="senha" class="senha" placeholder="Senha" />
            <input type="password" name="confirmarsenha" id="confirmarsenha" class="confirmarsenha" placeholder="Confirmar senha" />
            <input type="text" name="usuario" id="usuario" class="usuario" placeholder="Usuário" />
            <label>.vispstore.com</label>
          </div>

        </li>

        <li>

          <div class="numeroli">2</div>

          <div class="linhadivisoria"></div>

          <h1>Seus dados para acesso</h1>

          <div class="inputs">
            <input type="text" name="nome" id="nome" class="nome" placeholder="Nome" />
            <input type="text" name="datadenascimento" id="datadenascimento" class="datadenascimento" placeholder="Data de nascimento" />
            <div class="mascaraselectsexo">Sexo</div>
            <select name="sexo" class="sexo" id="sexo">
              <option></option>
              <option value="Masculino">Masculino</option>
              <option value="Feminino">Feminino</option>
            </select>
            <input type="text" name="telefone" id="telefone" class="telefone" placeholder="Telefone" />
            <input type="text" name="celular" id="celular" class="celular" placeholder="Celular" />

            <div class="cpfoucnpj">
              <input type="radio" name="tipo" id="radiocpf" checked />
              <label class="cpf">CPF</label>

              <input type="radio" name="tipo" id="radiocnpj" />
              <label class="cnpj">CNPJ</label>      
            </div>

            <input type="text" name="cpf" id="cpf" class="cpf" placeholder="CPF" />
            <input type="text" name="cnpj" id="cnpj" class="cnpj" placeholder="CNPJ" style="display: none" />
          </div>

          <p class="termo">Ao clicar em “Criar conta” você está<br />concordando com os termos de uso</p>

        </li>

        <li>

          <div class="numeroli">3</div>

          <h1>Seus dados para acesso</h1>

          <div class="inputs">
            <input type="text" name="cep" id="cep" class="cep" placeholder="Cep" />
            <input type="text" name="endereco" id="endereco" class="endereco" placeholder="Endereço" />
            <input type="text" name="numero" id="numero" class="numero" placeholder="Número" onkeypress='return SomenteNumero(event)' />
            <input type="text" name="complemento" id="complemento" class="complemento" placeholder="Complemento" />
            <input type="text" name="bairro" id="bairro" class="bairro" placeholder="Bairro" />
            <div class="mascaraselectuf">UF</div>
            <select name="uf" class="uf" id="uf">
            </select>
            <div class="mascaraselectcidade">Cidade</div>
            <select name="cidade" class="cidade" id="cidade">
            </select>
          </div>

        </li>

      </ul>

      <div class="botao criarconta"><a href="#">Criar Conta</a></div>

    </form>

    <!-- UF E CIDADE PAG CADASTRO -->
    <script language="JavaScript" type="text/javascript" src="js/cidades-estados.js"></script>
    <script type="text/javascript">
    new dgCidadesEstados({
      cidade: document.getElementById('cidade'),
      estado: document.getElementById('uf')
    });
    </script>
    <!-- FIM UF E CIDADE PAG CADASTRO -->

  <?php include('footer.php') ?>
