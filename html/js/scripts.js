$(document).ready( function(){

// Slider home
  royalSliderInstance = $('#slider').royalSlider({  
      imageAlignCenter:true,
      hideArrowOnLastSlide:true,
      slideshowEnabled: true,           
      slideshowDelay:8000,
      keyboardNavEnabled:true
  }).data('royalSlider'); 
// FIM Slider home

//Chamada Accordion
  $("#accordiondepartamento").msAccordion({vertical:true});
//FIM Chamada Accordion

//Chamada mascada dinheiro
  $(".de").maskMoney();
  $(".por").maskMoney();
//FIM Chamada mascada dinheiro

//TOOLTIP PRATELEIRA
  $(".prateleira li").ezpz_tooltip({
    contentId: 'tooltip',
    showContent: function(content) {
      content.fadeIn(600);
    },
    hideContent: function(content) {
      content.stop(true, true).fadeOut(100);
    }
  });
//FIM TOOLTIP PRATELEIRA

//TOOLTIP TAMANHO PAG CADASTRO PROUTO
  $("input#tamanho").ezpz_tooltip({
    contentId: 'tooltip',
    showContent: function(content) {
      content.fadeIn(600);
    },
    hideContent: function(content) {
      content.stop(true, true).fadeOut(100);
    }
  });
//FIM TOOLTIP PRATELEIRA

//BARRA DE CADASTRESE
  setTimeout(function(){
    $('#recebanovidades').animate({
      marginTop: '0px'
    },500)
  },10000);
  setTimeout(function(){
    $('#recebanovidades').animate({
      marginTop: '-160px'
    },500)
  },80000);
  $('#recebanovidades .fechar').click(function(){
    $('#recebanovidades').animate({
      marginTop: '-160px'
    },500)
  });
//FIM BARRA DE CADASTRESE

//ALTERANDO CHECKBOX
  checkButtons = $('input[type="checkbox"]');

  checkButtons.each( function(){
      $(this).after('<span rel="' + this.id + '" class="footage-checkbox"></span>').css('display','none');
  });

  checkButtons.change( function(){
      this.checked = ( this.checked ) ? false:true;

      if( this.checked )
          $('span.footage-checkbox[rel="' + this.id + '"]').addClass('checked');
      else
          $('span.footage-checkbox[rel="' + this.id + '"]').removeClass('checked');
  });

  $('span.footage-checkbox').click( function(){
      tempChk = $('input#'+$(this).attr('rel')).change();
  });

  $('label').click( function(){
      tempChk = $('input#'+$(this).attr('for')).change();
  });
//FIM ALTERANDO CHECKBOX

//SELECT SEXO PÁGINA CADASTRO
$("select#sexo").change(function () { 
    
  var selectsexo = "";
  $("select#sexo option:selected").each(function () {
        selectsexo += $(this).val() + " ";
  });
  $(".mascaraselectsexo").text(selectsexo);

});
//FIM SELECT SEXO PÁGINA CADASTRO

//SELECT SEXO PÁGINA CADASTRO
$("select#sexofilho").change(function () { 
    
  var selectsexofilho = "";
  $("select#sexofilho option:selected").each(function () {
        selectsexofilho += $(this).val() + " ";
  });
  $(".mascaraselectsexofilho").text(selectsexofilho);

});
//FIM SELECT SEXO PÁGINA CADASTRO

//UF E CIDADE PÁGINA DE CADASTRO
$("select#uf").change(function () { 
  
  $(".mascaraselectcidade").html('Carregando...');
  
  var selectuf = "";
  $("select#uf option:selected").each(function () {
        selectuf += $(this).val() + " ";
  });
  $(".mascaraselectuf").text(selectuf);

  setTimeout(function(){
    $(".mascaraselectcidade").html('Selecione cidade');
  },1000)

});
$("select#cidade").change(function () { 
    
  var selectcidade = "";
  $("select#cidade option:selected").each(function () {
        selectcidade += $(this).val() + " ";
  });
  $(".mascaraselectcidade").text(selectcidade);

});
// FIM UF E CIDADE PÁGINA DE CADASTRO

//SELECT ORDENAR POR PÁGINA CATEGORIA
$("select#ordenarpor").change(function () { 
    
  var selectordenarpor = "";
  $("select#ordenarpor option:selected").each(function () {
        selectordenarpor += $(this).val() + " ";
  });
  $(".mascaraordenarpor span").text(selectordenarpor);

});
//FIM SELECT ORDENAR POR PÁGINA CATEGORIA

//SELECT QUANTIDADE CHECKOUT
$("select#quantidade").change(function () { 
    
  var selectordenarpor = "";
  $("select#quantidade option:selected").each(function () {
        selectordenarpor += $(this).val() + " ";
  });
  $(".mascaradequantidade").text(selectordenarpor);

});
//FIM QUANTIDADE CHECKOUT

//SELECT FRETE CHECKOUT
$("select#tipodefrete").change(function () { 
    
  var selectordenarpor = "";
  $("select#tipodefrete option:selected").each(function () {
        selectordenarpor += $(this).val() + " ";
  });
  $(".mascaratipodefrete").text(selectordenarpor);

});
//FIM FRETE CHECKOUT

//SELECT CATEGORIA
$("select#categoria").change(function () { 
    
  var categoria = "";
  $("select#categoria option:selected").each(function () {
        categoria += $(this).val() + " ";
  });
  $(".mascaracategoria").text(categoria);

});
//FIM CATEGORIA

//SELECT QUANTIDADE
$("select#quantidade").change(function () { 
    
  var quantidade = "";
  $("select#quantidade option:selected").each(function () {
        quantidade += $(this).val() + " ";
  });
  $(".mascaraquantidade").text(quantidade);

});
//FIM CATEGORIA

//SELECT CATEGORIA
$("select#subcategoria").change(function () { 
    
  var subcategoria = "";
  $("select#subcategoria option:selected").each(function () {
        subcategoria += $(this).val() + " ";
  });
  $(".mascarasubcategoria").text(subcategoria);

});
//FIM CATEGORIA

// MASCARAS PADRÕES
  $("#telefone").mask("(99) 9999-9999");
  $("#celular").mask("(99) 9999-9999");
  $("#cpf").mask("999.999.999-99");
  $("#cnpj").mask("99.999.999/9999-99");  
  $("#cep").mask("99999-999");
  $("#datadenascimento").mask("99/99/9999");
  $("#dataenvio").mask("99/99/9999");

  $("#precooriginal").maskMoney({symbol:'R$ ', showSymbol:true, thousands:'.', decimal:',', symbolStay: true});
  $("#seupreco").maskMoney({symbol:'R$ ', showSymbol:true, thousands:'.', decimal:',', symbolStay: true});  
// FIM MASCARAS PADRÕES

// CPF OU CNJP
  $('#radiocnpj').click(function(){
    $('#cpf').hide();
    $('#cnpj').show();
  });
  $('#radiocpf').click(function(){
    $('#cnpj').hide();
    $('#cpf').show();
  });

// MASCARA DE FRETES
var mascarainputfrete = $('#container .dadosdoproduto .segundonivel ul.tabeladefrete li .mascarainput');
$('#container .dadosdoproduto .segundonivel ul.tabeladefrete li').click(function(){
  $(mascarainputfrete).removeClass('ativo');
  $(this).find('div').addClass('ativo');
});
// FIM MASCARA DE FRETES

$('.fileinput-button').click(function(){
  $('.fileupload-buttonbar').css('height', '300px');
})

//SLIDER PAG PRODUTO
$(function(){
  $('#produtoimagem').slides({
    preload: true,
    preloadImage: 'images/loading.gif',
    effect: 'slide, fade',
    crossfade: true,
    slideSpeed: 200,
    fadeSpeed: 600,
    generateNextPrev: true,
    generatePagination: false
  });
});
//FIM SLIDER PAG PRODUTO

//ESTRELINHAS
$('ol.estrelinhas li').click(function(){  
  var $this = $( this );  
  var ol = $this.parent('ol');  
  var rating = $this.index()+1;  

  if( $this.hasClass('active') && !$this.next('li').hasClass('active') ){  
    $( ol ).find('li').removeClass('active');  
    rating = 0;  
  }  
  else{  
    $( ol ).find('li').removeClass('active');  
    for( var i=0; i<rating; i++ ){  
      $( ol ).find('li').eq( i ).addClass('active');  
    };  
  }  
});
//FIM ESTRELINHAS

//CONFIRMAR SENHA
$('#alterar').click(function(){
  $('.confirmarsenha').fadeIn('slow');
  $('html, body').animate({
        scrollTop: $("#header").offset().top
    }, 1000);
});
$('.confirmarsenha .close').click(function(){
  $('.confirmarsenha').fadeOut('slow');
});

//ESQUECI MINHA SENHA
$('#container ul.teladelogin li .esquecisenha').click(function(){
  $('.esqueciminhasenha').fadeIn('slow');
  $('html, body').animate({
        scrollTop: $("#header").offset().top
    }, 1000);
});
$('.esqueciminhasenha .close').click(function(){
  $('.esqueciminhasenha').fadeOut('slow');
});

//DELETAR PRODUTO DE FAVORITOS
$('#container .minhavitrine.favoritos #right ul.prateleira li .close').click(function(){
  $(this).parent().remove();
});

//DELETAR PRODUTO MINHAS VENAS
$('#container .minhavitrine.minhasvendas ul.produtos li .direita #close').click(function(){
  $(this).parent().parent().remove();
});

});