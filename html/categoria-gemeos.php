  <?php include('header.php') ?>

  <!-- BREADCRUMB -->
  <div id="breadcrumb" class="gemeos">

    <a href="#">ROUPAS</a> > GÊMEOS

    <div class="ordenarpor">
      <div class="mascaraordenarpor"><span>ORDENAR POR</span> <div class="seta"></div></div>
      <select id="ordenarpor">
        <option>Ordenar por</option>
        <option>Mais recente</option>
        <option>Mais antigo</option>
        <option>Menor preço</option>
        <option>Maior preço</option>
      </select>
    </div>

  </div><!-- FIM BREADCRUMB -->

  <!-- CENTRO -->
  <div class="centralizando content gemeos">

    

    <?php include('sidebar.php') ?>

    <!-- PRATELEIRA -->
    <div id="right">
      <h1>produtos em destaque</h1>
      <ul class="prateleira">

        <li>
          <span id="tooltip" class="tooltip">
            <div class="nome">Nome do produto</div>
            <div class="usadoounovo">Produto usado</div>
          </span>
          <a href="#">
            <div class="informacao">
              <div class="tamanho gemeos">
                P
              </div>
              <div class="preco gemeos">
                R$ 199,00
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho gemeos">
                PP
              </div>
              <div class="preco gemeos">
                R$ 19,00
              </div>
            </div>
            <img src="images/produto-exemplo-2.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho gemeos">
                GG
              </div>
              <div class="preco gemeos">
                R$ 1.999,00
              </div>
            </div>
            <img src="images/produto-exemplo-3.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho semresposta">
                sem resposta
              </div>
              <div class="preco gemeos">
                R$ 9,30
              </div>
            </div>
            <img src="images/produto-exemplo-3.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho gemeos">
                M
              </div>
              <div class="preco gemeos">
                R$ 10,00
              </div>
            </div>
            <img src="images/produto-exemplo-2.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho gemeos">
                G
              </div>
              <div class="preco gemeos">
                R$ 939,00
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho gemeos">
                P
              </div>
              <div class="preco gemeos">
                R$ 199,00
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho gemeos">
                PP
              </div>
              <div class="preco gemeos">
                R$ 19,00
              </div>
            </div>
            <img src="images/produto-exemplo-2.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho gemeos">
                GG
              </div>
              <div class="preco gemeos">
                R$ 1.999,00
              </div>
            </div>
            <img src="images/produto-exemplo-3.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho semresposta">
                sem resposta
              </div>
              <div class="preco gemeos">
                R$ 9,30
              </div>
            </div>
            <img src="images/produto-exemplo-3.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho gemeos">
                M
              </div>
              <div class="preco gemeos">
                R$ 10,00
              </div>
            </div>
            <img src="images/produto-exemplo-2.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho gemeos">
                G
              </div>
              <div class="preco gemeos">
                R$ 939,00
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho semresposta">
                sem resposta
              </div>
              <div class="preco gemeos">
                R$ 9,30
              </div>
            </div>
            <img src="images/produto-exemplo-3.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho gemeos">
                M
              </div>
              <div class="preco gemeos">
                R$ 10,00
              </div>
            </div>
            <img src="images/produto-exemplo-2.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho gemeos">
                G
              </div>
              <div class="preco gemeos">
                R$ 939,00
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho semresposta">
                sem resposta
              </div>
              <div class="preco gemeos">
                R$ 9,30
              </div>
            </div>
            <img src="images/produto-exemplo-3.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho gemeos">
                M
              </div>
              <div class="preco gemeos">
                R$ 10,00
              </div>
            </div>
            <img src="images/produto-exemplo-2.jpg" alt="" title="" />
          </a>
        </li>

        <li>
          <a href="#">
            <div class="informacao">
              <div class="tamanho gemeos">
                G
              </div>
              <div class="preco gemeos">
                R$ 939,00
              </div>
            </div>
            <img src="images/produto-exemplo-1.jpg" alt="" title="" />
          </a>
        </li>

      </ul>

      <div class="exibirmais">
        <div class="mais">exibir mais produtos</div>
      </div>

    </div><!-- FIM PRATELEIRA -->

  <?php include('footer.php') ?>
