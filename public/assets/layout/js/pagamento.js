var success = function(data){

	var sale_id = $('#sale_id').val();

	$.ajax({
		url:'/visp/public/finish-sale',
		type:'POST',
		dataType:'json',
		data:{
			sale_id:sale_id,
			sale_data:data,
			payment_way: get_payment_way()
		},
		success: function(json){
			console.log(json);
			if(json.status){
				alert('Pagamento realizado com sucesso!\n');
				window.location.href='/visp/public/minhas-compras';
			}else{
				alert(json.error);
			}
		}
	});
};

var failReturn = function(data) {
	alert('Falha\n' + JSON.stringify(data));
};

payBillet = function() {
	var settings = {
		"Forma": "BoletoBancario"
	}
	MoipWidget(settings);
}

payCreditcard = function(){

	var institution 	= $('#creditcardinstitution').val();
	var parcels  		= $('#parcelsnumber').val();
	var creditcardnumber= $('#creditcardnumber').val();
	var expiration		= $('#month').val() + '/' + $('#year').val();
	var securitynumber	= $('#securitynumber').val();
	var name 			= $('#cobranca_nome').val();
	var birthday		= $('#cobranca_datadenascimento').val();
	var phone			= $('#cobranca_telefone').val();
	var rg 				= $('#cpf').val();

	var settings = {
		"Forma": "CartaoCredito",
		"Instituicao": institution,
		"Parcelas": parcels,
		"Recebimento": "AVista",
		"CartaoCredito": {
			"Numero": creditcardnumber,
			"Expiracao": expiration,
			"CodigoSeguranca": securitynumber,
			"Portador": {
				"Nome": name,
				"DataNascimento": birthday,
				"Telefone": phone,
				"Identidade": rg
			}
		}
	}

	console.log(settings);

	MoipWidget(settings);
}

payBankTransfer = function(){
	var option;
	$.each($('input[name=\'banktransfer\']'),function(i,obj){
		if(obj.checked)
			option = obj.value;
	});

	var settings = {
		"Forma": "DebitoBancario",
		"Instituicao": option,
	}			

	MoipWidget(settings);
}

function get_payment_way () {
	var option;  

	$.each($('input[name=\'paymentway\']'),function(i,obj){
		if(obj.checked)
			option = obj.value;
	});

	return option;
}

$(document).ready(function(){

	$('.optiondiv').hide();

	$('input[name=\'paymentway\']').on('click',function(){
		var div = $(this).val();	
		$('.optiondiv').hide();
		$('#'+div).show();
	});

	$('#pay').on('click',function(e){
		e.preventDefault();

		var option = get_payment_way();

		$.ajax({
			url:'/visp/public/start-sale',
			type:'POST',
			dataType:'json',
			async:false,
			data:
				$('[name=\'transparente_form\']').serialize()
			,
			success: function(json){
				
				console.log(json);

				$('#MoipWidget').attr('data-token',json.token);
				$('#sale_id').attr('value',json.sale_id);

				if(option == 'billetoption')
					payBillet();
				if(option == 'banktransferoption')
					payBankTransfer();
				if(option == 'creditcardoption')
					payCreditcard();		
			}
		});		
	});

	$('.li-banktransfer').on('click',function(e){
		e.preventDefault();

		$(this).children('input').attr('checked',true);
	});
});