$(document).ready(function(){
	$('.filtrar-por a').on('click',function(e){
		e.preventDefault();

		var input = $(this).siblings('input');
		var $this = $(this);


		if ($this.css('font-weight') == 'bold') {
			$this.attr('style','font-weight:none');
		} else {
			$this.attr('style','font-weight:bold');
		}

		if (input.attr('type') == 'radio') {

			var ul = $this.parent().parent();

			$.each(ul.children('li'), function(i,obj){

				var obj = $(obj);
				
				if (obj.children('input').is(':checked')) {
					obj.children('input').attr('checked', false);
					$(obj).children('a').attr('style','font-weight:none');
				}
			});
		};
		if(input.attr('checked') == 'checked'){
			input.attr('checked',false);
		} else {
			input.attr('checked',true);
		}
	});

	$("select[name='estado']").change(function () { 
   		var selected_value = $(this).children("option:selected").text(); 
   		$('.mascaraestados').text(selected_value);
	});

	$("select#ordenarpor").change(function () {  
	  $(".mascaraordenarpor span").text($(this).children("option:selected").text());
	  $(this).parent().parent().parent().submit();
	});

	(function(){
		$('.mascaraestados').text($("select[name='estado']").children("option:selected").text());
		$(".mascaraordenarpor span").text($('select#ordenarpor').children("option:selected").text());
	})();




});