$(document).ready( function(){

// Slider home
  royalSliderInstance = $('#slider').royalSlider({  
      imageAlignCenter:true,
      hideArrowOnLastSlide:true,
      slideshowEnabled: true,           
      slideshowDelay:8000,
      keyboardNavEnabled:true
  }).data('royalSlider'); 
// FIM Slider home

//Chamada Accordion
  $("#accordiondepartamento").msAccordion({
    vertical:true,
    defaultid: get_default_id()
  });

  $('#accordiondepartamento').on('click','a',function() {
      var category_name = $(this).parent().parent().parent().siblings('.title').children('h2').text();
      var subcategory_name = $(this).text();
      var subcategory_parent = $(this).closest('.set').attr('class');

      $.removeCookie('category',{path:'/'});
      $.removeCookie('subcategory_parent',{path:'/'});
      $.removeCookie('subcategory_text_name',{path:'/'});

      $.cookie('category',category_name,{path:'/'});
      $.cookie('subcategory_text_name',subcategory_name,{path:'/'});
      $.cookie('subcategory_parent',subcategory_parent,{path:'/'});
  });

(function(){

  var parent = $.cookie('subcategory_parent'); 
  var name   = $.cookie('subcategory_text_name');
  
  if (parent && name) {
    var splited = parent.split(' ');
    var classes = '';

    for(i in splited){
      classes += '.'+splited[i];
    }

    $.each($(classes).find('ul > li'),function(i,obj){
      if($(obj).children('a').text() == name){
        $(obj).children('a').attr('style','font-weight:bold');
      } 
    });
  }
  

})();

$("#logo_visp").on('click',function(e) {
  $.removeCookie('category',{path:'/'});
  $.removeCookie('subcategory_parent',{path:'/'});
  $.removeCookie('subcategory_text_name',{path:'/'});
});

//FIM Chamada Accordion

//Chamada mascada dinheiro
  $(".de").maskMoney();
  $(".por").maskMoney();
//FIM Chamada mascada dinheiro

//TOOLTIP PRATELEIRA
  $("[id*=\'tooltip-target\']").ezpz_tooltip({
    // contentId: 'tooltip',
    showContent: function(content) {
      content.fadeIn(600);
    },
    hideContent: function(content) {
      content.stop(true, true).fadeOut(100);
    }
  });
//FIM TOOLTIP PRATELEIRA

//TOOLTIP TAMANHO PAG CADASTRO PROUTO
  $("input#tamanho").ezpz_tooltip({
    contentId: 'tooltip',
    showContent: function(content) {
      content.fadeIn(600);
    },
    hideContent: function(content) {
      content.stop(true, true).fadeOut(100);
    }
  });
//FIM TOOLTIP PRATELEIRA

//BARRA DE CADASTRESE
  // setTimeout(function(){
  //   $('#recebanovidades').animate({
  //     marginTop: '0px'
  //   },500)
  // },10000);
  // setTimeout(function(){
  //   $('#recebanovidades').animate({
  //     marginTop: '-160px'
  //   },500)
  // },80000);
  // $('#recebanovidades .fechar').click(function(){
  //   $('#recebanovidades').animate({
  //     marginTop: '-160px'
  //   },500)
  // });
//FIM BARRA DE CADASTRESE

// //ALTERANDO CHECKBOX
//   checkButtons = $('input[type="checkbox"]');

//   checkButtons.each( function(){
//       $(this).after('<span rel="' + this.id + '" class="footage-checkbox"></span>').css('display','none');
//   });

//   checkButtons.change( function(){
//       this.checked = ( this.checked ) ? false:true;

//       if( this.checked )
//           $('span.footage-checkbox[rel="' + this.id + '"]').addClass('checked');
//       else
//           $('span.footage-checkbox[rel="' + this.id + '"]').removeClass('checked');
//   });

//   $('span.footage-checkbox').click( function(){
//       tempChk = $('input#'+$(this).attr('rel')).change();
//   });

//   $('label').click( function(){
//       tempChk = $('input#'+$(this).attr('for')).change();
//   });
//FIM ALTERANDO CHECKBOX

//SELECT SEXO PÁGINA CADASTRO
$("select#sexo").change(function () {     
  var selectsexo = "";
  $("select#sexo option:selected").each(function () {
        selectsexo += $(this).text() + " ";
  });
  $(".mascaraselectsexo").text(selectsexo);

});
//FIM SELECT SEXO PÁGINA CADASTRO

//SELECT SEXO PÁGINA CADASTRO
$("select.sexofilho").change(function () { 
    
  var selectsexofilho = $(this).children('option:selected').text(); 
  
  $(this).siblings('.mascaraselectsexofilho').text(selectsexofilho);

});
//FIM SELECT SEXO PÁGINA CADASTRO

//UF E CIDADE PÁGINA DE CADASTRO
$("select#uf").change(function () { 
  
  $(".mascaraselectcidade").html('Carregando...');
  
  var selectuf = "";
  $("select#uf option:selected").each(function () {
        selectuf += $(this).val() + " ";
  });
  $(".mascaraselectuf").text(selectuf);

  setTimeout(function(){
    $(".mascaraselectcidade").html('Selecione cidade');
  },1000)

});
$("select#cidade").change(function () { 
    
  var selectcidade = "";
  $("select#cidade option:selected").each(function () {
        selectcidade += $(this).val() + " ";
  });
  $(".mascaraselectcidade").text(selectcidade);

});
// FIM UF E CIDADE PÁGINA DE CADASTRO


//SELECT QUANTIDADE CHECKOUT
$("select#quantidade").change(function () { 
    
  var selectordenarpor = "";
  $("select#quantidade option:selected").each(function () {
        selectordenarpor += $(this).val() + " ";
  });
  $(".mascaradequantidade").text(selectordenarpor);

});
//FIM QUANTIDADE CHECKOUT

//SELECT FRETE CHECKOUT
$("select#tipodefrete").change(function () { 
    
  var selectordenarpor = "";
  $("select#tipodefrete option:selected").each(function () {
        selectordenarpor += $(this).val() + " ";
  });
  $(".mascaratipodefrete").text(selectordenarpor);

});
//FIM FRETE CHECKOUT

//SELECT CATEGORIA
$("select#categoria").change(function () { 
    
  var categoria = "";
  $("select#categoria option:selected").each(function () {
        categoria += $(this).text() + " ";
  });
  $(".mascaracategoria").text(categoria);

});
//FIM CATEGORIA

//SELECT QUANTIDADE
$("select#quantidade").change(function () { 
    
  var quantidade = "";
  $("select#quantidade option:selected").each(function () {
        quantidade += $(this).val() + " ";
  });
  $(".mascaraquantidade").text(quantidade);

});
//FIM CATEGORIA

//SELECT CATEGORIA
$("select#subcategoria").change(function () { 
    
  var subcategoria = "";
  $("select#subcategoria option:selected").each(function () {
        subcategoria += $(this).text() + " ";
  });
  $(".mascarasubcategoria").text(subcategoria);

});
//FIM CATEGORIA

// MASCARAS PADRÕES
  $("#telefone").mask("(99) 9999-9999");
  $("#celular").mask("(99) 9999-9999");
  $("#cpf").mask("999.999.999-99");
  $("#cnpj").mask("99.999.999/9999-99");  
  $("#cep").mask("99999-999");
  $("#datadenascimento").mask("99/99/9999");
  $("#dataenvio").mask("99/99/9999");
  $(".data").mask("99/99/9999");

  $("#precooriginal").maskMoney({symbol:'R$ ', showSymbol:true, thousands:'.', decimal:',', symbolStay: true});
  $("#seupreco").maskMoney({symbol:'R$ ', showSymbol:true, thousands:'.', decimal:',', symbolStay: true});  
// FIM MASCARAS PADRÕES

// CPF OU CNJP
  $('#radiocnpj').click(function(){
    $('#cpf').hide();
    $('#cpf').attr('value','');
    $('#cnpj').show();
  });
  $('#radiocpf').click(function(){
    $('#cnpj').hide();
    $('#cnpj').attr('value','');
    $('#cpf').show();
  });

// MASCARA DE FRETES
var mascarainputfrete = $('#container .dadosdoproduto .segundonivel ul.tabeladefrete li .mascarainput');

$('#container .dadosdoproduto .segundonivel ul.tabeladefrete li').on('click',function(){
  var input = $(this).children('input'); 
  var type = input.val();
  var combinar = $('input[name=\'combinar\']');

  if(input.val() != combinar.val()){
    combinar.attr('checked', false);
    $(mascarainputfrete).removeClass('ativo');
    $(this).children('div').addClass('ativo');
  }else{
    if(!input.attr('checked')){
      $(this).children('div').removeClass('ativo');
    }else{
      $.each(mascarainputfrete.parent(),function(i,obj){

        var obj     = $(obj).children('input');
        var checked = obj.attr('checked');
        var value   = obj.val();

        if(value == 'free'){
          obj.siblings('div').removeClass('ativo');
          obj.attr('checked', false);
        }
        
      });

      $(this).children('div').addClass('ativo');
    }
  } 
});
// FIM MASCARA DE FRETES

$('.fileinput-button').click(function(){
  $('.fileupload-buttonbar').css('height', '300px');
})

//SLIDER PAG PRODUTO

$(function(){
  if($().slides){
    $('#produtoimagem').slides({
      preload: true,
      preloadImage: 'images/loading.gif',
      effect: 'slide, fade',
      crossfade: true,
      slideSpeed: 200,
      fadeSpeed: 600,
      generateNextPrev: true,
      generatePagination: false
    });
  }
});
//FIM SLIDER PAG PRODUTO

//ESTRELINHAS
$('ol.estrelinhas li').click(function(){
  var $this = $( this );  
  var product_id = $this.parent().attr('data-product-id');
  var user_id = $this.parent().attr('data-user-id');
  var ol = $this.parent('ol');  
  var rating = $this.index()+1;
  
  $.ajax({
    url:'/visp/public/rating',
    type:'POST',
    dataType:'json',
    data:{
      product_id:product_id,
      //user_id:user_id,
      rating:rating
    },
    success: function(json){
      if(json.status){
        alert('Obrigado por avaliar este produto!');
      }else{
        alert(json.error);
      }
    }
  });

  if( $this.hasClass('active') && !$this.next('li').hasClass('active') ){  
    $( ol ).find('li').removeClass('active');  
    rating = 0;  
  }  
  else{  
    $( ol ).find('li').removeClass('active');  
    for( var i=0; i<rating; i++ ){  
      $( ol ).find('li').eq( i ).addClass('active');  
    };  
  }  
});
//FIM ESTRELINHAS

//CONFIRMAR SENHA
$('#alterar').click(function(){
  $('.confirmarsenha').fadeIn('slow');
  $('html, body').animate({
        scrollTop: $("#header").offset().top
    }, 1000);
});
$('.confirmarsenha .close').click(function(){
  $('.confirmarsenha').fadeOut('slow');
});

$('#confirmar').on('click',function(e){
 
  e.preventDefault();

  var form = $('#form-change-password');
  var url = form.attr('action');
  var form_data = form.serialize();

  $.ajax({
    url:url,
    type:'POST',
    data: form_data,
    dataType: 'json',
    success: function(json){                  
      if (json.status) {
        $('.sucesso').attr('style','display:block');
        $('[name=\'new_password\']').attr('value','');
        $('[name=\'new_password_confirmation\']').attr('value','');
         setInterval(function(){
          $('.confirmarsenha .close').trigger('click');
        },1000);
      } else {
        $('.mensagemdeerro').attr('style','display:block');       
      }
    }
  });

});


//ESQUECI MINHA SENHA
$('#container ul.teladelogin li .esquecisenha').click(function(){
  $('.esqueciminhasenha').fadeIn('slow');
  $('html, body').animate({
        scrollTop: $("#header").offset().top
    }, 500);
});
$('.esqueciminhasenha .close').click(function(){
  $('.esqueciminhasenha').fadeOut('slow');
});

// ADICIONAR FAVORITOS

$('#adorei').on('click',function(e){
  e.preventDefault;

  var product_id = $(this).attr('data-product-id');
  var user_id    = $(this).attr('data-user-id');

  $.ajax({
    url:'/visp/public/bookmark',
    type:'POST',
    dataType:'json',
    data:{product_id:product_id,user_id:user_id},
    success: function(json){
      if(json.status){
        alert('Favorito adicionado com successo!');
      }else{
        alert(json.error);
      }
    }
  });

});

//DELETAR FAVORITOS

$('.delete-bookmark').on('click',function(){
  var id = $(this).attr('data-product-id');
  var li = $(this).parent();
  
  $.ajax({
      url:'/visp/public/bookmark/'+id,
      type:'DELETE',                  
      dataType:'json',                  
      success: function(json){
        if(json.status){
          li.remove();
          alert('Favorito removido com successo!');
        }else{
          alert(json.error);
        }
      }
    });
});


// BOTAO COMPRAR

$('.comprar').on('click',function(e){
  e.preventDefault();
    console.log(this);                  
  var add_btn = $(this);
  var cart = $('#cart');
  var product_id = $(this).attr('data-product-id');
  
  $.ajax({
    url:'/visp/public/product',
    type:'POST',
    dataType:'json',
    data:{product_id:product_id},
    success: function(json){
      window.location = "/visp/public/products/checkout"
      /*add_btn.remove();
      cart.append('<a class="shoppingcart-remove" data-product-id="'+product_id+'" href="#">Remover do Carrinho</a>');*/
    }
  });
});

// REMOVER DO CARRINHO

$('.shoppingcart-remove').on('click',function(e){
  e.preventDefault();
  
  var product_tr = $(this).closest('tr.informacoes');
  //var cart = $('#cart');
  var product_id = $(this).attr('data-product-id')

  $.ajax({
    url:'/visp/public/product/'+product_id,
    type:'DELETE',
    dataType:'json',                  
    success: function(json){
      product_tr.remove();
      document.location.reload();
      //cart.append('<a class="shoppingcart-add" data-product-id="'+product_id+'" href="#">Adicionar ao carrinho</a>');
    }
  });
});

//CONSULTA AOS CORREIOS

$('#consultarcorreios').on('click',function(){
    var user_cep = $('#cep').val();
    var product_cep = $('.product-cep').val();
   
    // console.log(ceps);

    $.ajax({
        url:'/visp/public/correios',
        type:'POST',
        dataType:'json',
        data:{user_cep:user_cep,product_cep:product_cep},
        success: function(json){
          console.log(json);
          if(!json.erro){
            alert(
              'Codigo: '+json.codigo[0]+"\n"+
              'Valor: R$ '+json.valor[0]+"\n"+
              'Prazo de Enrega: '+json.prazo_de_entrega[0]+"\n dias"+
              'Valor Mão Própria: '+json.valor_mao_propria[0]+"\n"+
              'Aviso de recebimento: '+json.valor_aviso_recebimento[0]+"\n"+
              'Valor declarado: '+json.valor_declarado[0]+"\n"+
              'Entrega domiciliar: '+json.entrega_domiciliar[0]+"\n"+
              'Entrega sábado: '+json.entrega_sabado[0]+"\n"
            );
          }else{
            var str = '';
            $.each(json.erro,function(i,obj){              
              str += obj+"\n";
            });

            alert(str);
          }
        }
    });
});

// PSEUDO AJAX DA HOME

ajax_amount = 3;

function load_products(){
  var lis = $('.prateleira').children('li');
  var size = lis.size();
  $.each(lis,function(i,obj){
    if(i >= ajax_amount)
      $(obj).hide();
  });
}

load_products();

$('.exibirmais').on('click',function(e){
    e.preventDefault();
    var lis = $('.prateleira').children('li');
    var j = 0;
    $.each(lis,function(i,obj){
      
      if($(obj).css('display') == 'none' && j < ajax_amount){        
        $(obj).fadeIn('slow').show();
        j++
      }
    });
  });


// CONTAGEM DO CARRINHO

$.ajax({
  url:'/visp/public/count_cart',
  type:'GET',
  success: function(cart_count){                  
    $('.carrinho a .minhascompras span').html('('+cart_count+')');
  }
});

$('[name=\'product_original_price\']').on('keyup',function(){
  var in_site = $('#price-in-site');
  var price = parseFloat($(this).val().replace(/R\$|\./g,'').replace(/,/g,'.'));
  var price_in_site = (price + (0.15*price));
  in_site.html('R$ '+ Math.ceil(price_in_site));
});




});

function get_default_id () {
  var arr = {
    'ROUPAS':0,
    'SAPATOS':1,
    'ACESSÓRIOS':2,
    'BRINQUEDOS':3,
    'LIVROS E DVDS':4,
    'ENXOVAL':5,
    'MÓVEIS E DECORAÇÃO':6,
    'CADEIRAS E CARRINHOS':7,
  };

  var cookie = $.cookie('category');

  return (arr[cookie]) ? arr[cookie] : 0;
}