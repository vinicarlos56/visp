<?php

class Create_Products_Photos_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products_photos', function($table){			
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('title',50);
			$table->string('file_name',50);
			$table->integer('product_id')->unsigned();			
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products_photos');
	}

}