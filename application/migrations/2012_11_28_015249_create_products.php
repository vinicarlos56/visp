<?php

class Create_Products {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		$engine = Config::get('database.default');

		if ($engine == 'mysql') {	
			Schema::create('products', function($table){
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->string('title',50);
				$table->string('description',255);
				$table->integer('category_id');
				$table->float('weight')->nullable();
				$table->string('origin',1)->nullable();
				$table->string('condition',1)->nullable();
				$table->float('original_price')->nullable();
				$table->float('price');
				$table->integer('amount');
				$table->integer('user_id')->unsigned();			
				$table->timestamps();
			});
		} else if($engine == 'sqlite') {
			Schema::create('products', function($table){
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->string('title',50);
				$table->string('description',255);
				$table->integer('category_id');
				$table->string('weight',50)->nullable();
				$table->string('origin',255)->nullable();
				$table->string('condition',255)->nullable();
				$table->float('original_price')->nullable();
				$table->float('price');
				$table->integer('amount');
				$table->integer('user_id')->unsigned();			
				$table->timestamps();
			});
		}
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}