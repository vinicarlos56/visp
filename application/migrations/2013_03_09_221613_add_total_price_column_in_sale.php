<?php

class Add_Total_Price_Column_In_Sale {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sales', function($table){
			$table->float('total_price');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sales', function($table){
			$table->drop_column('total_price');
		});
	}

}