<?php

class Add_Token_Column_In_Sales {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sales', function($table){
			$table->string('token');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sales', function($table){
			$table->drop_column('token');
		});
	}

}