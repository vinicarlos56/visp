<?php

class Drop_Columns_Filename_And_Alt_On_Banners {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('banners', function($table){
			$table->drop_column('alt');
			$table->drop_column('file_name');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		return;
	}

}