<?php

class Changing_To_Unsigned_Category_Id_On_Products {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{

		$engine = Config::get('database.default');

		if ($engine == 'mysql') {			
			Schema::table('products', function($table){
				$table->drop_column('category_id');			
			});

			Schema::table('products', function($table){
				$table->integer('category_id')->unsigned();	
			});		
		}
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('products', function($table){
			$table->drop_column('category_id');			
		});

		Schema::table('products', function($table){
			$table->integer('category_id');	
		});	
	}

}