<?php

class Rename_Subcategory {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		$engine = Config::get('database.default');

		if ($engine == 'mysql') {
			Schema::rename('subcategory','subcategories');
		} else if($engine == 'sqlite') {
			Schema::rename('subcategory','categories');
		}
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::rename('subcategory','subcategories');
	}

}