<?php

class Fill_Subcategory_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		$created = date('Y-m-d h:m:s');
		DB::table('subcategories')->insert(array('name'=>'Roupas','created_at'=>$created));
		DB::table('subcategories')->insert(array('name'=>'Sapatos','created_at'=>$created));
		DB::table('subcategories')->insert(array('name'=>'Acessórios','created_at'=>$created));
		DB::table('subcategories')->insert(array('name'=>'Brinquedos','created_at'=>$created));
		DB::table('subcategories')->insert(array('name'=>'Livros e DVDs','created_at'=>$created));
		DB::table('subcategories')->insert(array('name'=>'Enxoval','created_at'=>$created));
		DB::table('subcategories')->insert(array('name'=>'Móveis e Decoração','created_at'=>$created));
		DB::table('subcategories')->insert(array('name'=>'Cadeiras e Carrinhos','created_at'=>$created));

	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('subcategories')->where('name','=','Roupas')->delete();
		DB::table('subcategories')->where('name','=','Sapatos')->delete();
		DB::table('subcategories')->where('name','=','Acessórios')->delete();
		DB::table('subcategories')->where('name','=','Brinquedos')->delete();
		DB::table('subcategories')->where('name','=','Livros e DVDs')->delete();
		DB::table('subcategories')->where('name','=','Enxoval')->delete();
		DB::table('subcategories')->where('name','=','Móveis e Decoração')->delete();
		DB::table('subcategories')->where('name','=','Cadeiras e Carrinhos')->delete();
	}

}