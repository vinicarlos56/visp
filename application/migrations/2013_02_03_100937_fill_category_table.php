<?php

class Fill_Category_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()	
	{
		$created = date('Y-m-d h:m:s');
		DB::table('categories')->insert(array('name'=>'Meninos','created_at'=>$created));
		DB::table('categories')->insert(array('name'=>'Meninas','created_at'=>$created));
		DB::table('categories')->insert(array('name'=>'Unissex','created_at'=>$created));
		DB::table('categories')->insert(array('name'=>'Gêmeos','created_at'=>$created));
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('categories')->where('name', '=', 'Meninos')->delete();
		DB::table('categories')->where('name', '=', 'Meninas')->delete();
		DB::table('categories')->where('name', '=', 'Unissex')->delete();
		DB::table('categories')->where('name', '=', 'Gêmeos')->delete();
	}

}