<?php

class Add_Phones_Columns_In_Users {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($table){
			$table->string('cell_phone');
			$table->string('phone');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function($table){
			$table->drop_column('cell_phone');
			$table->drop_column('phone');
		});
	}

}