<?php

class Add_Status_Column_On_Banner {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('banners', function($table){
			$table->boolean('status')->default(0);
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('banners',function($table){
			$table->drop_column('status');
		});
	}

}