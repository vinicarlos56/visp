<?php

class Create_Addresses {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('addresses', function($table){
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('cep',15);
			$table->string('logradouro',255);
			$table->string('city',50);
			$table->string('neighborhood',50);
			$table->string('state',2);
			$table->string('complement',100)->nullable();
			$table->integer('number');	
			$table->integer('user_id');				
			$table->timestamps();

		});

		DB::table('addresses')->insert(array(
			'cep'		      => '21645440',
		    'logradouro'      => 'Rua Olvera Bueno',
		    'city'  	      => 'Rio de Janeiro',		    
		    'neighborhood'    => 'Anchieta',
		    'state'		  	  => 'RJ',
		    'complement'	  => '',
		    'number'		  => '713',
		    'user_id'	  => '1'
		));
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('addresses');
	}

}