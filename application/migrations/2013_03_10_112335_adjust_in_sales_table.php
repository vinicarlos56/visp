<?php

class Adjust_In_Sales_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sales', function($table){
			$table->string('payment_way');
			$table->string('status');
			$table->string('url');
			$table->string('cc_moip_code');
			$table->float('cc_moip_tax');
			$table->float('cc_total_paid');
			$table->integer('cc_class_code');
			$table->string('cc_class_description');
			$table->drop_column('seller_id');
			$table->drop_column('product_id');
			$table->drop_column('amount');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sales', function($table){
			$table->drop_column('payment_way');
			$table->drop_column('status');
			$table->drop_column('url');
			$table->drop_column('cc_moip_code');
			$table->drop_column('cc_moip_tax');
			$table->drop_column('cc_total_paid');
			$table->drop_column('cc_class_code');
			$table->drop_column('cc_class_description');
			$table->integer('seller_id')->unsigned();
			$table->integer('product_id')->unsigned();
			$table->integer('amount')->unsigned();
		});
	}

}