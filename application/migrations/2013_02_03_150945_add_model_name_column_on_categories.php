<?php

class Add_Model_Name_Column_On_Categories {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('categories', function($table){
			$table->string('model_name');
		});		
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('categories',function($table){
			$table->drop_column('model_name');
		});
	}

}