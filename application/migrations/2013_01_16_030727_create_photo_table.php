<?php

class Create_Photo_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('photos', function($table){			
			$table->increments('id');
			$table->string('title',50);
			$table->string('alt',50);			
			$table->string('file_name',50);
			$table->integer('model_id')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('photos');
	}

}