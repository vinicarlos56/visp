<?php

class Alter_Column_Weight_On_Products {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		$engine = Config::get('database.default');

		if ($engine == 'mysql') {	
			DB::query('alter table products modify weight varchar(50)');
		}
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::query('alter table products modify weight float');
	}

}