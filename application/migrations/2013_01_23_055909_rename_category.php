<?php

class Rename_Category {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		$engine = Config::get('database.default');

		if ($engine == 'mysql') {
			Schema::rename('category','categories');
		} else if($engine == 'sqlite') {
			Schema::rename('category','subcategories');
		}
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::rename('categories','category');
	}

}