<?php

class Create_Sales_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales', function($table){
			$table->increments('id');
			$table->integer('buyer_id')->unsigned();
			$table->integer('seller_id')->unsigned();
			$table->integer('product_id')->unsigned();
			$table->integer('amount')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales');
	}

}