<?php

class Fill_Model_Name_On_Subcategories {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('subcategories')->where('name','=','Roupas')->update(array('model_name'=>'clothes'));
		DB::table('subcategories')->where('name','=','Brinquedos')->update(array('model_name'=>'toys'));
		DB::table('subcategories')->where('name','=','Sapatos')->update(array('model_name'=>'shoes'));
		DB::table('subcategories')->where('name','=','Acessórios')->update(array('model_name'=>'acessories'));
		DB::table('subcategories')->where('name','=','Livros e DVDs')->update(array('model_name'=>'books'));
		DB::table('subcategories')->where('name','=','Enxoval')->update(array('model_name'=>'outfit'));
		DB::table('subcategories')->where('name','=','Móveis e Decoração')->update(array('model_name'=>'furnitures'));
		DB::table('subcategories')->where('name','=','Cadeiras e Carrinhos')->update(array('model_name'=>'babystroller'));		
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('subcategories')->where('name','=','Roupas')->update(array('model_name'=>''));
		DB::table('subcategories')->where('name','=','Brinquedos')->update(array('model_name'=>''));
		DB::table('subcategories')->where('name','=','Sapatos')->update(array('model_name'=>''));
		DB::table('subcategories')->where('name','=','Acessórios')->update(array('model_name'=>''));
		DB::table('subcategories')->where('name','=','Livros e DVDs')->update(array('model_name'=>''));
		DB::table('subcategories')->where('name','=','Enxoval')->update(array('model_name'=>''));
		DB::table('subcategories')->where('name','=','Móveis e Decoração')->update(array('model_name'=>''));
		DB::table('subcategories')->where('name','=','Cadeiras e Carrinhos')->update(array('model_name'=>''));		
	}

}