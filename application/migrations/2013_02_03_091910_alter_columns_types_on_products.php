<?php

class Alter_Columns_Types_On_Products {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		$engine = Config::get('database.default');

		if ($engine == 'mysql') {			
			DB::query('alter table products modify origin varchar(255)');
			DB::query('alter table products modify `condition` varchar(255)');
		}
		
	} 


	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::query('alter table products modify origin varchar(1)');
		DB::query('alter table products modify `condition` varchar(1)');		
	}

}