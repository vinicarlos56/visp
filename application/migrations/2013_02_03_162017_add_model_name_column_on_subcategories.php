<?php

class Add_Model_Name_Column_On_Subcategories {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('subcategories', function($table){
			$table->string('model_name');
		});	
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('subcategories', function($table){
			$table->drop_column('model_name');
		});	
	}

}