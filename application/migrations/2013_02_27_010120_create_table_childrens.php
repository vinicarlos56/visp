<?php

class Create_Table_Childrens {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('childrens', function($table){
			$table->increments('id');
			$table->string('name');
			$table->date('birthday');
			$table->string('sex',1);
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('childrens');
	}

}