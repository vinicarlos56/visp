<?php

class Allowing_Null_In_Cpf_Column {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		$engine = Config::get('database.default');

		if ($engine == 'mysql') {	
			DB::query('alter table users modify cpf varchar(200);');
		}
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::query('alter table users modify cpf varchar(200) not null;');
	}

}