<?php

class Add_Size_Color_Mark_Columns_On_Products {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('products', function($table){
			$table->string('size');
			$table->string('color');
			$table->string('mark');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('products', function($table){
			$table->drop_column('size');
			$table->drop_column('color');
			$table->drop_column('mark');
		});
	}

}