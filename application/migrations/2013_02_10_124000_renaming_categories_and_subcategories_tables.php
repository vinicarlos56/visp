<?php

class Renaming_Categories_And_Subcategories_Tables {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		$engine = Config::get('database.default');

		if ($engine == 'mysql') {
			DB::query('rename table categories to _categories');		
			DB::query('rename table subcategories to _subcategories');
			DB::query('rename table _categories to subcategories');
			DB::query('rename table _subcategories to categories');
		}
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::query('rename table categories to _categories');
		DB::query('rename table subcategories to _subcategories');
		DB::query('rename table _categories to subcategories');
		DB::query('rename table _subcategories to categories');
	}

}