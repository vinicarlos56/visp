<?php

class Fill_Model_Name_On_Categories {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('categories')->where('name','=','Meninos')->update(array('model_name'=>'boys'));
		DB::table('categories')->where('name','=','Meninas')->update(array('model_name'=>'girls'));
		DB::table('categories')->where('name','=','Unissex')->update(array('model_name'=>'unisex'));
		DB::table('categories')->where('name','=','Gêmeos')->update(array('model_name'=>'gemeos'));
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('categories')->where('name','=','Meninos')->update(array('model_name'=>''));
		DB::table('categories')->where('name','=','Meninas')->update(array('model_name'=>''));
		DB::table('categories')->where('name','=','Unissex')->update(array('model_name'=>''));
		DB::table('categories')->where('name','=','Gêmeos')->update(array('model_name'=>''));
	}

}