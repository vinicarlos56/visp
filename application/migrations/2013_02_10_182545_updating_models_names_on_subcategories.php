<?php

class Updating_Models_Names_On_Subcategories {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('subcategories')->where('name','=','Gêmeos')->update(array('model_name'=>'gemini'));		
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('subcategories')->where('name','=','Gêmeos')->update(array('model_name'=>'gemeos'));			
	}

}