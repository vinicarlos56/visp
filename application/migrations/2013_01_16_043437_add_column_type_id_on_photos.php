<?php

class Add_Column_Type_Id_On_Photos {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up(){
		Schema::table('photos', function($table){
			$table->integer('type_id');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('photos', function($table){
			$table->drop_column('type_id');
		});
	}

}