<?php

class Add_User_Id_Column_In_Childrens {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('childrens', function($table){
			$table->integer('user_id')->unsigned();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('childrens', function($table){
			$table->drop_column('user_id');
		});
	}

}