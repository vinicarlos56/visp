<?php

Route::get('test_email',array('as'=>'check_email_moip','uses'=>'users@check_moip'));

Route::get('test', function(){

	// // Cria a instancia da aplicacao, informando o appid e o secret
	// $facebook = new Facebook(array(
	//   'appId'  => '410973395664819',
	//   'secret' => '9e5010bb6d532088df4e6f887977f084',
	// ));

	// // obtem o id do usuario
	// $user = $facebook->getUser();
	
	// if ($user) { // usuario logado
	//     try {
	// 	    // Obtem dados do usuario logado
	// 	    $user_profile = $facebook->api('/me');
	// 	    // $permissions  = $facebook->api("/me/permissions");
	// 	    $get = User::where('email','=', $user_profile['email'])
	// 	    		   ->where('fb_id','=',$user_profile['id'])->first();

	// 	    if(!$get){
	// 	    	$user = User::create(array(
	// 	        	'fb_id' 	=> $user_profile['id'],
	// 	        	'name'  	=> $user_profile['name'],
	// 	        	'nickname' 	=> $user_profile['username'],
	// 	        	'email' 	=> $user_profile['email'],
	// 	        	'birthday' 	=> User::store_date($user_profile['birthday']),
	// 	        	'sex' 		=> User::get_fb_gender($user_profile['gender']),
	// 	        	'cpf'=>null
	// 	        ));
	// 	    }else{
	// 	    	$user = $get;
	// 	    }	        

	// 	    if($user){		    
	// 		    if ( Auth::login($user) ){
	// 		    echo "<script>
	// 	            window.close();
	// 	            window.opener.location = '".URL::base()."users/".Auth::user()->nickname."';
	// 	            </script>";	
	// 		        // return Redirect::to('users/'.Auth::user()->nickname);
	// 		    }
	// 		    else
	// 		        return Redirect::to_route('login')->with('login_errors', true);
	// 	    }

	//     } catch (FacebookApiException $e) {
	//             error_log($e);
	//             $user = null;
	//     }
	// } else {
 //        // usuario nao logado, solicitar autenticacao

	// 	$params = array (
	// 	  'display'=>'popup',
 //          'scope' => 'email, user_birthday'              
 //        );
 //        // $loginUrl =
 //        return $facebook->getLoginUrl($params);
 //        // echo "<a href=\"$loginUrl\">Facebook Login</a><br />";
 //        // echo "<strong><em>Voc&ecirc; n&atilde;o esta conectado..</em></strong>";
	// }
});

Route::get('/',array('as'=>'home','uses'=>'home@index'));
Route::get('login',array('as'=>'login','uses'=>'users@login'));
Route::get('logout', array('as'=>'logout','uses'=>'users@logout'));
Route::get('cadastro', array('as'=>'register','uses'=>'users@register')); //register substituindo o new
Route::get('users/(:any)',array('uses'=>'users@view'));
Route::get('users/(:num)/edit',array('uses'=>'users@edit'));
Route::get('produtos/novo',array('as'=>'new_product','uses'=>'products@new'));
Route::get('products/(:num)/edit',array('uses'=>'products@edit'));
Route::get('minhas-vendas',array('as'=>'my_products','uses'=>'products@index'));
Route::get('minhas-compras',array('as'=>'my_purchases','uses'=>'products@purchases'));
Route::get('produto/(:any)',array('as'=>'view_product','uses'=>'products@view'));
Route::get('count_cart',array('uses'=>'shoppingcart@count'));
Route::get('products/checkout',array('uses'=>'products@checkout'));
Route::get('conta-moip',array('as'=>'moip_account','uses'=>'users@moip'));
Route::get('favoritos',array('as'=>'bookmarks','uses'=>'bookmarks@view'));
Route::get('produtos/buscar/(:any)/(:any)',array('uses'=>'products@search'));
Route::get('transparente', array('uses'=>'sales@transparente'));
Route::get('buscar', array('as'=>'general_search','uses'=>'products@general_search',));

Route::get('upload',function(){ // FIXME: Pra que serve isso?

	$teste = new UploadHandler;
	$teste->get();
});

Route::get('products/(:num)/delete',array('uses'=>'products@destroy')); // FIXME: Usar o método Delete

// páginas estáticas no momento

Route::get('como-funciona',array('before'=>'auth',function(){
	return View::make('static.como-funciona');
}));

Route::get('quero-vender',array('before'=>'auth',function(){
	return View::make('static.quero-vender');
}));

Route::get('quero-comprar',array('before'=>'auth',function(){
	return View::make('static.quero-comprar');
}));

Route::get('contato',array('before'=>'auth',function(){
	return View::make('static.contato');
}));

Route::get('central-de-atendimento',array('before'=>'auth',function(){
	return View::make('static.central-de-atendimento');
}));

// páginas estáticas no momento
Route::post('finish-sale',array('as'=>'finish-sale','uses'=>'sales@finish'));
Route::post('start-sale',array('as'=>'start-sale','uses'=>'sales@start_transaction'));
Route::post('moip', array('as'=>'moip','before'=>'csrf','uses'=>'sales@moip'));
Route::post('correios',array('uses'=>'sales@correios'));
Route::post('avatar',array('before'=>'csrf','uses'=>'users@avatar'));
Route::post('users',array('before'=>'csrf','uses'=>'users@create'));
Route::post('products',array('before'=>'csrf','uses'=>'products@create'));
Route::post('products/photos',array('before'=>'csrf','uses'=>'products@photos'));
Route::post('login',array('before'=>'csrf','uses'=>'users@login'));
Route::post('product',array('uses'=>'shoppingcart@add'));
Route::post('bookmark',array('uses'=>'bookmarks@create'));
Route::post('newsletter',array('uses'=>'home@newsletter'));
Route::post('rating', array('uses'=>'users@rating'));
Route::post('users/nova-senha', array('as'=>'new_password','before'=>'csrf','uses'=>'users@change_password'));



Route::put('users/update',array('before'=>'csrf','uses'=>'users@update'));
Route::put('products/update',array('before'=>'csrf','uses'=>'products@update'));
Route::put('user/moip',array('before'=>'csrf','uses'=>'users@moip'));
Route::put('upload',array('uses'=>'products@photos'));


// Route::delete('bookmark',array('uses'=>'bookmarks@destroy'));
Route::delete('bookmark/(:num)',array('uses'=>'bookmarks@destroy'));
Route::delete('product/(:num)',array('uses'=>'shoppingcart@destroy'));
Route::delete('photo/(:any)',array('uses'=>'products@delete_photo'));








/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application.
|
*/

Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function()
{
	return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
|
| Filters provide a convenient method for attaching functionality to your
| routes. The built-in before and after filters are called before and
| after every request to your application, and you may even create
| other filters that can be attached to individual routes.
|
| Let's walk through an example...
|
| First, define a filter:
|
|		Route::filter('filter', function()
|		{
|			return 'Filtered!';
|		});
|
| Next, attach the filter to a route:
|
|		Router::register('GET /', array('before' => 'filter', function()
|		{
|			return 'Hello World!';
|		}));
|
*/

Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::to('login');
});