<?php 

namespace Helpers;

class Currency
{
	
	public static function format_price($price){
		return (float) str_replace(',','.',str_replace('.','',str_replace('R$ ','',$price)));
	}

	public static function show_price($price){		
		return 'R$ '.number_format((float)$price,2,',','.');
	}

	public static function select_parcels($total_price)
	{
		echo "<select name='parcelsnumber' id='parcelsnumber'>";
			for ($i=1; $i <= 12; $i++) {
				if ($i === 1) 
					echo "<option value='{$i}'>À Vista ".self::show_price($total_price/$i)."</option>";
				else
					echo "<option value='{$i}'>{$i}x de ".self::show_price($total_price/$i)."</option>";
				
			}
		echo "</select>";
	}
}