<?php

class Sales_Controller extends Base_Controller {
	public $restful = true;
	public $layout  = "layout.main_user";

	public function __construct(){
		parent::__construct();

	    // $this->filter('before', 'auth')->only(array(
	    // 	'moip','transparente','finish'
	    // ));
	}


	public function post_moip(){

		$shoppingcart 		  = new ShoppingCart;
		$buyer_cep 	  		  = Input::get('cep');
		$total_price 		  = Sales::get_total_selling_price($shoppingcart);
		$transport_price      = IoC::resolve('saleservice')->calculate_total_transport_price($buyer_cep);		
		$transport_price_list = IoC::resolve('saleservice')->get_transport_prices_list();

		return View::make('products.transparente')->with(array(
			'cart' 		 			=> $shoppingcart,
			'transport_price_list'	=> $transport_price_list,
			'transport_price'		=> $transport_price,
			'total_price'			=> $total_price,
			'auth_user'				=> Auth::user()
		));	
	}

	public function post_start_transaction()
	{
		$shoppingcart = new ShoppingCart;
		$total_price  = Sales::get_total_selling_price($shoppingcart);;
		$buyer_cep 	  = Auth::user()->address->cep;
		$data 		  = Input::all();
		// TODO: Ao setar iniciar venda, setar ela na sessão para que seja retornada enquanto não for finalizada
		$sale 			   = new Sale();
		$sale->buyer_id    = Auth::user()->id;		
		$sale->total_price = $total_price + IoC::resolve('saleservice')->calculate_total_transport_price($buyer_cep);
		$sale->save();		

		$msg = '';

		foreach ($shoppingcart->get_products() as $product_id) {
			$msg .= Product::find($product_id)->title.",";
			$order = new Order;
			$order->product_id = $product_id;
			$order->sale_id    = $sale->id;
			$order->amount 	   = 1;			
			$order->save();			
		}

		$answer = $sale->start_transaction($data,substr($msg,0,-1));

		return json_encode(array('token'=>$answer->token,'sale_id'=>$sale->id));

	}

	public function get_transparente($sale){
		return View::make('products.transparente')->with(array('sale'=>$sale));
	}

	public function post_finish(){
		$sale_id   = Input::get('sale_id');
		$sale_data = Input::get('sale_data');
		$payment   = Input::get('payment_way');
		$payment_way = array(
			'billetoption' 		 => 'BoletoBancario',
			'banktransferoption' => 'DebitoBancario',
			'creditcardoption' 	 => 'CartaoCredito'
		);	

		$sale = Sale::update($sale_id,array(			
			'payment_way' 	=> $payment_way[$payment],
			'status' 		=> isset($sale_data['Status']) 			? $sale_data['Status'] 		: false,
			'cc_moip_code' 	=> isset($sale_data['CodigoMoIP']) 		? $sale_data['CodigoMoIP'] 	: false,
			'cc_moip_tax' 	=> isset($sale_data['TaxaMoIP']) 		? $sale_data['TaxaMoIP'] 	: false,
			'cc_total_paid' => isset($sale_data['TotalPago']) 		? $sale_data['TotalPago'] 	: false,
			'url' 			=> isset($sale_data['url']) 			? $sale_data['url'] 		: false,
			'cc_class_code'	=> isset($sale_data['Classificacao']['Codigo']) 		? $sale_data['Classificacao']['Codigo'] 	: false,
			'cc_class_description' => isset($sale_data['Classificacao']['Descricao']) 	? $sale_data['Classificacao']['Descricao'] 	: false
		));	

		$sale = Sale::find($sale_id);

		foreach ($sale->orders as $order) {
			$order->product->sold = true;
			$order->product->save();
		}
		Session::forget('shoppingcart');

		$json_response['status'] = 1;

		return json_encode($json_response);



		// TODO: reduzir a quantidade de produtos assim que for fnalizada a compra

		// Codigo: 0
		// Mensagem: "Requisição processada com sucesso"
		// StatusPagamento: "Sucesso"
		// url: "https://desenvolvedor.moip.com.br/sandbox/Instrucao.do?token=V2P0N153I0E3X1R0P0H7Q0W3N4X9M71427Q0Z0R0O0H0M0L2F4A3Z3T1S2L8"
	}

	public function post_correios(){
		
	}

}