<?php

class Products_Controller extends Base_Controller {
	public $restful = true;
	public $layout  = "layout.main_user";


	public function __construct(){
		parent::__construct();

	    // $this->filter('before', 'auth')->only(array(
	    // 	'index','new','edit','create','photos','update','destroy','purchases'
	    // ));
	}

	public function get_index(){		

		return $this->layout->nest(
			'content',
			'products.index',
			array(
				'user_id'=>Auth::user()->id,
				'products'=>Auth::user()->products
			)
		);							  
	}

	public function get_purchases(){
		// TODO: verificar se a compra foi finalizada
		//$cart = Session::get('shoppingcart');

		//if($cart){
		// 	foreach ($cart as $product) {
		// 		Product::update($product,array('sold'=>1));
		// 	}			
		// }


		// Session::forget('shoppingcart');

		// $products = Product::where('sold','=',1)->get();

		// TODO: remove, duplicidade
		$sales = Sale::where('buyer_id','=',Auth::user()->id)->get();
		$product_sale = array();
		$products = array();
		foreach ($sales as $sale) {
			foreach ($sale->orders as $order) {
				$products[] = $order->product;
				$product_sale[$order->product->id] = $sale;
			}
		}

		return $this->layout->nest('content','products.purchases',array('products'=>$products,'sales'=>$product_sale));
	}

	// TODO: remover daqui, colocar no controller shopping cart 
	public function get_checkout(){			

		$cart  		 = new ShoppingCart;

		// if(is_array($cart) and !empty($cart)){
		// 	foreach ($cart['users'] as $user_id => $content) {	
		// 		$user_products = array();
		// 		$users[$user_id]['user_name'] = User::find($user_id)->name;
		// 		foreach ($content['products'] as $product_id) {					
		// 			$user_products[] = IoC::resolve('product',array($product_id));;
		// 		}
		// 		$users[$user_id]['products'] = $user_products;
		// 	}
		// }	

		return View::make('products.checkout', 
			array(				
				'cart'=>$cart
			)
		);
	}

	public function get_new(){
		$user = Auth::user();		
		//$user = User::where('id','=',$user_id)->first();		
		// TODO: Remover a redundância na atribuição dos dados
		return $this->layout->nest(
			'content',
			// 'products.new',
			'products.form',
			array('user_id'=>$user->id)
		)->with('user',$user);
	}

	public function get_edit($product_id){
		$product = IoC::resolve('product',array($product_id));
		return $this->layout->nest(
			'content',
			// 'products.edit',
			'products.form',
			array('product'=>$product)
		)->with('product',$product);
	}

	public function post_create(){		

		$input 		= Input::all();		
		$validation = Product::validate($input,array('product'));

		if (!$validation) {
			return Redirect::to_route('new_product')
				->with('errors',Product::errors_to_string())
				->with_input();
		}else{
			$user = new Product;
			$user->save_product($input);

			return Redirect::to_route('my_products')->with('success','Produto adicionado com sucesso!');			
		}
	}

	public function put_photos(){

		$photo 		= new Photo;
		$product_id = Input::get('product_id');

		$_FILES['files']['name'] 	 = $_FILES['files']['name'][0];
		$_FILES['files']['size'] 	 = $_FILES['files']['size'][0];
		$_FILES['files']['tmp_name'] = $_FILES['files']['tmp_name'][0];
		$_FILES['files']['type']	 = $_FILES['files']['type'][0];
		$_FILES['files']['error'] 	 = $_FILES['files']['error'][0];

		$type = $_FILES['files']['type'];

		if($type == 'image/jpeg' OR $type != 'image/jpg'){
			$photo->save_photo(array(
				'input_name'=> 'files',
				'title'		=> 'title',//Input::get('title'),
				'alt'		=> 'title',//Input::get('title'), // TODO: Replace this in the future
				'file_name'	=> $_FILES['files']['name'],
				'type_id'	=> Photo::PRODUCT,
				'model_id'	=> $product_id		
			))->thumb(190)
			  ->thumb(90)
			  ->thumb(312,360);

			$obj = new stdClass;

			$obj->name = $_FILES['files']['name'];
			$obj->size = $_FILES['files']['size'];
			$obj->type = $_FILES['files']['type'];
			$obj->delete_type = "DELETE";
			$obj->delete_url = "/visp/public/photo/{$obj->name}";

			$obj->thumbnail_url = "/visp/public/uploads/thumbs/thumb_90x90_".$obj->name;

			$teste = array('files'=>array($obj));
			
			return json_encode($teste);

		}else{
			Session::flash('errors',"O formato de imagem {$type} não é suportado.");
			return Redirect::to("products/{$product_id}/edit");
		}
	}

	public function delete_delete_photo($name){
		$photo = Photo::where('file_name','=',$name)->first();
		
		if(		unlink("public/uploads/images/{$photo->file_name}") AND
				unlink("public/uploads/thumbs/thumb_190x190_{$photo->file_name}") AND
				unlink("public/uploads/thumbs/thumb_90x90_{$photo->file_name}") AND
				unlink("public/uploads/thumbs/thumb_312x360_{$photo->file_name}") AND
				$photo->delete()
		)return true;
	}

	public function post_photos(){

		$photo 		= new Photo;
		$product_id = Input::get('product_id');
		$type 		= Input::file('image.type');

		if($type == 'image/jpeg' OR $type != 'image/jpg'){
			$photo->save_photo(array(
				'input_name'=> 'files',
				'title'		=> 'title',//Input::get('title'),
				'alt'		=> 'title',//Input::get('title'), // TODO: Replace this in the future
				'file_name'	=> Input::file('image.name'),
				'type_id'	=> Photo::PRODUCT,
				'model_id'	=> $product_id		
			))->thumb(190)
			  ->thumb(90)
			  ->thumb(312,360);

			return Redirect::to("products/{$product_id}/edit");
		}else{
			Session::flash('errors',"O formato de imagem {$type} não é suportado.");
			return Redirect::to("products/{$product_id}/edit");
		}
	}


	public function put_update(){		

		$input 		= Input::all();		
		$validation = Product::validate($input,array('product'));

		if (!$validation) {
			return Redirect::to('products/'.$input['product_id'].'/edit')
				->with('errors',Product::errors_to_string())
				->with_input();
		}else{
			$user = new Product;
			$user->save_product($input,$input['product_id']);

			return Redirect::to('products/'.$input['product_id'].'/edit')->with('success','Produto alterado com sucesso!');
		}
		// TODO: Colocar validação em produtos
		// $products_data = array(
		// 	'title'       	 => $input['title'],
		// 	'description' 	 => $input['description'],
		// 	'category_id' 	 => $input['category_id'],
		// 	'weight'      	 => $input['weight'],
		// 	'origin'      	 => $input['origin'],
		// 	'condition'   	 => $input['condition'],
		// 	'original_price' => $input['original_price'],
		// 	'price'          => $input['price'],
		// 	'amount'         => $input['amount'],
		// 	'user_id'		 => Auth::user()->id
		// );

		// Product::update($input['product_id'],$products_data);

	}

	/**
	 * FIXME: Utilizar o método restful delete
	*/

	public function get_destroy($product_id){
		$product = Product::find($product_id);
		
		foreach ($product->photos as $photo) {
			if(
				unlink("public/uploads/images/{$photo->file_name}") AND
				unlink("public/uploads/thumbs/thumb_190x190_{$photo->file_name}") AND
				unlink("public/uploads/thumbs/thumb_90x90_{$photo->file_name}") AND
				unlink("public/uploads/thumbs/thumb_312x360_{$photo->file_name}")
			)$photo->delete();
			else
				exit();
		}
		

		if($product->delete())
			return Redirect::to_route('my_products')->with('success','Produto deletado com sucesso');
		else
			return Redirect::to_route('my_products')->with('error','Produto não foi deletado');
	}


	public function get_view($product_url_title){ // TODO: Substituir por urltitle
		$resolve = Product::where('url_title','=',$product_url_title)->first();		
		$product = IoC::resolve('product',array($resolve->id));		
		// $product = $resolve::where('title','=',$product_url_title)->first();

		return View::make(
			'products.view',			
			array('product'=>$product)
		);
	}

	public function get_search($category,$subcategory){

		$filter = Input::all();

		$category_arr = array(
			'roupas'			=> 'clothes',
			'sapatos'			=> 'shoes',
			'acessorios'		=> 'acessories',
			'brinquedos'		=> 'toys',
			'livrosedvds'		=> 'books',
			'enxoval'			=> 'outfit',
			'moveisedecoracao'	=> 'furnitures',
			'cadeirasecarrinhos'=> 'babystroller'
		);

		$subcategory_arr = array(
			'meninos' => 'boys', 	 
			'meninas' => 'girls',  
			'unissex' => 'unisex', 
			'gemeos'  => 'gemini'
		);

		$css_class = array(
			'meninos'=>'menino',
			'meninas'=>'menina',
			'unissex'=>'unisses',
			'gemeos'=>'gemeos',
		);

		$category_id    = DB::table('categories')->where('model_name','=',$category_arr[$category])->first()->id;
		$subcategory_id = DB::table('subcategories')->where('model_name','=',$subcategory_arr[$subcategory])->first()->id;

		$products = Product::where('category_id','=',$category_id)->where('subcategory_id','=',$subcategory_id)
			->where('products.status','=','1')
			->where('products.price','>',0)
			->where('products.sold','=',0)
			->join('photos', 'products.id', '=', 'photos.model_id',' ')
			->where('photos.type_id','=',Photo::PRODUCT)
			->distinct();

		if (!empty($filter['frete'])) {
			foreach ($filter['frete'] as $frete) {
				if ($frete == 'gratis') 
					$products->where('weight','=','free');
				if ($frete == 'combinar')
					$products->where('combine','=','combinar');
				if ($frete == 'normal')
					$products->where('weight','<>','free')->where('combine','<>','combinar');
			}
		}

		if (!empty($filter['estado'])) {
			$products->join('users','users.id','=','products.user_id',' ')
					 ->join('addresses','users.id','=','addresses.user_id',' ')
					 ->where('state','=',$filter['estado']);
		}

		if (!empty($filter['preco_de'])) {
			$products->where('price','>=',$filter['preco_de']);
		} 
		if (!empty($filter['preco_por'])) {
			$products->where('price','<=',$filter['preco_por']);
		}

		if (!empty($filter['condicao']))
			$products->where('condition','=',$filter['condicao']);

		if (!empty($filter['preco'])) {
			switch ($filter['preco']) {
				case '0-50':
					$products->where('price','<=',50);
					break;
				case '50-100':
					$products->where('price','>=',50);
					$products->where('price','<=',100);
					break;
				case '100-200':
					$products->where('price','>=',100);
					$products->where('price','<=',200);
					break;
				case '200-0':
					$products->where('price','>=',200);
					break;				
			}
		}

		if (!empty($filter['search_order'])) {
			switch ($filter['search_order']) {
				case 'recente':
					$products->order_by('products.created_at','desc');
					break;
				case 'antigo':
					$products->order_by('products.created_at');
					break;
				case 'precomenor':
					$products->order_by('products.price');
					break;
				case 'precomaior':
					$products->order_by('products.price','desc');
					break;				
			}
		}

		$products = $products->get(array('products.*'));
		
		return View::make(
			'products.search',			
			array(
				'products'	  => $products,
				'category'	  => DB::table('categories')->where('model_name','=',$category_arr[$category])->first()->name,
				'subcategory' => DB::table('subcategories')->where('model_name','=',$subcategory_arr[$subcategory])->first()->name,
				'subcategory_name' => $css_class[$subcategory],
				'filter' => $filter,
			)
		);		
	}

	public function get_general_search()
	{
		$term = Input::get('term');
		$result = Product::where('products.title','LIKE',"%$term%")->or_where('products.description','LIKE',"%$term%")
			->where('products.status','=','1')
			->where('products.price','>',0)
			->where('products.sold','=',0)
			->join('photos', 'products.id', '=', 'photos.model_id',' ')
			->where('photos.type_id','=',Photo::PRODUCT)
			->distinct()
			->get(array('products.*'));

		return View::make(
			'products.search',			
			array(
				'products'	  => $result,
				'term'=>$term,	
			)
		);

	}

}