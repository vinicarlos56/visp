<?php 

Class ShoppingCart_Controller extends Controller{

	public $restful = true;
	private $shoppingcart;

	public function __construct()
	{
		parent::__construct();

		$this->shoppingcart = new ShoppingCart;
	}

	public function get_products()
	{
		
	}

	public function post_add()
	{
		$product_id = Input::get('product_id');
		$product = Product::find($product_id);

		$this->shoppingcart->add_product($product);

		return json_encode($this->shoppingcart);	
	}

	public function get_count()
	{
		return $this->shoppingcart->count_products();
	}

	public function delete_destroy($product_id)
	{
		if(empty($product_id)) return;

		$product = Product::find($product_id);

		$this->shoppingcart->remove_product($product);

		return json_encode($this->shoppingcart);	
	}
}