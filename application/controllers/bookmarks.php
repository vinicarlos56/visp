<?php

class Bookmarks_Controller extends Base_Controller {
	public $restful = true;
	public $layout  = 'layout.main_user';

	public function post_create(){
		$bookmark = new Bookmark;
		$user_id  = Auth::user()->id;//Input::get('user_id');
		$product_id = Input::get('product_id');		
		$json_response['status'] = 0;
		if($bookmark->add($user_id,$product_id)){
			$json_response['status'] = 1;			
		}else{
			$json_response['error'] = 'Não Foi possível adicionar esse produto aos favoritos.';
		}
		return json_encode($json_response);
	}

	public function delete_destroy($id){
		$bookmark = Bookmark::find($id);
		
		$json_response['status'] = 0;
		if($bookmark->delete()){
			$json_response['status'] = 1;
		}

		return json_encode($json_response);
	}

	public function get_view(){
		$user = Auth::user();

		$bookmarks = Bookmark::where('user_id','=',$user->id)->get();
		
		if(!empty($bookmarks))
			foreach ($bookmarks as $bookmark) {
				$product = IoC::resolve('product',array($bookmark->product_id));
				$product->bookmark_id = $bookmark->id;
				$products[] = $product;
			}
		else{
			$products = array();
		}

		return $this->layout->nest('content','users.bookmarks',array('bookmarks'=>$products));
	}
}