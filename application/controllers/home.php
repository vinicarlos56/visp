<?php

class Home_Controller extends Base_Controller {

	public $restful = true;

	public function get_index(){	

		$products = Product::where('status','=','1')
			->where('price','>',0)
			->where('sold','=',0)
			->join('photos', 'products.id', '=', 'photos.model_id')
			->where('photos.type_id','=',Photo::PRODUCT)
			->distinct()
			->get(array('products.id','products.size','products.price'));
		
		foreach ($products as $obj) {
			$pro[] = IoC::resolve('product',array($obj->id));
		}

		$sliders = Banner::where('type','=','slider')->get();
		$side 	 = Banner::where('type','=','side')->get();
		
		$this->layout->nest('content','home.index',array(
			'products' => isset($pro) ? $pro : array(),
		))->with(array(
			'side'=>$side,
			'sliders'=>$sliders

		));
		
	}

	public function post_newsletter(){
		$email = Input::get('newsletter_email');

		if($email){
			if(Newsletter::create(array('email'=>$email))){
				return Redirect::to_route('home');
			}
		}
	}

}