<?php

class Users_Controller extends Base_Controller
{

	public $restful = true;
	public $layout  = "layout.main_user";

	public function __construct(){
		parent::__construct();

	    // $this->filter('before', 'auth')->only(array(
	    // 	'index',
	    // 	'perfil',
	    // 	'edit',
	    // 	'moip',
	    // 	'rating'
	    // ));
	}

	public function get_index(){
		$user = Auth::user();
		return View::make('users.index')
					 ->with('user',$user)
					 ->first();
	}

	public function get_logout(){
		$user = Auth::user();		

		Auth::logout();
		Session::forget('shoppingcart'); // FIXME: Refatorar funcionalidade do shoppngcart

		if (!empty($user->fb_id)) {

			$facebook = new Facebook(array(
			  'appId'  => '410973395664819',
			  'secret' => '9e5010bb6d532088df4e6f887977f084',
			));

			$url = $facebook->getLogoutUrl(array('next'=>URL::to_route('login')));
			session_destroy();
		} else {
			$url = URL::to_route('login');
		}	

    	return Redirect::to($url);
	}

	public function get_login(){
	    return View::make('users.login');
	}

	// TODO: Fazer a verificação se é admin e não permitir

	public function post_login(){		
	    $userdata = array(
	        'username'      => Input::get('login'),
	        'password'      => Input::get('senha')
	    );
	    
	    if ( Auth::attempt($userdata) )
	        return Redirect::to('users/'.Auth::user()->nickname);
	    else
	        return Redirect::to_route('login')->with('login_errors', true);	        
	    
	}

	public function get_view($nickname){

		$user = User::where('nickname','=',$nickname)->first();
		$products = Product::where('user_id','=',$user->id)->get();
		return $this->layout->nest(
			'content','users.index',array('user'=>$user,'products'=>$products)
		)->with('user',$user);
	}

	public function get_edit($user_id){

		$user = User::find($user_id);
		$address = !empty($user->address) ? $user->address : new Address; // TODO: extract to a method
		
		return $this->layout->nest(
			'content','users.edit',array('user'=>$user,'address'=>$address)
		)->with('user',$user);
	}

	public function get_register(){		
		return View::make('users.new');		
	}

	public function post_create(){		

		$mailer = IoC::resolve('mailer');		
		$input 	= Input::all();
		$email 	= Input::get('user_email');

		User::$validation_rules = array(		
			//'user_id'   => 'required', 
			'name'      => 'required',
			'password'	=> 'required|confirmed',			
			'email'     => 'required|email|confirmed|unique:users',
			'nickname'  => 'required|unique:users',
			'birthday' 	=> 'required',
			'sex'		=> 'required',
			'cpf'		=> 'unique:users',
			'cnpj'		=> 'unique:users',
			'cell_phone'=> 'required',
			'phone'		=> 'required'
		);

		$cpf = Input::get('user_cpf');
		$cnpj = Input::get('user_cnpj');		

		if($cpf or $cnpj) // FIXME: Loucura da cliente
			$flag = true;
		else
			$flag = false;
		

		$validation = User::validate($input,array('user','address'));		

		if (!$validation) {
			return Redirect::to_route("register")
				->with('errors',User::errors_to_string())				
				->with_input('except', array('user_password','user_password_confirmation'));
		}elseif(!$flag){
			return Redirect::to_route("register")
						->with('identifier','O Campo CPF ou CNPJ devem ser preenchidos!')
						->with_input('except', array('user_password','user_password_confirmation'));
		}else{
			$user = new User;
			$user->save_user($input);

			// $message = Swift_Message::newInstance('Message From Website')
			//     ->setFrom(array('vinicarlos57@gmail.com'=>'Mr Example'))
			//     ->setTo(array($email=>'Mr Example'))
			//     ->addPart('My Plain Text Message','text/plain')
			//     ->setBody('My HTML Message','text/html');

			// // Send the email
			// $mailer->send($message);

			return Redirect::to_route('home')->with('success','Usuário adicionado com sucesso!');
		}
	}

	public function put_update(){	

		$input 		= Input::all();
		$childrens  = Input::get('childrens'); 
		
		User::$validation_rules = array(		
			'id'   => 'required', 
			'name'      => 'required',
			'birthday' 	=> 'required',
			'sex'		=> 'required',			
			'cell_phone'=> 'required',
			'phone'		=> 'required'
		);

		$validation = User::validate($input,array('user','address'));		
		$user_id	= Input::get('user_id');//$input['id']; //modificado para não dar merda no explode

		if (!$validation) {
			return Redirect::to("users/{$user_id}/edit")->with('errors',User::errors_to_string());
		}else{
			$user = new User;
			$user->save_user($input,$user_id,$childrens);

			return Redirect::to("users/{$user_id}/edit")
						   ->with('success','Usuário alterado com sucesso!');
		}
	}

	public function put_moip()
	{
		$email_moip = Input::get('contamoip');
		$user = Auth::user();


		if ($user->check_moip_email($email_moip)) {			
			$user->email_moip = $email_moip;
		    $user->save();
		    return $this->get_moip();
		} else {
			return Redirect::to('conta-moip')->with('errors','O email informado não é um email moip válido.');	
		}
	}

	public function get_moip(){		
		$user = Auth::user();
		return $this->layout->nest(
			'content','users.moip',array('user'=>$user)
		)->with('user',$user);
	}

	public function post_avatar(){
		$user_id = Input::get('user_id');
		$photo 	 = new Photo;
		// TODO: Remover imagem caso ela exista para substituir pela nova
		$photo->save_photo(array(
			'input_name'=> 'avatar',
			'title'		=> 'title',
			'alt'		=> 'title',
			'file_name'	=> Input::file('avatar.name'),
			'type_id'	=> Photo::USER,
			'model_id'	=> $user_id		
		))->thumb(175,219)
		  ->thumb(56,56);

		User::update($user_id,array('avatar'=>Input::file('avatar.name')));

		return Redirect::to("users/{$user_id}/edit");
	}

	public function post_rating(){
		// TODO: Refatorar, registrar o usuáro que está avaliando, o usuário avaliado e o produto
		$product_id = Input::get('product_id');
		$user_id 	= Product::find($product_id)->user->id;
		$rating 	= Input::get('rating');
		$json_response['status'] = 0;

		if(Rating::create(array(
			'product_id' => $product_id, // FIXME: refatorar, essa informação não será utilizada
			'user_id' 	 => $user_id,
			'rating' 	 => $rating,
		)))
			$json_response['status'] = 1;

		return json_encode($json_response);
	}

	public function get_check_moip($email)
	{
		$user = new User;

		if($user->check_emaip_moip($email))
			$json_response['status'] = 0;
		else
			$json_response['status'] = 1;

		return json_encode($json_response);

	}

	public function post_change_password()
	{

		$input = Input::all();		
		$user  = Auth::user();
		$valid_actual_pass = Hash::check($input['actual_password'], $user->password);
		$json_response['status'] = 0;

		if ($valid_actual_pass) {
			if ($input['new_password'] === $input['new_password_confirmation']) {
				$user->password = Hash::make($input['new_password']);	
				$user->save();
				$json_response['status'] = 1;
			} else {
				// TODO: Validate if the new password not match
			}
		}
		return json_encode($json_response);
	}
}