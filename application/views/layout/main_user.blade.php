@layout('layout.main')

@section('sidebar')
@endsection

@section('content')
<!-- FIXME: Consertar o nome das classes na div -->

@yield('div')
<!-- <div class="minhavitrine meusdados"> -->
<!-- <div class="centralizando content minhavitrine moip"> -->
<!-- <div class="centralizando content minhavitrine minhasvendas"> -->


	<h2>MINHA VITRINE VIsP</h2>

	<!-- BARRA DE VITRINE -->
	<div class="barradevitrinemenu">
		<div class="primeiro passoapasso">
			<a href="{{URL::to_route('moip_account')}}">conta moip</a>
		</div>

		<div class="segundo passoapasso">
			<a href="{{URL::to_route('my_products')}}">minhas vendas</a>
		</div>

		<div class="terceiro passoapasso">
			<a href="{{URL::to_route('my_purchases')}}">minhas compras</a>
		</div>

		<div class="quarto passoapasso">
			<a href="{{URL::to("users/".Auth::user()->id."/edit")}}">meus dados</a>
		</div>

		<div class="quinto passoapasso">
			<a href="#">quero vender</a>
		</div>

		<div class="sexto passoapasso">
			<a href="{{URL::to_route('bookmarks')}}">favoritos</a>
		</div>
	</div><!-- FIM BARRA DE VITRINE -->

	<!-- SIDEBAR -->
	<div id="sidebar">

		<!-- AVATAR E NOME -->
			<div class="avatar">
				@if(Auth::user()->avatar)
					{{HTML::image(URL::to('uploads/thumbs/thumb_175x219_').Auth::user()->avatar,null,array('style'=>'max-width:170px;max-height:219px'))}}
				@elseif(Auth::user()->fb_id)
					{{HTML::image(URL::to('https://graph.facebook.com/'.Auth::user()->fb_id.'/picture?width=170&height=219'),null,array('style'=>'max-width:170px;max-height:219px'))}}					
				@endif
			</div>
			<h3 class="nome">{{Auth::user()->nickname}}</h3>
		<!-- FIM AVATAR E NOME -->

		<!-- PRODUTOS COMPRADOS -->
		<div class="retangulorosa">
			<div class="numero">{{Auth::user()->get_purchases()}}</div>
			<div class="traco"></div>
			<div class="texto">PRODUTOS<br />COMPRADOS</div>
		</div><!-- FIM PRODUTOS COMPRADOS -->

		<!-- PRODUTOS VENDIDOS -->
		<div class="retangulorosa">
			<div class="numero">{{Auth::user()->get_sales()}}</div>
			<div class="traco"></div>
			<div class="texto">PRODUTOS<br />VENDIDOS</div>
		</div><!-- FIM PRODUTOS VENDIDOS -->

		<!-- AVALIACAO -->
		<div class="avaliacao">
			<div class="qualificacao">Sua qualificação</div>
			<ul class="estrelinhas">
				@for($i = 1; $i <= 5; $i++)
                  @if($i <= Auth::user()->rating_value())
                    <li class="active"></li>
                  @else
                    <li></li>
                  @endif
                @endfor
			</ul>
			<div class="avaliacoes">{{Auth::user()->count_ratings()}} avaliações</div>
		</div><!-- FIM AVALIACAO -->

		<!-- ORIGEM DO PRODUTO -->
		<div id="origemdoproduto">
			<h3>origem do produto</h3>
			<div class="retangulo">{{isset(Auth::user()->address->city) ? Auth::user()->address->city : '' }} - {{isset(Auth::user()->address->state) ? Auth::user()->address->state : ''}}</div>
		</div><!-- FIM ORIGEM DO PRODUTO -->
	</div><!-- FIM SIDEBAR -->
	

		@yield('user_content')
</div> 

@endsection