<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

  <meta http-equiv="content-language" content="pt-br" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="refresh" content="1080" />
  <meta http-equiv="Content-Script-Type" content="text/javascript" />
  <meta http-equiv="Content-Style-Type" content="text/css" />

  <meta name="description" content="lorem lorem lorem lorem lorem"/>
  <meta name="keywords" content="lorem lorem lorem lorem lorem"/>

  <title>VIsP</title>
  
  {{HTML::style('favicon.ico',array('rel'=>'shortcut icon', 'type'=>'image/x-icon'))}}
  {{HTML::style('assets/layout/css/style.css')}}
  {{HTML::script('assets/lib/js/jquery-1.8.2.min.js')}}
  <!-- <script type="text/javascript" src="js/jquery.min.js"></script> -->

</head>

<body>

<!-- CONTAINER -->
<div id="container">
	<!-- HEADER-->
	<div id="header">
		<!-- TOP -->
		<div class="top">
			@section('navigation')
				<div class="centralizando">
					<!-- <a href="{{URL::base();}}">ViSP</a> -->
					<a href="{{URL::to('central-de-atendimento')}}" class="atendimento">Atendimento - Ajuda</a>
					<ul style="margin-left:200px" class="entrada">				
						@if (Auth::check())
							<li class="first"><a href="{{URL::to('users/'.Auth::user()->nickname); }}">olá, {{Auth::user()->name;}}</a></li>

							<li><a href="{{URL::to("users/".Auth::user()->id."/edit")}}">Editar meus dados</a></li>					
							<li>{{HTML::link_to_route('new_product', 'Adicionar Produtos')}}</li>
							<li>{{HTML::link_to_route('my_products', 'Meus Produtos')}}</li>

							<li>{{HTML::link_to_route('logout', 'Logout')}}</li>		
						@else
							<li class="first">Olá, visitante</li>
							<li>{{HTML::link_to_route('login','Login')}}</li>
							<li>{{HTML::link_to_route('register','Cadastro')}}</li>
						@endif						
					</ul>
					<div class="carrinho">
						<a href="{{URL::to('products/checkout')}}">
							<div class="minhascompras">Minhas compras <span></span></div>
							<div class="icon"></div>
						</a>
					</div>					

				</div>
			@yield_section
		</div><!-- FIM TOP -->

		<div class="centralizando">
			<h1>
				<a id="logo_visp" href="{{URL::base()}}">
					{{HTML::image('assets/layout/images/logo-visp.png', 'Logo ViSP', array('title'=>'Logo ViSP'))}}					
				</a>
			</h1>

			<div class="direitologo">
				<form action="{{URL::to_route('general_search')}}">
					<input type="text" name="term" class="busca" placeholder="O que você quer encontrar?" />
					<input type="submit" class="btnbuscar botao" />
				</form>
				<ul class="redesocial">
					<li class="compartilhe"></li>
					<li class="facebook"><a href="#" target="_blank" title="Facebook" alt="Facebook">Facebook</a></li>
					<li class="gplus"><a href="#" target="_blank" title="Google Plus" alt="Google Plus">Google Plus</a></li>
					<li class="pinterest"><a href="#" target="_blank" title="Pinterest" alt="Pinterest">Pinterest</a></li>
				</ul>
				<ul class="menu">
					<li>
						{{HTML::link('como-funciona', 'COMO FUNCIONA',array('alt'=>'COMO FUNCIONA','title'=>'COMO FUNCIONA'))}}
					</li>
					<li>
						{{HTML::link('quero-vender', 'QUERO VENDER',array('alt'=>'QUERO VENDER','title'=>'QUERO VENDER'))}}
					</li>
					<li>
						{{HTML::link('quero-comprar', 'QUERO COMPRAR',array('alt'=>'QUERO COMPRAR','title'=>'QUERO COMPRAR'))}}						
					</li>
				</ul>
			</div>
		</div>
	</div><!-- FIM HEADER -->

	@if(URI::segment(1) == '')
		<!-- SLIDER -->
		<div id="slider" class="royalSlider">
			<ul class="royalSlidesContainer">
				<!-- SLIDER 1 -->
				@if(empty($sliders))
					<li class="royalSlide"> 
						<a href="#">
							<img src="assets/layout/images/slider1.jpg" />
						</a>
					</li>
				@else
					@foreach($sliders as $banner)
						<li class="royalSlide"> 
							<a href="#">
								<img src="{{URL::to("uploads/thumbs/thumb_960x305_{$banner->photo->file_name}")}}" />
							</a>
						</li>
					@endforeach
				@endif
			</ul>
		</div><!-- FIM SLIDER -->
	@endif

	<!-- RECEBA NOVIDADES -->
	<div id="recebanovidades">
		<div class="centralizando">
			<div class="fechar">X FECHAR</div>
			<h1>primeira vez aqui?</h1>
			<p>junte-se a VIsp! Fique por dentro das novidades</p>
			<form>
				<label>NOME</label>
				<input type="text" name="nome" class="nome" />
				<label class="email">EMAIL</label>
				<input type="text" name="email" class="email" />
				<input type="submit" class="cadastrar" value="CADASTRAR" /> 
			</form>
		</div>
	</div><!-- FIM RECEBA NOVIDADES -->
	
	<!-- CENTRO -->
	<div class="centralizando content">

		<!-- TODO: Integrar o sitema de breadcumb e ajustar o funcionamento -->
		@if(isset($product) AND URI::segment(1) == 'produto')
			<!-- BREADCRUMB -->
			<div id="breadcrumb" class="{{$product->subcategory()->get_css_class()}}">

				<a href="#">{{$product->category()->get_name()}} </a> > {{$product->subcategory()->get_name()}}

			</div><!-- FIM BREADCRUMB -->
		@elseif(isset($subcategory_name))
			<!-- BREADCRUMB -->
			<div id="breadcrumb" class="{{$subcategory_name}}">

				<a href="#">{{$category}} </a> > {{$subcategory}}

				<!-- <div class="ordenarpor">
			      <div class="mascaraordenarpor"><span>ORDENAR POR</span> <div class="seta"></div></div>
			      <select name="search_order" id="ordenarpor">
			        <option>Ordenar por</option>
			        <option value="recente">Mais recente</option>
			        <option value="antigo">Mais antigo</option>
			        <option value="precomenor">Menor preço</option>
			        <option value="precomaior">Maior preço</option>
			      </select>
			    </div>
 -->
			</div><!-- FIM BREADCRUMB -->
		@endif

		@section('sidebar')
			<!-- SIDEBAR -->
			<div id="sidebar">
				<h1>NAVEGUE POR AQUI</h1>
				<!-- ACCORDION CATEGORIAS -->
				<div id="accordiondepartamento" class="accordionWrapper" style="width:230px;">
					<div class="set roupas">
						<div class="title">
							<div class="icon"></div>
							<h2>ROUPAS</h2>
						</div>
						<div class="content">
							<ul>
								<li><a href="{{URL::to('produtos/buscar/roupas/meninos')}}">Meninos</a></li>
								<li><a href="{{URL::to('produtos/buscar/roupas/meninas')}}">Meninas</a></li>
								<li><a href="{{URL::to('produtos/buscar/roupas/unissex')}}">Unissex</a></li>
								<li><a href="{{URL::to('produtos/buscar/roupas/gemeos')}}">Gêmeos</a></li>
							</ul>
						</div>
					</div>

					<div class="set sapatos">
						<div class="title">
							<div class="icon"></div>
							<h2>SAPATOS</h2>
						</div>
						<div class="content">
							<ul>
								<li><a href="{{URL::to('produtos/buscar/sapatos/meninos')}}">Meninos</a></li>
								<li><a href="{{URL::to('produtos/buscar/sapatos/meninas')}}">Meninas</a></li>
								<li><a href="{{URL::to('produtos/buscar/sapatos/unissex')}}">Unissex</a></li>
								<li><a href="{{URL::to('produtos/buscar/sapatos/gemeos')}}">Gêmeos</a></li>
							</ul>
						</div>
					</div>

					<div class="set acessorios">
						<div class="title">
							<div class="icon"></div>
							<h2>ACESSÓRIOS</h2>
						</div>
						<div class="content">
							<ul>
								<li><a href="{{URL::to('produtos/buscar/acessorios/meninos')}}">Meninos</a></li>
								<li><a href="{{URL::to('produtos/buscar/acessorios/meninas')}}">Meninas</a></li>
								<li><a href="{{URL::to('produtos/buscar/acessorios/unissex')}}">Unissex</a></li>
								<li><a href="{{URL::to('produtos/buscar/acessorios/gemeos')}}">Gêmeos</a></li>
							</ul>
						</div>
					</div>

					<div class="set brinquedos">
						<div class="title">
							<div class="icon"></div>
							<h2>BRINQUEDOS</h2>
						</div>
						<div class="content">
							<ul>
								<li><a href="{{URL::to('produtos/buscar/brinquedos/meninos')}}">Meninos</a></li>
								<li><a href="{{URL::to('produtos/buscar/brinquedos/meninas')}}">Meninas</a></li>
								<li><a href="{{URL::to('produtos/buscar/brinquedos/unissex')}}">Unissex</a></li>
								<li><a href="{{URL::to('produtos/buscar/brinquedos/gemeos')}}">Gêmeos</a></li>
							</ul>
						</div>
					</div>

					<div class="set livrosedvds">
						<div class="title">
							<div class="icon"></div>
							<h2>LIVROS E DVDS</h2>
						</div>
						<div class="content">
							<ul>
								<li><a href="{{URL::to('produtos/buscar/livrosedvds/meninos')}}">Meninos</a></li>
								<li><a href="{{URL::to('produtos/buscar/livrosedvds/meninas')}}">Meninas</a></li>
								<li><a href="{{URL::to('produtos/buscar/livrosedvds/unissex')}}">Unissex</a></li>
								<li><a href="{{URL::to('produtos/buscar/livrosedvds/gemeos')}}">Gêmeos</a></li>
							</ul>
						</div>
					</div>

					<div class="set enxoval">
						<div class="title">
							<div class="icon"></div>
							<h2>ENXOVAL</h2>
						</div>
						<div class="content">
							<ul>
								<li><a href="{{URL::to('produtos/buscar/enxoval/meninos')}}">Meninos</a></li>
								<li><a href="{{URL::to('produtos/buscar/enxoval/meninas')}}">Meninas</a></li>
								<li><a href="{{URL::to('produtos/buscar/enxoval/unissex')}}">Unissex</a></li>
								<li><a href="{{URL::to('produtos/buscar/enxoval/gemeos')}}">Gêmeos</a></li>
							</ul>
						</div>
					</div>

					<div class="set moveisedecoracao">
						<div class="title">
							<div class="icon"></div>
							<h2>MÓVEIS E DECORAÇÃO</h2>
						</div>
						<div class="content">
							<ul>
								<li><a href="{{URL::to('produtos/buscar/moveisedecoracao/meninos')}}">Meninos</a></li>
								<li><a href="{{URL::to('produtos/buscar/moveisedecoracao/meninas')}}">Meninas</a></li>
								<li><a href="{{URL::to('produtos/buscar/moveisedecoracao/unissex')}}">Unissex</a></li>
								<li><a href="{{URL::to('produtos/buscar/moveisedecoracao/gemeos')}}">Gêmeos</a></li>
							</ul>
						</div>
					</div>

					<div class="set cadeirasecarrinhos">
						<div class="title">
							<div class="icon"></div>
							<h2>CADEIRAS E CARRINHOS</h2>
						</div>
						<div class="content">
							<ul>
								<li><a href="{{URL::to('produtos/buscar/cadeirasecarrinhos/meninos')}}">Meninos</a></li>
								<li><a href="{{URL::to('produtos/buscar/cadeirasecarrinhos/meninas')}}">Meninas</a></li>
								<li><a href="{{URL::to('produtos/buscar/cadeirasecarrinhos/unissex')}}">Unissex</a></li>
								<li><a href="{{URL::to('produtos/buscar/cadeirasecarrinhos/gemeos')}}">Gêmeos</a></li>
							</ul>
						</div>
					</div>
				</div><!--FIM DE ACCORDEON DE DEPARTAMENTOS -->

				<h1>FILTRAR POR</h1>
				<!-- FILTRAR POR -->
				{{Form::open(URI::current(), 'GET')}}
					<div class="filtrar-por">
						<h2>TIPOS DE ENVIO</h2>
						<ul>
							<li>
								@if(isset($filter['frete']) and in_array('gratis',$filter['frete']))
									<a style="font-weight:bold" href="#">Frete Grátis</a> 
									<input style="display:none" type="checkbox" checked="checked" name="frete[]" value="gratis">
								@else
									<a href="#">Frete Grátis</a> 
									<input style="display:none" type="checkbox" name="frete[]" value="gratis">
								@endif
							</li>
							<li>
								@if(isset($filter['frete']) and in_array('combinar',$filter['frete']))
									<a style="font-weight:bold" href="#">A combinar</a>
									<input style="display:none" type="checkbox" checked="checked" name="frete[]" value="combinar">
								@else
									<a href="#">A combinar</a>
									<input style="display:none" type="checkbox" name="frete[]" value="combinar">
								@endif
							</li>
							<li>
								@if(isset($filter['frete']) and in_array('normal',$filter['frete']))
									<a style="font-weight:bold" href="#">Frete Normal</a> 
									<input style="display:none" type="checkbox"  checked="checked" name="frete[]" value="normal">
								@else
									<a href="#">Frete Normal</a> 
									<input style="display:none" type="checkbox" name="frete[]" value="normal">
								@endif
							</li>
						</ul>

						<h2>POR ESTADO</h2>
						<div class="mascaraestados"><span>selecione o estado</span></div>
						<select name="estado">
							<option value="">selecione o estado</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'AC') ? 'selected="selected"': ''?> value="AC">Acre</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'AL') ? 'selected="selected"': ''?> value="AL">Alagoas</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'AM') ? 'selected="selected"': ''?> value="AM">Amazonas</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'AP') ? 'selected="selected"': ''?> value="AP">Amapá</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'BA') ? 'selected="selected"': ''?> value="BA">Bahia</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'CE') ? 'selected="selected"': ''?> value="CE">Ceará</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'DF') ? 'selected="selected"': ''?> value="DF">Distrito Federal</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'ES') ? 'selected="selected"': ''?> value="ES">Espirito Santo</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'GO') ? 'selected="selected"': ''?> value="GO">Goiás</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'MA') ? 'selected="selected"': ''?> value="MA">Maranhão</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'MG') ? 'selected="selected"': ''?> value="MG">Minas Gerais</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'MS') ? 'selected="selected"': ''?> value="MS">Mato Grosso do Sul</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'MT') ? 'selected="selected"': ''?> value="MT">Mato Grosso</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'PA') ? 'selected="selected"': ''?> value="PA">Pará</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'PB') ? 'selected="selected"': ''?> value="PB">Paraíba</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'PE') ? 'selected="selected"': ''?> value="PE">Pernambuco</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'PI') ? 'selected="selected"': ''?> value="PI">Piauí</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'PR') ? 'selected="selected"': ''?> value="PR">Paraná</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'RJ') ? 'selected="selected"': ''?> value="RJ">Rio de Janeiro</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'RN') ? 'selected="selected"': ''?> value="RN">Rio Grande do Norte</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'RO') ? 'selected="selected"': ''?> value="RO">Rondônia</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'RR') ? 'selected="selected"': ''?> value="RR">Roraima</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'RS') ? 'selected="selected"': ''?> value="RS">Rio Grande do Sul</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'SC') ? 'selected="selected"': ''?> value="SC">Santa Catarina</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'SE') ? 'selected="selected"': ''?> value="SE">Sergipe</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'SP') ? 'selected="selected"': ''?> value="SP">São Paulo</option>
							<option <?=(isset($filter['estado']) and $filter['estado'] == 'TO') ? 'selected="selected"': ''?> value="TO">Tocantins</option>
						</select>

						<h2>PREÇO</h2>
						<div class="de-por">
							<label class="de">de R$</label>
							<label class="por">até R$</label>

							<input type="text" class="de"  value="<?=(isset($filter['preco_de']))? $filter['preco_de'] : ''?> " name="preco_de" />
							<input type="text" class="por" value="<?=(isset($filter['preco_por']))? $filter['preco_por'] : ''?> " name="preco_por" />
						</div>
						<ul>
							<li>
								<a <?=(isset($filter['preco']) and $filter['preco'] == '0-50') ? 'style="font-weight:bold"' : ''?> href="#">Até R$ 50</a>
								<input style="display:none" type="radio" name="preco" <?=(isset($filter['preco']) and $filter['preco'] == '0-50') ? 'checked="checked"' : ''?> value="0-50">
							</li>
							<li>
								<a <?=(isset($filter['preco']) and $filter['preco'] == '50-100') ? 'style="font-weight:bold"' : ''?> href="#">R$ 50 a R$ 100</a>
								<input style="display:none" type="radio" name="preco" <?=(isset($filter['preco']) and $filter['preco'] == '50-100') ? 'checked="checked"' : ''?> value="50-100">
							</li>
							<li>
								<a <?=(isset($filter['preco']) and $filter['preco'] == '100-200') ? 'style="font-weight:bold"' : ''?> href="#">R$ 100 a R$ 200</a>
								<input style="display:none" type="radio" name="preco" <?=(isset($filter['preco']) and $filter['preco'] == '100-200') ? 'checked="checked"' : ''?> value="100-200">
							</li>
							<li>
								<a <?=(isset($filter['preco']) and $filter['preco'] == '200-0') ? 'style="font-weight:bold"' : ''?> href="#">Acima de R$ 200</a>
								<input style="display:none" type="radio" name="preco" <?=(isset($filter['preco']) and $filter['preco'] == '200-0') ? 'checked="checked"' : ''?> value="200-0">
							</li>
						</ul>

						<h2>CONDIÇÃO</h2>
						<ul>
							<li>
								<a <?=(isset($filter['condicao']) and $filter['condicao'] == 'nuncausado') ? 'style="font-weight:bold"' : ''?>  href="#">Novo</a>
								<input style="display:none" type="radio" name="condicao" <?=(isset($filter['condicao']) and $filter['condicao'] == 'nuncausado') ? 'checked="checked"' : ''?>  value="nuncausado">
							</li>
							<li>
								<a <?=(isset($filter['condicao']) and $filter['condicao'] == 'usado') ? 'style="font-weight:bold"' : ''?> href="#">Usado</a>
								<input style="display:none" type="radio" name="condicao"  <?=(isset($filter['condicao']) and $filter['condicao'] == 'usado') ? 'checked="checked"' : ''?> value="usado">
							</li>
						</ul>
						@if(isset($subcategory_name))
						
							<div class="ordenarpor">
						      <div class="mascaraordenarpor"><span>ORDENAR POR</span> <div class="seta"></div></div>
						      <select name="search_order" id="ordenarpor">
						        <option>Ordenar por</option>
						        <option <?=(isset($filter['search_order']) and $filter['search_order'] == 'recente') ? 'selected="selected"' : ''?> value="recente">Mais recente</option>
						        <option <?=(isset($filter['search_order']) and $filter['search_order'] == 'antigo') ? 'selected="selected"' : ''?> value="antigo">Mais antigo</option>
						        <option <?=(isset($filter['search_order']) and $filter['search_order'] == 'precomenor') ? 'selected="selected"' : ''?> value="precomenor">Menor preço</option>
						        <option <?=(isset($filter['search_order']) and $filter['search_order'] == 'precomaior') ? 'selected="selected"' : ''?> value="precomaior">Maior preço</option>
						      </select>
						    </div>
						@endif

						<input type="submit" value="Filtrar">
					</div><!-- FIM FILTRAR POR -->
				{{Form::close()}}
				

				<!-- FACEBOOK -->
				<!-- <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FVispStore&amp;width=250&amp;height=258&amp;show_faces=true&amp;colorscheme=light&amp;stream=false&amp;border_color&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:250px; height:291px; margin-top: 20px;" allowTransparency="true"></iframe> -->
				<!-- FIM FACEBOOK -->

				<!-- NEWSLETTER -->
				<div class="newsletter">
					<h2>RECEBA A VIsP POR EMAIL</h2>
					<p>lorem ipsum dolor sit amet consectur</p>
					<form class="news" action="newsletter" method="POST">
						<input type="text" name="newsletter_email" class="email" placeholder="Digite seu email" />
						<input type="submit" class="assinar" />
					</form>
				</div><!-- FIM NEWSLETTER -->
				
				@if(isset($side))
					@foreach($side as $banner)
						<!-- BANNER -->
						<div class="banner">
							<a href="#">
								<img src="{{URL::to("uploads/thumbs/thumb_250x90_{$banner->photo->file_name}")}}" alt="" title="" />
							</a>
						</div><!-- FIM BANNER -->
					@endforeach
				@endif				
			</div><!-- FIM SIDEBAR -->
		@yield_section

		@yield('content')

		<!-- FOOTER -->
		<div id="footer">
			<ul class="menufooter">
				<li><a href="#" title="QUEM SOMOS" alt="QUEM SOMOS">QUEM SOMOS</a></li>
				<li><a href="{{URL::to('central-de-atendimento')}}" title="PRECISA DE AJUDA?" alt="PRECISA DE AJUDA?">PRECISA DE AJUDA?</a></li>
				<li>{{HTML::link('quero-vender', 'QUERO VENDER',array('alt'=>'QUERO VENDER','title'=>'QUERO VENDER'))}}</li>
				<li>{{HTML::link('quero-comprar', 'QUERO COMPRAR',array('alt'=>'QUERO COMPRAR','title'=>'QUERO COMPRAR'))}}</li>
			</ul>

			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quam libero, tincidunt nec placerat sollicitudin, cursus ac massa. Suspendisse potenti. In mattis lorem in lacus semper vitae elementum quam elementum. Etiam ac enim felis, vel pellentesque velit. Nulla feugiat semper sapien eu vehicula. Nunc ullamcorper rhoncus sollicitudin. Pellentesque placerat dui ut ipsum semper id varius justo suscipit. Donec consequat purus at metus facilisis fringilla.</p>

			<!-- SUBFOOTER -->
			<div class="subfooter">
				<div class="moip"></div>
				<ul class="formas-de-pagamento">
					<li class="formas">Formas de pagamento</li>
					<li class="master">MasterCard</li>
					<li class="visa">Visa</li>
					<li class="diners">Diners</li>
					<li class="amex">Amex</li>
					<li class="bb">Banco do Brasil</li>
					<li class="itau">Itaú</li>
					<li class="bradesco">Bradesco</li>
					<li class="boleto">Boleto Bancário</li>
				</ul>
				<div class="certificados">aqui fica os certificados de segurança</div>
			</div><!-- FIM SUBFOOTER -->

			<!-- COPYRIGHT -->
			<div class="copyright-politica-e-termo">
				<div class="copyright">
				copyright 2012 - todos os direitos reservados
				</div>

				<ul class="politica-e-termo">
					<li><a href="#">Politica de privacidade</a></li>
					<li><a href="#">Termos de uso</a></li>
				</ul>
			</div><!-- FIM COPYRIGHT -->
		</div><!-- FIM FOOTER -->

	</div><!-- CENTRO -->
</div><!-- CONTAINER -->


<!-- SLIDER -->
{{HTML::script('assets/lib/js/jquery.easing.min.js')}}
{{HTML::script('assets/lib/js/royal-slider-8.1.js')}}
<!-- <script type="text/javascript" src="js/jquery.easing.min.js"></script> 
<script type="text/javascript" src="js/royal-slider-8.1.js"></script> -->
<!-- FIM SLIDER -->

<!-- PLACEHOLDER PARA IE -->
{{HTML::script('assets/lib/js/placeholder.js')}}
<!-- <script type="text/javascript" src="js/placeholder.js"></script>  -->
<!-- FIM PLACEHOLDER PARA IE -->

<!-- ACCORDION -->
{{HTML::script('assets/lib/js/jquery.msAccordion.js')}}
<!-- <script type="text/javascript" src="js/jquery.msAccordion.js"></script>  -->
<!-- FIM ACCORDION -->

<!-- MASCARA DINHEIRO -->
{{HTML::script('assets/lib/js/jquery.maskMoney.js')}}
<!-- <script type="text/javascript" src="js/jquery.maskMoney.js"></script>  -->
<!-- FIM MASCARA DINHEIRO -->

<!-- TOOLTIP PRATELEIRA -->
{{HTML::script('assets/lib/js/jquery.ezpz_tooltip.min.js')}}
<!-- <script type="text/javascript" src="js/jquery.ezpz_tooltip.min.js"></script>  -->
<!-- FIM TOOLTIP PRATELEIRA -->

{{HTML::script('assets/lib/js/jquery.cookie.js')}}

<!-- UF E CIDADE PAG CADASTRO -->
{{HTML::script('assets/layout/js/cidades-estados.js')}}
<!-- <script language="JavaScript" type="text/javascript" src="js/cidades-estados.js"></script> -->
<!-- FIM UF E CIDADE PAG CADASTRO -->

<!-- MASCARAS -->
{{HTML::script('assets/lib/js/jquery.maskedinput-1.1.4.pack.js')}}
<!-- <script language="JavaScript" type="text/javascript" src="js/jquery.maskedinput-1.1.4.pack.js"></script> -->
<!-- FIM MASCARAS -->

<!-- SCRIPTS GERAL DO SITE -->
{{HTML::script('assets/layout/js/scripts.js')}}
<!-- <script type="text/javascript" src="js/scripts.js"></script> -->
<!-- FIM SCRIPTS GERAL DO SITE -->

{{HTML::script('assets/layout/js/filter.js')}}

</body>
</html>