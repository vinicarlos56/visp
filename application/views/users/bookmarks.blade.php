@section('div')
 	<div class="centralizando content minhavitrine favoritos">
@endsection

@section('user_content') 

<!-- PRATELEIRA -->
    <div id="right">
      <h1>Favoritos</h1>
      @if(!empty($bookmarks))
      <ul class="prateleira">        
      	@foreach($bookmarks as $bookmark)
        <li>
          <div data-product-id="{{$bookmark->bookmark_id}}" class="close delete-bookmark">Deletar</div>
         
          <span id="tooltip" class="tooltip">
            <div class="nome">{{$bookmark->title}}</div>
            <div class="usadoounovo">{{$bookmark->state}}</div>
          </span>
          <a href="#">
            <div class="informacao">
              <div class="tamanho {{$bookmark->subcategory()->get_css_class()}}">
               {{$bookmark->size}}
              </div>
              <div class="preco {{$bookmark->subcategory()->get_css_class()}}">
                R$ {{$bookmark->price}}
              </div>
            </div>
           <img style="" src="{{URL::to("uploads/thumbs/thumb_190x190_{$bookmark->photos()->first()->file_name}")}}" alt="" title="" />
          </a>
        </li>
        @endforeach
    </ul>
    @endif
  <!-- <div class="exibirmais">
    <div class="mais">exibir mais produtos</div>
  </div> -->

</div><!-- FIM MAIN -->  
@endsection