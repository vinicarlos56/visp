@layout('layout.main')

@section('sidebar')
@endsection

@section('content')

{{Form::open('users', 'POST',array('class'=>'criarconta'))}}
	{{Form::token()}}

	<div class="error">
		@if (Session::has('errors'))
	    	<span>{{Session::get('errors')}}</span>
		@endif

		@if(Session::has('identifier'))
			<br>
			<span>{{Session::get('identifier')}}</span>
		@endif
	</div>


	<ul class="teladecadastro">
		<li>
			<div class="numeroli">1</div>
			<div class="linhadivisoria"></div>
			<h1>Seus dados para acesso</h1>
			<div class="inputs">
				{{Form::input('text','user_email',Input::old('user_email'),array('class'=>'email','placeholder'=>'Email'))}}
				{{Form::input('text','user_email_confirmation',Input::old('user_email_confirmation'),array('class'=>'confirmaremail','placeholder'=>'Confirmar email'))}}
				{{Form::input('password','user_password',null,array('class'=>'senha','placeholder'=>'Senha'))}}
				{{Form::input('password','user_password_confirmation',null,array('class'=>'confirmarsenha','placeholder'=>'Confirmar senha'))}}
				{{Form::input('text','user_nickname',Input::old('user_nickname'),array('class'=>'usuario','placeholder'=>'Usuário'))}}				
				<label>.vispstore.com</label>
			</div>
		</li>

		<li>
			<div class="numeroli">2</div>
			<div class="linhadivisoria"></div>
			<h1>Quem é você?</h1>
			<div class="inputs">
				{{Form::input('text','user_name',Input::old('user_name'),array('class'=>'nome','placeholder'=>'nome'))}}
				{{Form::input('text','user_birthday',Input::old('user_birthday'),array('class'=>'datadenascimento data','placeholder'=>'Data de nascimento'))}}
				<div class="mascaraselectsexo">Sexo</div>
				{{Form::select('user_sex',array('m'=>'Masculino','f'=>'Feminino'),Input::old('user_sex'),array('id'=>'sexo','class'=>'sexo'))}}

				<!-- <input type="text" name="telefone" id="telefone" class="telefone" placeholder="Telefone" /> -->
				{{Form::input('text','user_phone',Input::old('user_phone'),array('class'=>'telefone','id'=>'telefone','placeholder'=>'Telefone'))}}
				<!-- <input type="text" name="celular" id="celular" class="celular" placeholder="Celular" /> -->
				{{Form::input('text','user_cell_phone',Input::old('user_cell_phone'),array('class'=>'celular','id'=>'celular','placeholder'=>'Celular'))}}

				<div class="cpfoucnpj">
					<?php 
						$cpf  = Input::old('user_cpf');
						$cnpj = Input::old('user_cnpj');
						$cpf_check  = '';
						$cnpj_check = '';
						$cpf_style  = '';
						$cnpj_style = '';

						if($cpf OR $cnpj){
							if($cnpj){
								$cnpj_check = 'checked';
								$cpf_style = 'display:none';
							}else if($cpf){
								$cpf_check  = 'checked';
								$cnpj_style = 'display:none';
							}
						}else{
							$cpf_check = 'checked';
							$cnpj_style = 'display:none';
						}
						

					 ?>
					<input type="radio" name="tipo" value="cpf" id="radiocpf" {{$cpf_check}} />
					<label class="cpf">CPF</label>

					<input type="radio" name="tipo" value="cnpj" id="radiocnpj" {{$cnpj_check}} />
					<label class="cnpj">CNPJ</label>      
				</div>

				{{Form::input('text','user_cpf',Input::old('user_cpf'),array('id'=>'cpf','class'=>'cpf','placeholder'=>'CPF','style'=>$cpf_style))}}
				<!-- <input type="text" name="cnpj" id="cnpj" class="cnpj" placeholder="CNPJ" style="display: none" /> -->
				{{Form::input('text','user_cnpj',Input::old('user_cnpj'),array('id'=>'cnpj','class'=>'cnpj','placeholder'=>'CNPJ','style'=>$cnpj_style))}}
			</div>

			<p class="termo">Ao clicar em “Criar conta” você está<br />concordando com os termos de uso</p>
		</li>

		<li>
			<div class="numeroli">3</div>
			<h1>Onde você mora?</h1>
			<div class="inputs">
				{{Form::input('text','address_cep',Input::old('address_cep'),array('class'=>'cep','placeholder'=>'CEP'))}}
				{{Form::input('text','address_logradouro',Input::old('address_logradouro'),array('class'=>'endereco','placeholder'=>'Endereço'))}}
				{{Form::input('text','address_number',Input::old('address_number'),array('class'=>'numero','placeholder'=>'Número','onkeypress'=>'return SomenteNumero(event)'))}}
				{{Form::input('text','address_complement',Input::old('address_complement'),array('class'=>'complemento','placeholder'=>'Complemento'))}}
				{{Form::input('text', 'address_neighborhood',Input::old('address_neighborhood'),array('class'=>'bairro','placeholder'=>'Bairro'))}}
				
				<div class="mascaraselectuf">UF</div>
				{{Form::select('address_state',array(),Input::old('address_state'),array('id'=>'uf','class'=>'uf'))}}
				
				<div class="mascaraselectcidade">Cidade</div>
				{{Form::select('address_city',array(),Input::old('address_city'),array('id'=>'cidade','class'=>'cidade'))}}
			</div>
		</li>
	</ul>
	<!-- <div style="cursor:pointer" class="botao criarconta"> -->
		{{Form::submit('Criar Conta',array('class'=>'botao criarconta','style'=>'cursor:pointer'))}}
	<!-- </div> -->
{{Form::close()}}

	<!-- UF E CIDADE PAG CADASTRO -->
    <!-- <script language="JavaScript" type="text/javascript" src="js/cidades-estados.js"></script>-->
    <script type="text/javascript">
	    $(document).ready(function(){
		    new dgCidadesEstados({
		      cidade: document.getElementById('cidade'),
		      estado: document.getElementById('uf')
		    });	    
		});
    </script>
    <!-- FIM UF E CIDADE PAG CADASTRO -->



@endsection