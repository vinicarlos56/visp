@section('div')
  <div class="minhavitrine meusdados">
@endsection

@section('user_content') 

  <!-- CENTRO -->
  <!-- NOTE: Removi pois está duplicado com o layout geral,
        agora está no layout de user-->

  <!-- <div class="centralizando content minhavitrine meusdados"> -->

   <!--  <h2>MINHA VITRINE VIsP</h2> -->

    <!-- BARRA DE VITRINE -->    

    <!-- SIDEBAR MINHA VITRINE -->    

    <!-- RIGHT -->
    <div id="right">
	    <h1>MEUS DADOS</h1>

		{{Form::open('users/update', 'PUT')}}

		{{Form::token()}}

			@if(Session::has('errors'))
				<div class="error">
					<span>{{Session::get('errors')}}</span>
				</div>
			@elseif(Session::has('success'))
				<div class="success">
					<span>{{Session::get('success')}}</span>
				</div>
			@endif

			<!-- <input type="text" class="nome" id="nome" name="nome" placeholder="Gustavo Roma" /> -->
			{{Form::input('text', 'user_name', $user->name,array('class'=>'nome','id'=>'nome','placeholder'=>'Gustavo Roma'))}}

			<div class="envolveasinputs">
				<!-- <input type="text" class="datadenascimento" id="datadenascimento" name="datadenascimento" placeholder="28/02/1980" /> -->
				{{Form::input('text', 'user_birthday', User::show_date($user->birthday),array('class'=>'datadenascimento','id'=>'datadenascimento','placeholder'=>'28/02/1980'))}}

				<div class="mascaraselectsexo">Masculino</div>
				<!-- <select name="sexo" class="sexo" id="sexo">
					<option></option>
					<option value="Masculino" selected>Masculino</option>
					<option value="Feminino">Feminino</option>
				</select> -->
				{{Form::select('user_sex', array('m'=>'Masculino','f'=>'Feminino'), $user->sex,array('class'=>'sexo','id'=>'sexo'))}}

				@if(isset($user->cpf))
					<label class="cpf">CPF - {{$user->cpf}}</label>					
				@elseif(isset($user->cnpj))
					<label class="cpf">CNPJ - {{$user->cnpj}}</label>
				@endif
			</div>

			<div class="envolveasinputs">
				{{Form::input('text', 'user_phone', $user->phone,array('class'=>'telefone','id'=>'telefone','placeholder'=>'(21) 2580-0000'))}}
				<!-- <input type="text" name="telefone" id="telefone" class="telefone" placeholder="(21) 2580-0000"> -->
				{{Form::input('text', 'user_cell_phone', $user->cell_phone,array('class'=>'celular','id'=>'celular','placeholder'=>'(21) 8222-0000'))}}
				<!-- <input type="text" name="celular" id="celular" class="celular" placeholder="(21) 8222-0000"> -->
			</div>

			<div class="envolveasinputs">
				<!-- <input type="text" name="cep" id="cep" class="cep" placeholder="20771-400"> -->
				{{Form::input('text', 'address_cep', $address->cep,array('class'=>'cep','id'=>'cep','placeholder'=>'20771-400'))}}

				<!-- <input type="text" name="endereco" id="endereco" class="endereco" placeholder="Avenida Barata Ribeiro"> -->
				{{Form::input('text', 'address_logradouro', $address->logradouro,array('class'=>'endereco','id'=>'endereco','placeholder'=>'Avenida Barata Ribeiro'))}}
			</div>

			<div class="envolveasinputs">
				<!-- <input type="text" name="numero" id="numero" class="numero" placeholder="100" onkeypress="return SomenteNumero(event)"> -->
				{{Form::input('text', 'address_number', $address->number,array('id'=>'numero','class'=>'numero','placeholder'=>'100','onkeypress'=>'return SomenteNumero(event)'))}}

				<!-- <input type="text" name="complemento" id="complemento" class="complemento" placeholder="Apartamento 101"> -->
				{{Form::input('text', 'address_complement', $address->complement, array('class'=>'complemento','id'=>'complemento','placeholder'=>'Apartamento 101'))}}

				<!-- <input type="text" name="bairro" id="bairro" class="bairro" placeholder="Copacabana"> -->
				{{Form::input('text', 'address_neighborhood', $address->neighborhood,array('class'=>'bairro','id'=>'bairro','placeholder'=>'Copacabana'))}}
			</div>

			<div class="envolveasinputs">
				<div class="mascaraselectuf">RJ</div>
				<!-- <select name="uf" class="uf" id="uf"></select> -->
				{{Form::select('address_state', array(),null,array('class'=>'uf','id'=>'uf'))}}
				<div class="mascaraselectcidade">Rio de Janeiro</div>
				<!-- <select name="cidade" class="cidade" id="cidade"></select> -->
				{{Form::select('address_city',array() , $address->city, array('class'=>'cidade','id'=>'cidade'))}}
			</div>
		
			<div class="filhos">

				<h1>FILHOS (AS)</h1>

				<div class="mensagemtabela"></div>
				<table cellpadding="0" cellspacing="0" border="0">

					<tr>
						<td align="right" colspan="5"><input type="button" id="add" alt="Adicionar" title="Adicionar" /></td>
					</tr>					
					<!-- TODO: refatorar esta exibição -->
					<tbody id="repetir">
						<!-- TODO: Ajustar javascript ao duplicar campos -->
						<!-- TODO: Refatorar, Remover Duplicidade -->
						@foreach($user->childrens as $children)
							<tr id="">
								<td>{{Form::input('text', 'childrens[names][]',$children->name,array('id'=>'nome','class'=>'nome','placeholder'=>'nome'))}}</td>
								<td>{{Form::input('text', 'childrens[birthdays][]', User::show_date($children->birthday), array('class'=>'datadenascimento data','placeholder'=>'Data de Nascimento'));}}</td>
								<td>
									<div class="mascaraselectsexofilho">Sexo</div>
									{{Form::select('childrens[sex][]',array('m'=>'Masculino','f'=>'Feminino'),$children->sex,array('class'=>'sexofilho'))}}										
								</td>									
								<td><input type="button" id="remove" onclick="$.removeLinha(this);" alt="Remover" title="Remover" /></td>
							</tr>
							{{Form::hidden('childrens[id][]', $children->id)}}
						@endforeach

						<tr id="linha_1">
							<td>{{Form::input('text', 'childrens[names][]',null,array('id'=>'nome','class'=>'nome','placeholder'=>'nome'))}}</td>
							<td>{{Form::input('text', 'childrens[birthdays][]', null, array('class'=>'datadenascimento data','placeholder'=>'Data de Nascimento'));}}</td>
							<td>
								<div class="mascaraselectsexofilho">Sexo</div>
								{{Form::select('childrens[sex][]',array('m'=>'Masculino','f'=>'Feminino'),null,array('id'=>'sexofilho','class'=>'sexofilho'))}}										
							</td>									
							<td><input type="button" id="remove" onclick="$.removeLinha(this);" alt="Remover" title="Remover" /></td>
						</tr>
						
					</tbody>
					

				</table>
			</div>

			{{Form::hidden('user_id', $user->id)}}

			<!-- <input type="submit" name="salvar" id="salvar" class="botao salvar" /> -->
			{{Form::submit('Salvar',array('id'=>'salvar','class'=>'botao salvar'))}}  

		{{Form::close()}}

		{{Form::open(URL::to_route('new_password'), 'POST',array('id'=>'form-change-password'))}}
			{{Form::token()}}

			<div class="alterarsenha">
				<h1>Alterar Senha</h1>

				<input name="new_password" type="password" placeholder="Digite sua nova senha" id="novasenha" class="novasenha"/>
				<input name="new_password_confirmation" type="password" placeholder="Confirme sua nova senha" id="novasenha" class="novasenha"/>
				<input type="button" id="alterar" class="alterar" value="Alterar" />

				<div class="confirmarsenha">

					<div class="close">Fechar</div>

					<!-- Se a senha for incorreta, basta dar um display:block nessa div mensagem erro -->
					<div  class="mensagemdeerro">Senha incorreta!</div>
					<input name="actual_password" type="password" id="senhaatual" class="senhaatual" placeholder="Digite sua senha atual" />
					<input type="button" id="confirmar" class="confirmar" value="Confirmar" />

					<!-- Se a senha for correta, basta dar um display:block nessa mensagem e um display:none nas inputs -->
					<div class="sucesso">
						<div class="iconcorreto"></div>
						<div class="mensagemsucesso">Senha alterada com sucesso!</div>
					</div>
				</div>
			</div>
		{{Form::close()}}

		<div class="alteraravatar">
			{{Form::open_for_files('avatar', 'POST')}}
				{{Form::token()}}
				{{Form::label('avatar', 'Avatar:')}}
				{{Form::hidden('user_id',$user->id)}}
				{{Form::file('avatar')}}
				{{Form::submit('Enviar')}}
			{{Form::close()}}
		</div>

    </div><!-- FIM RIGHT -->

    <!-- UF E CIDADE PAG CADASTRO -->    
    {{HTML::script('assets/layout/js/cidades-estados.js')}}
    <script type="text/javascript">
      new dgCidadesEstados({
        cidade: document.getElementById('cidade'),
        estado: document.getElementById('uf')
      });
      //ADIDIONAR E REMOVER FILHOS
      $.removeLinha = function (element){
        var linha_total = $('tbody#repetir tr').length;
              
        if (linha_total > 1){
          $(element).parent().parent().remove();
        }else{
          $('.mensagemtabela').html("Desculpe, mas você não pode remover esta última linha!");
        }       
      };
      $(document).ready(function(){     
        var limite_linhas = 10;           
        $('#add').click(function(){
              
          var linha_total = $('tbody#repetir tr').length;
                  
          if (limite_linhas && limite_linhas > linha_total){
                    
            var linha = $('tbody#repetir tr').html();
                      
            var linha_total = $('tbody#repetir tr').length;
                      
            var linha_id = $('tbody#repetir tr').attr('id');
            
            $('tbody#repetir').append('<tr id="linha_' + (linha_total + 1) + '">' + linha + '</tr>');
      
          }else{
            $('.mensagemtabela').html("Desculpe, mas você só pode adicionar até " + limite_linhas + " linhas!");
          }       
        });
      });
    </script>
    <!-- FIM UF E CIDADE PAG CADASTRO -->
@endsection
