@section('div')
 <div class="centralizando content minhavitrine">
@endsection

@section('user_content')
	<!-- PRATELEIRA -->
    <div id="right">
      <h1>últimos produtos vistos</h1>
      @if(!empty($user->email_moip))
      <ul class="prateleira">         
        @foreach($products as $product)          
          <li>
            <span id="tooltip" class="tooltip">
              <div class="nome">Nome do produto</div>
              <div class="usadoounovo">Produto usado</div>
            </span>
            <a href="#">
              <div class="informacao">
                <div class="tamanho menino">
                  P
                </div>
                <div class="preco menino">
                  R$ 199,00
                </div>
              </div>
              {{HTML::image("uploads/thumbs/thumb_190x190_{$product->photos()->first()->file_name}")}}
            </a>
          </li>
        @endforeach
      </ul>

      <!-- <div class="exibirmais">
        <div class="mais">exibir mais produtos</div>
      </div> -->

    </div><!-- FIM MAIN --> 
      @else

      <!-- MAIN -->
      <div id="main">

        <h2>Sua loja ainda está desabilitada.</h2>
        <h3>Por favor, registre-se no MoIP ou cadastre sua conta na VIsP.</h3>
        <p>Sem uma conta MoIP você nao pode habilitar sua loja e seus produtos não estarão visíveis na VIsP. Para validar uma conta existente ou cadastrar uma nova,
         <a href="{{URL::to_route('moip_account')}}">Clique aqui.</a>
        </p>

      </div><!-- FIM MAIN -->   
      
      @endif
@endsection