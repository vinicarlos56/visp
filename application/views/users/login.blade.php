@layout('layout.main')

@section('sidebar')
@endsection

@section('content')

<ul class="teladelogin">
	<li class="first">
		<h1>
			já possui conta?<br />
			<span>acesse agora!</span>
		</h1>

		{{Form::open('login', 'POST')}}
			{{Form::token()}}			
			{{Form::input('text', 'login',null,array('placeholder'=>'Email'))}}<br>			
			{{Form::input('password', 'senha',null,array('placeholder'=>'Senha'))}}<br>
			{{Form::submit('Acessar',array('class'=>'acessar botao'))}}
		{{Form::close()}}

		<div class="esquecisenha">
			<a href="#">Ops... Esqueci minha senha</a>
		</div>
	</li>

	<li class="last">
		<h1>SUA PRIMEIRA VEZ?</h1>

		<div class="conectefacebook">
			<a id="facebook" href="">conecte com facebook</a>
		</div>
		<div id="fb-root"></div>
		<script src="http://connect.facebook.net/en_US/all.js"></script>
		<script>
			$(document).ready(function(){
				$('#facebook').on('click',function(e){
					e.preventDefault();
					$.ajax({
					  url:'/visp/public/test',
					  type:'GET',
					  success: function(url){ 
					  	var screenX    = typeof window.screenX != 'undefined' ? window.screenX : window.screenLeft,
		                screenY    = typeof window.screenY != 'undefined' ? window.screenY : window.screenTop,
		                outerWidth = typeof window.outerWidth != 'undefined' ? window.outerWidth : document.body.clientWidth,
		                outerHeight = typeof window.outerHeight != 'undefined' ? window.outerHeight : (document.body.clientHeight - 22),
		                width = 500,
		                height = 270,
		                left     = parseInt(screenX + ((outerWidth - width) / 2), 10),
		                top      = parseInt(screenY + ((outerHeight - height) / 2.5), 10);               
					    window.open(url,'','width=500,height=270,left='+left+',top='+top);
					  }
					});		
				});
			});
		</script>

		<p>*Temos um compromisso com você de  nunca publicar nada no seu perfil sem a sua autorização</p>

		<div class="ou"><span>ou</span></div>

		<h1><span>Crie sua conta</span></h1>

		<p class="crie">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse non magna id diam lobortis laoreet ac nec lorem.</p>

		<div class="criesuaconta botao">
			{{HTML::link_to_route('register','crie sua conta')}}
		</div>
	</li>
</ul>
@endsection