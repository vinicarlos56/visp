@section('div')
  <div class="centralizando content minhavitrine moip">
@endsection
  
@section('user_content') 
	<!-- RIGHT -->
    <div id="right">
      <h1>CONTA MOIP</h1>

      <h3>Forma de recebimento de suas vendas</h3>

      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur purus sapien, fringilla quis eleifend condimentum, laoreet et mauris. Nullam consectetur varius nisi, quis sollicitudin elit dignissim id. Nullam egestas dolor non ligula varius a bibendum mi fringilla. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus fermentum est vel nibh ornare ultricies. </p>
      {{Form::open('user/moip', 'PUT')}}
      {{Form::token()}}

      <div class="acessomoip">
        <div class="left">
          @if(Session::has('errors'))
            <h1>{{Session::get('errors')}}</h1>
          @endif
          <div class="icon"></div>
          <label>E-mail da conta</label>
          <!-- Deixar o email cadastrado marcado -->
          <!-- <input placeholder="email@gmail.com" type="text" class="contamoip" id="contamoip" name="contamoip" /> -->
          @if(!empty($user->email_moip))
            {{Form::text('contamoip', $user->email_moip, array('placeholder'=>'email@gmail.com','class'=>'contamoip','id'=>'contamoip'))}}
          @else  
            {{Form::text('contamoip', Input::old('contamoip'), array('placeholder'=>'email@gmail.com','class'=>'contamoip','id'=>'contamoip'))}}
          @endif
          <!-- Esse trocar só aparece depois de preenchido. -->
          <div class="trocar"><a href="#">Trocar</a></div>
        </div>
        @if(!empty($user->email_moip))
          <div class="right">
        @else
          <div style="display:none" class="right">
        @endif
            <div class="emailencontrado">
              <h4>Email encontrado no MoIP.</h4>
              <p>Sua conta está ativa.</p>
              <div class="ok"></div>
            </div>
          </div>
      </div>

      <!-- <input type="submit" name="salvar" id="salvar" class="botao salvar" /> -->
      {{Form::submit('Salvar', array('id'=>'salvar','class'=>'botao salvar'))}}
      {{Form::close()}}
    </div><!-- FIM RIGHT -->
@endsection