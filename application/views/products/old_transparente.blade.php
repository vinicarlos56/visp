@layout('layout.main')

@section('sidebar')
@endsection

@section('content')

	{{HTML::script('assets/layout/js/pagamento.js')}}
	

	<style>
		#transparente input[type="text"] {
			/*float:left;*/
			background-color: #F7F7F7;
			border: 1px solid #DADADA;
			color: darkGray;
			height: 25px;
			padding-left: 10px;
			margin:5px;
		}
		#transparente select{
			height: 27px;
		}

		.hide{
			display: none;
		}
		.show{
			display: block;
		}

	</style>

	<div id="transparente">

		<div style="margin-bottom:20px" id="productresume">
			<label><b>Nome do produto:</b></label><p>{{isset($products[0])?$products[0]->title:''}}</p>			
			<label><b>Preço:</b></label><p>{{isset($products[0])?$products[0]->price:''}}</p>
		</div>

		<h3 style="margin-bottom:20px">Escolha a forma de pagamento:</h3>

		<div style="margin-bottom:20px" id="paymentoptions">
			<input type="radio" name="paymentway" value="billetoption">
			<label>Boleto</label>

			<input type="radio" name="paymentway" value="banktransferoption">
			<label>À Vista</label>

			<input type="radio" name="paymentway" value="creditcardoption">
			<label>Cartão de Crédito</label>
		</div>

		<div id="creditcardoption" class="optiondiv">
			<label for="parcelsnumber">Número de parcelas</label>
			<select name="parcelsnumber" id="parcelsnumber">
				<option value="">Selecione</option>
				@if(isset($sale->total_price))
					@for($i = 1; $i <= 6; $i++)
						<option value="{{$i}}"> {{$i}}X de R$ {{number_format(($sale->total_price/$i),2, ',', '.')}}</option>
					@endfor
				@endif
			</select><br/><br/>

			<label for="creditcardinstitution">Tipo de Cartão de crédito</label>
			<select name="creditcardinstitution" id="creditcardinstitution">
				<option value="">Selecione</option>
				<option value="AmericanExpress">American Express</option>
				<option value="Diners">Dinners</option>
				<option value="Mastercard">Mastercard</option>
				<option value="Hipercard">Hipercard</option>
				<option value="Visa">Visa</option>
			</select><br/><br/>

			<label for="creditcardnumber">Número do cartão de crédito</label>
			<input type="text" name="creditcardnumber" id="creditcardnumber"><br/><br/>

			<label for="">Data de Vencimento</label>
			<select name="month" id="month">
				<option value="">Mês</option>
				<option value="01"> 01 - Janeiro</option>
				<option value="02"> 02 - Fevereiro</option>
				<option value="03"> 03 - Março</option>
				<option value="04"> 04 - Abril</option>
				<option value="05"> 05 - Maio</option>
				<option value="06"> 06 - Junho</option>
				<option value="07"> 07 - Julho</option>
				<option value="08"> 08 - Agosto</option>
				<option value="09"> 09 - Setembro</option>
				<option value="10"> 10 - Outubro</option>
				<option value="11"> 11 - Novembro</option>
				<option value="12"> 12 - Dezembro</option>
			</select>

			<select name="year" id="year">
				<option value="">Ano</option>
				<option value="13">2013</option>
				<option value="14">2014</option>
				<option value="15">2015</option>
				<option value="16">2016</option>
				<option value="17">2017</option>
				<option value="18">2018</option>
				<option value="19">2019</option>
				<option value="20">2020</option>
				<option value="21">2021</option>
				<option value="22">2022</option>
				<option value="23">2023</option>
			</select><br/><br/>

			<label for="securitynumber">Número de Segurança</label>
			<input type="text" name="securitynumber" id="securitynumber"><br/><br/>

			<h3 style="margin-bottom:20px">Informações do portador</h3>

			<label for="name">Nome</label>
			<input type="text" name="name" id="name"><br/><br/>

			<label for="birthday">Data de aniversário</label>
			<input type="text" name="birthday" id="birthday"><br/><br/>

			<label for="phone">Telefone</label>
			<input type="text" name="phone" id="phone"><br/><br/>

			<label for="rg">Identidade</label>
			<input type="text" name="rg" id="rg"><br/><br/>
		</div>

		<div id="billetoption" class="optiondiv">
			<p>Imprima o boleto bancário após a finalização do pedido</p>
		</div>

		<div id="banktransferoption" class="optiondiv">
			<input type="radio" name="banktransfer" value="BancoDoBrasil">
			<label>Banco do Brasil</label><br/>

			<input type="radio" name="banktransfer" value="Itau">
			<label>Banco Itaú</label><br/>

			<input type="radio" name="banktransfer" value="Bradesco">
			<label>Banco Bradesco</label><br/>
		</div>
		<input type="hidden" value="{{$sale->id}}" id="sale_id">
		<input type="button" name="pay" id="pay" value="Pagar">
		<div id="MoipWidget"
			data-token="<?php echo $sale->token?>"
			callback-method-success="success"
			callback-method-error="failReturn">
		</div>
		
		<!-- <button id="pagarBoleto" onclick="pagarBoleto()"> Imprimir Boleto </button> -->
	    
	    
	    <!-- 	src='https://desenvolvedor.moip.com.br/sandbox/transparente/MoipWidget-v2.js'  -->
	    
	    {{HTML::script('assets/lib/js/MoipWidget-v2.js',array('charset'=>'ISO-8859-1'))}}
	    

	</div>   
	
@endsection