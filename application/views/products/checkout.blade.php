@layout('layout.main')

@section('sidebar')
@endsection

@section('content')


  <!-- CENTRO -->
  <div class="centralizando content checkout">

    <h2>MEU CARRINHO</h2>

    <!-- BARRA DE COMPRAS -->
    <div class="barradecompras">

      <div class="primeiro passoapasso"><a href="#">1- MINHAS COMPRAS</a></div>

      <div class="segundo passoapasso"><a href="#">2- MEUS DADOS E PAGAMENTO</a></div>

      <div class="terceiro passoapasso"><a href="#">3- PEDIDO FINALIZADO</a></div>

    </div><!-- FIM BARRA DE COMPRAS -->

    @if(!is_null($cart->get_cart()))
		{{Form::open('moip', 'POST')}}
			{{Form::token()}}
			<!-- COMPRAS DE UM VENDEDOR -->
			
			@foreach($cart->get_sellers() as $user_id)
				<table width="100%" border="0" cellspacing="0" cellpadding="0">  
					<tr class="pedidosvendedor">
						<td colspan="5">Pedidos do vendedor: <a href="#"><strong>{{User::find($user_id)->name}}</strong></a></td>
					</tr>
					<tr class="barracolunas">
						<td class="produto" width="355px;">Produtos</td>
						<td class="precoindividual" width="145px;">Preço individual</td>
						<td class="quantidade" width="145px;">Quantidade</td>
						<td class="frete" width="170px;">Frete</td>
						<td class="subtotal" width="146px;">Sub-total</td>
					</tr>

					@foreach(Product::load_by_user($cart,$user_id) as $product)
						<!-- PRODUTO -->
						<tr class="informacoes">
							<td class="fotoetitulo">
								<a href="#">            
									<img src="{{URL::to("uploads/thumbs/thumb_90x90_{$product->photos()->first()->file_name}")}}">
									<h3>{{$product->title}}</h3>
								</a>
							</td>
							<td>{{Currency::show_price($product->price)}}</td>
							<td class="quantidade">
								<!-- TODO: Trazer a quantidade do produto -->
								<div class="mascaradequantidade">1</div> 
								{{-- Form::select('products_amount['.$product->id.'][]',array('1'=>'01','2'=>'02','3'=>'03','4'=>'04','5'=>'05','6'=>'06','7'=>'07','8'=>'08','9'=>'09','10'=>'10'),null,array('id'=>'quantidade')) --}}
								
								<div class="excluir shoppingcart-remove" data-product-id="{{$product->id}}">excluir</div>
							</td>
							<td class="correios">PAC
								<!--  <div class="mascaratipodefrete">correios normal</div>
								<select id="tipodefrete">
								<option>correios normal</option>
								<option>lorem ipsum 1</option>
								<option>lorem ipsum 2</option>
								<option>lorem ipsum 3</option>
								</select>
								<p>Lorem ipsum at<br />a dolor ipsum color</p> -->
								<input type="hidden" class="product-cep" value="{{$product->user->address->cep}}">					
							</td>
							<td>{{Currency::show_price($product->price)}}</td>
						</tr><!-- FIM PRODUTO -->
					@endforeach     
				</table><!-- FIM COMPRAS DE UM VENDEDOR -->
			@endforeach

			<!-- SOMA TOTAL -->
			<div id="subtotal-geral">
				Subtotal: <strong>{{Currency::show_price(Sales::get_total_selling_price($cart))}}</strong>
			</div><!-- FIM SOMA TOTAL -->

			<!-- FINAL -->
			<div id="final">

				<div class="left">
					<div class="cep">
						<!-- <form> -->
							<label>CEP</label>
							<input class="input" name="cep" type="text" id="cep" value=""/> 
							<!-- <input type="button" id="consultarcorreios" value="Consultar Frete">             -->
						<!-- </form> -->
						<a href="http://www.correios.com.br/servicos/cep/cep_loc_log.cfm" target="_blank"><strong>Não sabe o cep?</strong></a>
						<p>lorem ipsum at dolor</p>
					</div>
				</div>

				<div class="right">
					@foreach($cart->get_sellers() as $user_id)
						<table width="430" border="0" cellspacing="0" cellpadding="0">
							<tr class="cabecalho">
								<td width="300" class="freteporvendedor">Frete por vendedor: {{User::find($user_id)->name}}</td>
								<td width="130" class="subtotal">Subtotal: {{Currency::show_price(Sales::get_price_by_seller($user_id))}}</td>
							</tr>
							<tr>
								<td class="linha" colspan="2"></td>
							</tr>
							@foreach(Product::load_by_user($cart,$user_id) as $product)
								<tr class="produto">
									<td width="300">{{$product->title}}</td>
									<td width="130">{{Currency::show_price($product->price)}}</td>
								</tr>
							@endforeach
						</table>
					@endforeach
				</div>

				<div class="moip-e-total">
					<div class="left">
						<div class="formadepagamento">
							<h2>Forma de pagamento</h2>
							<div class="iconmoip">Moip</div>
						</div>
						<div class="siteprotegido">
							<h2>Site protegido por</h2>
							<div class="icons">aqui entra os icones</div>
						</div>
					</div>

					<div class="right">
						<table width="430" border="0" cellspacing="0" cellpadding="0">
							<tr class="produto">
								<td width="300">Total do Carrinho</td>
								<td width="130">{{Currency::show_price(Sales::get_total_selling_price($cart))}}</td>
							</tr>
							<tr class="produto">
								<td colspan="2">em até 12x sem juros parcelados pelo moip</td>
							</tr>
						</table>
					</div>
				</div>
			</div><!-- FIM FINAL -->

			<div class="botao continuarcomprando">
				<a href="{{URL::to_route('home')}}">Continuar Comprando</a>
			</div>

			
			{{Form::submit('Finalizar',array('class'=>'botao finalizar'))}}
			
		{{Form::close()}}
    @else
      <h1 style="margin-top:20px">Você não possui produtos no carrinho</h1>
    @endif

@endsection