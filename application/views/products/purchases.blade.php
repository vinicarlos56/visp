@section('div')
  <div class="centralizando content minhavitrine minhascompras">

@endsection

@section('user_content')

    <div id="right">
      <h1>MINHAS COMPRAS</h1>

      <ul class="produtos">
	  @foreach($products as $product)
        <!-- PRODUTO -->
        <li>

          <div class="foto">
            <img src="{{URL::to("uploads/thumbs/thumb_90x90_{$product->photos()->first()->file_name}")}}" alt="" title="" />
          </div>

          <!-- FIM MEIO -->
          <div class="meio">
            <h2 class="tituloproduto">{{$product->title}}</h2>
            <div class="ref">ref. 34562</div>

            <!-- PREÇO -->
            <div id="preco">

              <div class="de">de: <span>R$ {{$product->original_price}}</span></div>
              <div class="atual">Por: <span>R$ {{$product->price}}</span></div>

            </div><!-- FIM PREÇO -->

            <!-- AVALIE VENDEDOR -->
            <div id="avalievendedor">

              <span>avalie o vendedor</span>

              <ol class="estrelinhas"><li></li><li></li><li></li><li></li><li></li></ol>

            </div><!-- FIM AVALIE VENDEDOR -->

            <!-- RECEBEU O PRODUTO -->
            <div id="recebeuoproduto">

              <span>recebeu o produto</span>

              <div class="sim"><a href="#" alt="Sim" title="Sim">Sim</a></div>

              <div class="nao"><a href="#" alt="Não" title="Não">Não</a></div>

            </div><!-- FIM RECEBEU O PRODUTO -->

          </div><!-- FIM MEIO -->

          <!-- DIREITA -->
          <div style="width:140px" class="direita">

              <a href="{{$sales[$product->id]->url}}">URL de pagamento</a><br>
              Forma de pagamento: {{$sales[$product->id]->payment_way}}
          </div><!-- FIM DIREITA -->

        </li><!-- FIM PRODUTO -->

        <!-- PRODUTO -->
    @endforeach
        
      </ul>

      
    </div><!-- FIM RIGHT -->


@endsection