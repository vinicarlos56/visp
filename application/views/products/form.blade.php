@section('sidebar')
@endsection

@section('content')

{{HTML::style('assets/lib/css/bootstrap.min.css')}}

<!-- Bootstrap CSS fixes for IE6 -->
<!--[if lt IE 7]><link rel="stylesheet" href="css/bootstrap-ie6.min.css"><![endif]-->

{{HTML::style('assets/lib/css/jquery.fileupload-ui.css')}}

<noscript>
	{{HTML::style('assets/lib/css/jquery.fileupload-ui-noscript.css')}}
</noscript>
<!--[if lt IE 9]><script src="css/html5.js"></script><![endif]-->

  <!-- CENTRO -->
  <div class="centralizando content dadosdoproduto">

  	<!-- TODO: encontrar uma maneira de realizar o upload -->
    <!-- <form id="fileupload" action="server/php/files/" method="POST" enctype="multipart/form-data"> -->
    {{-- TODO: refatorar abertura do form --}} 
    @if(URI::segment(2) == 'novo') 
    	{{Form::open('products','POST',array('id'=>'fileupload'))}} {{-- TODO: posteriormente adicionar open for files--}}
    @else
    	{{Form::open('products/update','PUT',array('id'=>'fileupload'))}}
    @endif

    	{{Form::token()}}

    	@if (Session::has('errors'))
			<div class="error">
	    		<span>{{Session::get('errors')}}</span>
			</div>
		@endif

		<!-- PRIMEIRO NÍVEL -->
		<div class="primeironivel">

			<h3>DADOS DO PRODUTO</h3>

			<div class="tituloedescricao">
				<!-- <input type="text" placeholder="Título do produto" name="titulodoproduto" class="titulodoproduto" id="titulodoproduto" /> -->
				<!-- FIXME: Refatorar title -->
				@if(isset($product->title))
					{{Form::input('text','product_title',$product->title,array('placeholder'=>'Título do produto *','class'=>'titulodoproduto input','id'=>'titulodoproduto'))}}
				@else
					{{Form::input('text','product_title',Input::old('product_title'),array('placeholder'=>'Título do produto *','class'=>'titulodoproduto input','id'=>'titulodoproduto'))}}
				@endif

				<!-- <textarea placeholder="Descrição do produto (conte uma história)" name="descricaodoproduto"></textarea> -->
				<!-- FIXME: Refatorar title -->
				@if(isset($product->description))
					{{Form::textarea('product_description', $product->description, array('placeholder'=>'Descrição do produto (conte uma história) *'))}}
				@else
					{{Form::textarea('product_description', Input::old('product_description'), array('placeholder'=>'Descrição do produto (conte uma história) *'))}}
				@endif
			</div>

			<div class="categoriatamanhocormarca">
				
				@if(isset($product->category_id))
					<div class="mascaracategoria">{{$product->category()->get_name()}}</div>
					{{Form::select('product_category_id', Type::select_all_categories('') ,$product->category_id,array('id'=>'categoria'))}}			
				@else
					<div class="mascaracategoria">Categoria *</div>
					{{Form::select('product_category_id', Type::select_all_categories('') ,Input::old('product_category_id'),array('id'=>'categoria'))}}			
				@endif
				
				
				<!-- <input type="text" placeholder="Tamanho" name="tamanho" id="tamanho" maxlength="3" /> -->
				@if(isset($product->size))
					{{Form::input('text','product_size',$product->size,array('id'=>'tamanho','maxlength'=>'3','placeholder'=>'Tamanho *','class'=>'input'))}}
				@else
					{{Form::input('text','product_size',Input::old('product_size'),array('id'=>'tamanho','maxlength'=>'3','placeholder'=>'Tamanho *','class'=>'input'))}}
				@endif

				<span id="tooltip" class="tooltip">
					<div class="nome">Tamanho do Produto</div>
					<div class="usadoounovo">lorem ipsum lorem ipsum</div>
				</span>
				
				<!-- <input type="text" placeholder="Cor" name="cor" id="cor" /> -->
				@if(isset($product->color))
					{{Form::input('text','product_color',$product->color,array('id'=>'cor','placeholder'=>'Cor *','class'=>'input'))}}
				@else
					{{Form::input('text','product_color',Input::old('product_color'),array('id'=>'cor','placeholder'=>'Cor *','class'=>'input'))}}
				@endif
				
				<!-- <input type="text" placeholder="Marca" name="marca" id="marca" /> -->
				@if(isset($product->mark))
					{{Form::input('text','product_mark',$product->mark,array('id'=>'marca','placeholder'=>'Marca *','class'=>'input'))}}
				@else
					{{Form::input('text','product_mark',Input::old('product_mark'),array('id'=>'marca','placeholder'=>'Marca *','class'=>'input'))}}
				@endif

			</div>

			<!-- TODO: Criar select de subcategoria -->
			<div class="subcategoriacondicaoorigem">

				@if(isset($product->category_id))
					<div class="mascarasubcategoria">{{$product->subcategory()->get_name()}}</div>
					{{Form::select('product_subcategory_id', Type::select_all_subcategories('') ,$product->subcategory_id,array('id'=>'subcategoria'))}}
				@else
					<div class="mascarasubcategoria">Sub-Categoria *</div>
					{{Form::select('product_subcategory_id', Type::select_all_subcategories('') ,Input::old('product_subcategory_id'),array('id'=>'subcategoria'))}}
				@endif

				<div class="condicao">
					<h4>Condição *</h4>
					<?php 
						// TODO:refatorar isso! :s (radiobutton condition)
						$nuncausado = false;
						$usado 		= false;
						$condition  = isset($product->condition) ? $product->condition : Input::old('product_condition');
						if($condition == 'nuncausado')
							$nuncausado = true;
						else if ($condition == 'usado')
							$usado = true;							
					 ?>
					<!-- <input type="radio" name="condicao" value="Nunca usado" id="nuncausado" class="nuncausado" /> -->
					{{Form::radio('product_condition','nuncausado',$nuncausado,array('id'=>'nuncausado','class'=>'nuncausado'))}}
					<label>Nunca usado</label>

					<!-- <input type="radio" name="condicao" value="Usado" id="usado" class="usado" /> -->
					{{Form::radio('product_condition','usado',$usado,array('id'=>'usado','class'=>'usado'))}}
					<label>Usado</label>
				</div>

				<div class="origem">
					<h4>Origem *</h4>
					<?php 
						// TODO:refatorar isso! :s (radiobutton origin)
						$nacional = false;
						$internacional = false;
						$origin  = isset($product->origin) ? $product->origin : Input::old('product_origin');
						if($origin == 'nacional')
							$nacional = true;
						else if ($origin == 'internacional')
							$internacional = true;
							
					 ?>
					<!-- <input type="radio" name="origem" value="Nacional" id="nacional" class="nacional" /> -->
					{{Form::radio('product_origin','nacional',$nacional,array('id'=>'nacional','class'=>'nacional'))}}
					<label>Nacional</label>

					<!-- <input type="radio" name="origem" value="Internacional" id="internacional" class="internacional" /> -->
					{{Form::radio('product_origin','internacional',$internacional,array('id'=>'internacional','class'=>'internacional'))}}
					<label>Importado</label>
				</div>
			</div>

		</div><!-- FIM PRIMEIRO NIVEL -->

		<!-- SEGUNDO NÍVEL -->
		<div class="segundonivel">

			<h3>PESO E FRETE *</h3>

			<?php 
				// TODO:refatorar isso! :s (radiobutton weight/frete)
				$weight  = isset($product->weight) ? $product->weight : Input::old('product_weight');
				$combine = isset($product->combine) ? $product->combine : Input::old('combinar');


				$free = false;
				$pena = false;
				$leve = false;
				$medio = false;
				$pesado = false;
				$combinar = false;

				switch ($weight) {
					case 'free':
						$free = true;
						break;

					case 'pena':
						$pena = true;
						break;

					case 'leve':
						$leve = true;
						break;

					case 'medio':
						$medio = true;
						break;

					case 'pesado':
						$pesado = true;
						break;					
				}

				if($combine == 'combinar')
					$combinar = true;		
					
			 ?>

			<ul class="tabeladefrete">
				<li>
					<div class="mascarainput <?=($free)?'ativo':''?>">Frete Grátis</div>
					<!-- <input type="radio" name="pesoefrete" value="Frete grátis - O frete será por sua conta" /> -->
					{{Form::radio('product_weight','free',$free)}}
					<label>O frete será por sua conta</label>
				</li>

				<li>
					<div class="mascarainput <?=($pena)?'ativo':''?>">Peso Pena</div>
					<!-- <input type="radio" name="pesoefrete" value="Peso pena - 0,3 a 2kg" /> -->
					{{Form::radio('product_weight','pena',$pena)}}
					<label>0,3 a 2kg</label>
				</li>

				<li>
					<div class="mascarainput <?=($leve)?'ativo':''?>">Peso Leve</div>
					<!-- <input type="radio" name="pesoefrete" value="Peso leve - 2 a 5kg" /> -->
					{{Form::radio('product_weight','leve',$leve)}}
					<label>2 a 5kg</label>
				</li>

				<li>
					<div class="mascarainput <?=($medio)?'ativo':''?>">Peso Médio</div>
					<!-- <input type="radio" name="pesoefrete" value="Peso médio - 5 a 12kg" /> -->
					{{Form::radio('product_weight','medio',$medio)}}
					<label>5 a 12kg</label>
				</li>

				<li>
					<div class="mascarainput <?=($pesado)?'ativo':''?>">Peso Pesado</div>
					<!-- <input type="radio" name="pesoefrete" value="Peso pesado - acima de 12kg" /> -->
					{{Form::radio('product_weight','pesado',$pesado)}}
					<label>acima de 12kg</label>
				</li>

				<li id="combinar">
					<div class="mascarainput <?=($combinar)?'ativo':''?>">A Combinar</div>
					<!-- <input type="radio" name="pesoefrete" value="A Combinar - Aceito marcar retirada" /> -->
					{{Form::checkbox('combinar','combinar',$combinar)}}
					<!-- TODO: alterar para checkbox para registrar corretamente -->
					<label>Aceito marcar retirada</label>
				</li>
			</ul>
		</div><!-- FIM SEGUNDO NÍVEL -->


	@if(URI::segment(2) != 'novo') 
		<!-- TERCEIRO NÍVEL -->
      <div class="terceironivel">

        <h3>INSERIR FOTOS</h3>

        <!-- UPLOAD -->
        <div class="row fileupload-buttonbar">
            <div class="span7">

                <span class="btn-success fileinput-button">
                    <input type="file" name="files[]" multiple>
                </span>
                <button type="submit" class="btn btn-primary start">
                    <i class="icon-upload icon-white"></i>
                    <span>Start upload</span>
                </button><br />
                <button type="reset" class="btn btn-warning cancel">
                    <i class="icon-ban-circle icon-white"></i>
                    <span>Cancelar upload</span>
                </button><br />
                <button type="button" class="btn btn-danger delete">
                    <i class="icon-trash icon-white"></i>
                    <span>Deletar</span>
                </button>
            </div>

            <div class="span5 fileupload-progress fade">

                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="bar" style="width:0%;"></div>
                </div>

                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>

        <div class="fileupload-loading"></div>

        <table role="presentation" class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>

        <div id="modal-gallery" class="modal modal-gallery hide fade" data-filter=":odd" tabindex="-1"></div>

        <script id="template-upload" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
            <tr class="template-upload fade">
                <td class="preview"><span class="fade"></span></td>
                <td class="name"><span>{%=file.name%}</span></td>
                <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
                {% if (file.error) { %}
                    <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
                {% } else if (o.files.valid && !i) { %}
                    <td>
                        <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
                    </td>
                    <td class="start">{% if (!o.options.autoUpload) { %}
                        <button class="btn btn-primary">
                            <i class="icon-upload icon-white"></i>
                            <span>Enviar</span>
                        </button>
                    {% } %}</td>
                {% } else { %}
                    <td colspan="2"></td>
                {% } %}
                <td class="cancel">{% if (!i) { %}
                    <button class="btn btn-warning">
                        <i class="icon-ban-circle icon-white"></i>
                        <span>Cancelar</span>
                    </button>
                {% } %}</td>
            </tr>
        {% } %}
        </script>

        <script id="template-download" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
            <tr class="template-download fade">
                {% if (file.error) { %}
                    <td></td>
                    <td class="name"><span>{%=file.name%}</span></td>
                    <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
                    <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
                {% } else { %}
                    <td class="preview">{% if (file.thumbnail_url) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
                    {% } %}</td>
                    <td class="name">
                        <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
                    </td>
                    <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
                    <td colspan="2"></td>
                {% } %}
                <td class="delete">
                    <button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                        <i class="icon-trash icon-white"></i>
                        <span>Deletar</span>
                    </button>
                    <input type="checkbox" name="delete" value="1">
                </td>
            </tr>
        {% } %}
        </script><!-- FIM UPLOAD -->

      </div><!-- FIM TERCEIRO NÍVEL -->
    @endif  


		<!-- QUARTO NÍVEL -->
		<div class="quartonivel">
			<h3>PREÇO *</h3>

			<!-- TODO: Ajustar mascara para fazer o registro correto no banco -->
			<div class="precoorigial">
				<label>Seu Preço</label>
				<!-- <input type="text" name="precooriginal" id="precooriginal" class="precooriginal" placeholder="R$ 0,00" /> -->
				@if(isset($product->original_price))
					{{Form::input('text','product_original_price', Currency::show_price($product->original_price),array('placeholder'=>'R$ 0,00','id'=>'precooriginal','class'=>'precooriginal input'))}}
				@else
					{{Form::input('text','product_original_price',Currency::show_price(Input::old('product_original_price')),array('placeholder'=>'R$ 0,00','id'=>'precooriginal','class'=>'precooriginal input'))}}
				@endif

			</div>

			<!-- TODO: Ajustar mascara para fazer o registro correto no banco -->
			<div class="seupreco">
				<label>Preço no Site</label>
					<span id="price-in-site" style="border: 1px solid;width: 130px;padding: 4px;float: left;margin-left: 20px; height:15px">{{isset($product->price) ? $product->price : '' }}</span>
				<!-- <input type="text" name="seupreco" id="seupreco" class="seupreco" placeholder="R$ 0,00" /> -->
				<?php /* ?>@if(isset($product->price))
					{{Form::input('text','product_price',$product->price,array('placeholder'=>'R$ 0,00','id'=>'seupreco','class'=>'seupreco'))}}
				@else
					{{Form::input('text','product_price',Input::old('product_price'),array('placeholder'=>'R$ 0,00','id'=>'seupreco','class'=>'seupreco'))}}
				@endif */ ?>	
			</div>

			<div class="inputdequantidade">

				@if(isset($product->amount))
					<div class="mascaraquantidade">{{$product->amount}}</div>
					{{Form::select('product_amount',array('1'=>'01','2'=>'02','3'=>'03','4'=>'04','5'=>'05','6'=>'06','7'=>'07','8'=>'08','9'=>'09','10'=>'10'),$product->amount,array('id'=>'quantidade'))}}
				@else
					<div class="mascaraquantidade">Quantidade</div>
					{{Form::select('product_amount',array('1'=>'01','2'=>'02','3'=>'03','4'=>'04','5'=>'05','6'=>'06','7'=>'07','8'=>'08','9'=>'09','10'=>'10'),Input::old('product_amount'),array('id'=>'quantidade'))}}
				@endif
				<!-- <select id="quantidade">
					<option>01</option>
					<option>02</option>
					<option>03</option>
					<option>04</option>
					<option>05</option>
					<option>06</option>
					<option>07</option>
					<option>08</option>
					<option>09</option>
					<option>10</option>
				</select> -->
			</div>
		</div><!-- FIM QUARTO NÍVEL -->

		<!-- QUINTO NÍVEL -->
		<div class="quintonivel" style="text-align: center;">
			<p>Ao clicar em “Enviar” você está concordando com os termos de serviço</p>
			<!-- <div class="botao enviarproduto"> -->
				<!-- <a href="#">Enviar</a> -->
			<!-- TODO: Ajustar o layout do botão de enviar -->
			{{Form::submit('Enviar',array('class'=>'botao enviarproduto'))}}
			<!-- </div> -->

		</div><!-- FIM QUINTO NÍVEL -->

		@if(isset($product->id))
			{{Form::hidden('product_id', $product->id)}}
		@endif

		{{Form::hidden('product_user_id', Auth::user()->id)}}

	{{Form::close()}}

	@if(URI::segment(2) != 'novo') 
		<?php /*<!-- TERCEIRO NÍVEL -->
		<div class="terceironivel">
			
			<h3>INSERIR FOTOS</h3>

			<!-- UPLOAD -->
			{{Form::open_for_files('products/photos','POST')}}
				{{Form::token()}}
				<div class="row fileupload-buttonbar">
					<div class="span7">
						<span class="btn-success fileinput-button">
							{{Form::file('image')}}
						</span>
					</div>				
				</div>
				{{Form::hidden('product_id', $product->id)}}
				{{Form::submit('Enviar')}}
			{{Form::close()}}
			<!-- FIM UPLOAD -->	
					
			<div style="margin-top:150px">
				@if(isset($product->photos))
					@foreach($product->photos as $photo)
						<img src="{{URL::to("uploads/thumbs/thumb_190x190_{$photo->file_name}")}}">
					@endforeach
				@endif
			</div>

		</div><!-- FIM TERCEIRO NÍVEL --> */ ?>
	@endif
		
@if(URI::segment(2) != 'novo') 
{{HTML::script('assets/layout/js/upload-de-fotos/vendor/jquery.ui.widget.js')}}
{{HTML::script('assets/layout/js/upload-de-fotos/tmpl.min.js')}}
{{HTML::script('assets/layout/js/upload-de-fotos/load-image.min.js')}}
{{HTML::script('assets/layout/js/upload-de-fotos/canvas-to-blob.min.js')}}
{{HTML::script('assets/layout/js/upload-de-fotos/bootstrap.min.js')}}
{{HTML::script('assets/layout/js/upload-de-fotos/jquery.iframe-transport.js')}}
{{HTML::script('assets/layout/js/upload-de-fotos/jquery.fileupload.js')}}
{{HTML::script('assets/layout/js/upload-de-fotos/jquery.fileupload-fp.js')}}
{{HTML::script('assets/layout/js/upload-de-fotos/jquery.fileupload-ui.js')}}
{{HTML::script('assets/layout/js/upload-de-fotos/main.js')}}
<!--[if gte IE 8]><script src="js/upload-de-fotos/cors/jquery.xdr-transport.js"></script><![endif]-->
@endif


@endsection