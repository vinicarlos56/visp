@layout('layout.main')

@section('content')

 <!-- BREADCUMB ESTAVA AQUI -->

  <!-- <link rel="stylesheet" href="css/global.css" /> -->

  <!-- CENTRO -->
  <div class="centralizando content produto {{$product->subcategory()->get_css_class()}}">

    

    <!-- RIGHT -->
    <div id="right">

      <!-- VENDEDOR -->
      <div class="vendedor">

        <a href="#">
          <div class="avatar">
            @if(!empty($product->user->avatar))
              {{HTML::image(URL::to('uploads/thumbs/thumb_56x56_'.$product->user->avatar))}}
            @endif
          </div>

          <div class="dadosvendedor">
            <div class="titulovendedor">vendedor</div>
            <h2 class="{{$product->subcategory()->get_css_class()}}">{{$product->user()->first()->name}}</h2>
            <div class="urlvendedor">www.vispstore.com/{{$product->user()->first()->nickname}}</div>
          </div>
        </a>

      </div><!-- FIM VENDEDOR -->

      <!-- PRIMEIRO NÍVEL -->
      <div class="primeiro-nivel">    

        <!-- PRODUTO IMAGEM -->
        <div id="produtoimagem">

          @if($product->condition == 'nuncausado')
            <div class="flag-nunca-usado">Nunca usado</div>
          @endif

          <div id="imagemapresentacao" class="slides_container">
            @foreach($product->photos as $photo)
              <span><img src="{{URL::to("uploads/thumbs/thumb_312x360_{$photo->file_name}")}}"></span>
            @endforeach           
          </div>

          <ul class="thumbproduto pagination">
            @foreach($product->photos as $photo)
              <li>
                <a href="images/imagem-produto-apresentacao-1">
                  <img src="{{URL::to("uploads/thumbs/thumb_90x90_{$photo->file_name}")}}">
                </a>
              </li>
            @endforeach                     
          </ul>

          <!-- REDES SOCIAIS -->
          <div id="redessociais">

            <h3>compartilhe</h3>

            <!-- AddThis Button BEGIN -->
           <!--  <div class="addthis_toolbox addthis_default_style ">
              <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
              <a class="addthis_button_tweet"></a>
              <a class="addthis_button_pinterest_pinit"></a>
              <a class="addthis_counter addthis_pill_style"></a>
            </div> -->
            <!-- // <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script> -->
            <!-- // <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50e72de94b4351f6"></script> -->
            <!-- AddThis Button END -->

          </div><!-- FIM REDES SOCIAIS -->

        </div><!-- FIM PRODUTO IMAGEM -->

        <!-- INFORMAÇÕES PRODUTOS -->
        <div id="informacoesproduto">         
          <!-- ADOREI -->
          <div data-product-id="{{$product->id}}" data-user-id="{{$product->user->id}}" id="adorei">
            <span>adorei</span>
            <div class="botao like">
              <div class="icon"></div>
            </div>
            <span>123 likes</span>
          </div><!-- FIM ADOREI -->         

          <h2 class="tituloproduto">{{$product->title}}</h2>
          <div class="ref">ref. {{$product->id}}</div>

          <!-- PREÇO -->
          <div id="preco">

            <!-- <div class="de">de: <span>R$ {{$product->original_price}}</span></div> -->
            <div class="atual">Por: <span>R$ {{$product->price}}</span></div>
            <div class="condicao">ou em 12x de <span>R$ {{ceil($product->price/12)}}</div>

          </div><!-- FIM PREÇO -->

          <!-- FLAG DE FRETE E BOTAO COMPRAR -->
          <div id="flagfrete-e-botaocomprar">

          @if($product->weight == 'free')
            <div class="flagfrete">Frete Grátis</div>
          @endif                   

          <div class="botao comprar" data-product-id="{{$product->id}}"><a href="#">Comprar</a></div>


          </div><!-- FIM FLAG DE FRETE E BOTAO COMPRAR -->

          <!-- DESCRICAO DO PRODUTO -->
          <div id="descricaodoproduto">

            <h3>Descrição do produto</h3>

            <p>{{$product->description}}</p>

          </div><!-- FIM INFORMAÇÕES PRODUTOS -->

          <!-- ORIGEM DO PRODUTO -->
          <div id="origemdoproduto">

            <h3>origem do produto</h3>
            <div class="retangulo">{{$product->user->address->city}}</div>

          </div><!-- FIM ORIGEM DO PRODUTO -->

          @if(Auth::user())
            <!-- QUALIFICACAO DO VENDEDOR -->
            <div id="qualificacaodovendedor">             
              <h3>qualificação do vendedor</h3>
              <ol data-user-id="{{Auth::user()->id}}" data-product-id="{{$product->id}}" class="estrelinhas">
                @for($i = 1; $i <= 5; $i++)
                  @if($i <= $product->user->rating_value())
                    <li class="active"></li>
                  @else
                    <li></li>
                  @endif
                @endfor
              </ol>
              <div class="quantidade"><span>{{$product->user->count_ratings()}}</span> avaliações</div>

            </div><!-- FIM QUALIFICACAO DO VENDEDOR -->
          @endif

        </div>

      </div><!-- FIM PRIMEIRO NÍVEL -->

      <!-- SEGUNDO NÍVEL -->
      <div id="segundo-nivel">

        <!-- COMPRA SEGURA -->
        <div id="comprasegura">

          <h2>COMPRA SEGURA</h2>

          <div class="entrega">
            <div class="icon"></div>
            <h3>Entrega</h3>
            <p>O pagamento so é liberado ao vendedor<br />
            após você confirmar que recebeu tudo certo</p>
          </div>

          <div class="compraprotegida">
            <div class="icon"></div>
            <h3>Compra protegida</h3>
            <p>A compra é feita em ambiente seguro.<br />
            os dados do cartao não são armazenados</p>
          </div>

        </div><!-- FIM COMPRA SEGURA -->

        <!-- OUTROS PRODUTOS -->
        <div id="outrosprodutos">

          <h2>Outros produtos do vendedor</h2>

          <ul class="produtos">           
            @foreach($product->get_others() as $others)
              <li>
                <a href="{{URL::to_route('view_product', array($others->title()))}}">
                 <img src="{{URL::to("uploads/thumbs/thumb_90x90_{$others->photos()->first()->file_name}")}}" alt="" title="" />
                </a>
              </li>
            @endforeach          

          </ul>

        </div><!-- FIM OUTROS PRODUTOS -->

      </div><!-- FIM SEGUNDO NÍVEL -->

      <!-- SLIDE IMAGEM -->
      {{HTML::script('assets/lib/js/slides.min.jquery.js')}}<!-- FIM SLIDE IMAGEM -->
      
    </div><!-- FIM RIGHT -->
@endsection
