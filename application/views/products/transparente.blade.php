<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

  <meta http-equiv="content-language" content="pt-br" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="refresh" content="1080" />
  <meta http-equiv="Content-Script-Type" content="text/javascript" />
  <meta http-equiv="Content-Style-Type" content="text/css" />

  <meta name="description" content="lorem lorem lorem lorem lorem"/>
  <meta name="keywords" content="lorem lorem lorem lorem lorem"/>

  <title>VIsP</title>

  <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />



    {{HTML::script('assets/lib/js/jquery-1.8.2.min.js')}}
    {{HTML::style('assets/layout/css/style.css')}}
    {{HTML::style('assets/layout/css/style_checkout.css')}}
    {{HTML::script('assets/layout/js/pagamento.js')}}

</head>

<body>

<!-- CONTAINER -->
<div id="container" class="checkout">

  <!-- CENTRO -->
  <div class="centralizando">
  {{Form::open(URL::to_route('finish-sale'), 'POST',array('name'=>'transparente_form'))}}
    {{Form::token()}}

    <ul>

      <li id="passo1" class="ativo">

        <div class="top">

          <div class="top1"></div>

          <span class="passo">Passo 1.</span>
          <span class="nome">Carrinho de compras</span>

        </div>

        <div class="title">Informações pessoais</div>

        <div class="conteudo">

          <h2>Informações de cobrança</h2>

          <div class="left">
            <label>Nome</label>
            <input type="text" name="cobranca_nome" class="nome ladoalado" id="cobranca_nome" value="{{$auth_user->name}}" />
          </div>

          <div class="right">
            <label>Sobrenome</label>
            <input type="text" name="cobranca_sobrenome" class="sobrenome ladoalado" id="sobrenome" />
          </div>

          <div class="left">
            <label>Data de nascimento</label>
            <input type="text" name="cobranca_datadenascimento" class="datadenascimento ladoalado" id="cobranca_datadenascimento" value="{{User::show_date($auth_user->birthday)}}" />
          </div>

          <div class="right">
            <label>Sexo (opcional)</label>
            {{Form::select('cobranca_sex', array('m'=>'Masculino','f'=>'Feminino'), $auth_user->sex,array('class'=>'sexo ladoalado','id'=>'sexo'))}}
            <!-- <input type="text" class="sexo" class="ladoalado" id="sexo" /> -->
          </div>

          <div class="left">
            <label>CPF</label>
            <input type="text" name="cobranca_cpf" class="cpf ladoalado" id="cpf" value="{{$auth_user->cpf}}" />
          </div>

          <div class="right">
            <label>Telefone</label>
            <input type="text" name="cobranca_telefone" class="telefone ladoalado" id="cobranca_telefone" value="{{$auth_user->phone}}"/>
          </div>

          <div class="both">
            <input type="checkbox" name="enderecodeentrega" class="enderecodeentrega" checked /> 
            <label class="enderecodeentrega">Endereço de cobrança igual ao de entrega</label>
          </div>

          <!-- ENDEREÇO DE COBRANÇA -->
          <div style="display:none" class="enderecodecobranca">
            <h2>Endereço de cobrança</h2>

            <div class="left">
              <label>Nome</label>
              <input type="text" name="cobranca_endereco_nome" class="nome ladoalado" value="{{$auth_user->name}}"/>
            </div>

            <div class="right">
              <label>Sobrenome</label>
              <input type="text" name="cobranca_endereco_sobrenome" class="sobrenome ladoalado" id="sobrenome" />
            </div>

            <div class="both cep">
              <label>Cep</label>
              <input type="text" name="cobranca_endereco_cep" class="nome unico"value="{{$auth_user->address->cep}}"/>

              <div class="naoseicep"><a href="#">Não sei o cep</a></div>
            </div>

            <div class="left">
              <label>Endereço</label>
              <input type="text" name="cobranca_endereco_logradouro" class="endereco ladoalado" id="endereco" value="{{$auth_user->address->logradouro}}"/>
            </div>

            <div class="right">
              <label>Número</label>
              <input type="text" name="cobranca_endereco_numero" class="numero ladoalado" id="numero" value="{{$auth_user->address->number}}"/>
            </div>

            <div class="left">
              <label>Complemento</label>
              <input type="text" name="cobranca_endereco_complemento" class="complemento ladoalado" id="complemento" value="{{$auth_user->address->complement}}"/>
            </div>

            <div class="right">
              <label>Bairro</label>
              <input type="text" name="cobranca_endereco_bairro" class="bairro ladoalado" id="bairro" value="{{$auth_user->address->neighborhood}}"/>
            </div>
          </div><!-- FIM ENDEREÇO DE COBRANÇA -->

          <!-- ENDEREÇO DE ENTREGA -->
          <div class="enderecodeentrega">
            <h2>Endereço de entrega</h2>

            <div class="left">
              <label>Nome</label>
              <input type="text" name="entrega_endereco_nome" class="nome ladoalado" value="{{$auth_user->name}}"/>
            </div>

            <div class="right">
              <label>Sobrenome</label>
              <input type="text" name="entrega_endereco_sobrenome" class="sobrenome ladoalado" id="sobrenome" />
            </div>

            <div class="both cep">
              <label>Cep</label>
              <input type="text" name="entrega_endereco_cep" class="nome unico" value="{{$auth_user->address->cep}}" />

              <div class="naoseicep"><a href="#">Não sei o cep</a></div>
            </div>

            <div class="left">
              <label>Endereço</label>
              <input type="text" name="entrega_endereco_logradouro" class="endereco ladoalado" id="endereco" value="{{$auth_user->address->logradouro}}" />
            </div>

            <div class="right">
              <label>Número</label>
              <input type="text" name="entrega_endereco_numero" class="numero ladoalado" id="numero" value="{{$auth_user->address->number}}"/>
            </div>

            <div class="left">
              <label>Complemento</label>
              <input type="text" name="entrega_endereco_complemento" class="complemento ladoalado" id="complemento" value="{{$auth_user->address->complement}}"/>
            </div>

            <div class="right">
              <label>Bairro</label>
              <input type="text" name="entrega_endereco_bairro" class="bairro ladoalado" id="bairro" value="{{$auth_user->address->neighborhood}}"/>
            </div>

            <div class="left">
              <label>Estado</label>
              <select name="cobranca_endereco_uf" name="entrega_endereco_estado" class="uf" id="uf">
              </select>
            </div>

            <div class="right">
              <label>Cidade</label>
              <select name="cobranca_endereco_cidade" name="entrega_endereco_cidade" class="cidade" id="cidade">
              </select>
            </div>
          </div><!-- FIM ENDEREÇO DE ENTREGA -->

        </div>

      </li><!-- FIM PASSO 1 -->

      <li id="passo2">

        <div class="top">

          <div class="top1"></div>

          <span class="passo">Passo 2.</span>
          <span class="nome">Informações para pagamento</span>

        </div>

        <div class="title">Formas de pagamento</div>

        <div class="conteudo">
  
          <!-- CARTAO DE CREDITO -->
          <input type="radio" style="float:left;width:10px" name="paymentway" value="creditcardoption">
          <h2 style="width:230px">Cartão de Crédito</h2>
          <div class="area">

            <div class="both cartao">
              {{HTML::image('assets/layout/images/cartoesaceitos.jpg')}}
              <select name="creditcardinstitution" class="cartoes" id="creditcardinstitution">
                <option value="">Cartão de Crédito</option>
                <option value="AmericanExpress">American Express</option>
                <option value="Diners">Dinners</option>
                <option value="Mastercard">Mastercard</option>
                <option value="Hipercard">Hipercard</option>
                <option value="Visa">Visa</option>
              </select>
            </div>

            <div class="both cartao">
              <label>N<sup>o</sup> de parcelas</label>
              {{Currency::select_parcels($total_price)}}
            </div>

            <div class="both">  
              <label>N<sup>o</sup> do cartão de crédito<sup class="obrigatorio">*</sub></label>
              <input type="text" name="creditcardnumber" class="numdocartaodecredito" id="creditcardnumber" />
            </div>

            <div class="left">
              <label>Data de vencimento<sup>*</sup></label>
              <select name="month" class="mes" id="month">
                <option  value="">Mês</option>
                <option  value="01">01</option>
                <option  value="02">02</option>
                <option  value="03">03</option>
                <option  value="04">04</option>
                <option  value="05">05</option>
                <option  value="06">06</option>
                <option  value="07">07</option>
                <option  value="08">08</option>
                <option  value="09">09</option>
                <option  value="10">10</option>
                <option  value="11">11</option>
                <option  value="12">12</option>
              </select>
            </div>

            <div class="right">
              <label></label>
              <select name="year" class="ano" id="year">
                <option value=""  >Ano</option>
                <option value="13">2013</option>
                <option value="14">2014</option>
                <option value="15">2015</option>
                <option value="16">2016</option>
                <option value="17">2017</option>
                <option value="18">2018</option>
                <option value="19">2019</option>
                <option value="20">2020</option>
                <option value="21">2021</option>
                <option value="22">2022</option>
                <option value="23">2023</option>
              </select>
            </div>

            <div class="both">
              <label>N<sup>o</sup> de segurança<sup class="obrigatorio">*</sup></label>
              <input type="text" class="seguranca" name="securitynumber" id="securitynumber" />
            </div>

          </div><!-- FIM CARTAO DE CREDITO -->

          <!-- BOLETO BANCÁRIO -->
          <input style="float:left;width:10px" type="radio" name="paymentway" value="billetoption">
          <h2 style="width:230px">Boleto Bancário</h2>
          <div class="area">
            <div class="boleto">
              <a href="#">Imprima o boleto bancário após a finalização do pedido</a>
            </div>
          </div><!-- FIM BOLETO BANCÁRIO -->

          <!-- TRANSFERÊNCIA BANCÁRIA -->
          <input style="float:left;width:10px" type="radio" name="paymentway" value="banktransferoption">
          <h2 style="width:230px">Transferência Bancária</h2>
          <div class="area">
            <ul class="bancos">
              <li style="width:225px" class="bb li-banktransfer">
                <a href="#">Banco do Brasil</a>
                <input type="radio" name="banktransfer" value="BancoDoBrasil">
              </li>
              <li style="width:225px" class="itau li-banktransfer">
                <a href="#">Banco Itaú</a>
                <input type="radio" name="banktransfer" value="Itau">
              </li>
              <li style="width:225px" class="bradesco li-banktransfer">
                <a href="#">Banco Bradesco</a>
                <input type="radio" name="banktransfer" value="Bradesco">
              </li>
            </ul>
          </div><!-- FIM TRANSFEÊNCIA BANCÁRIA -->

        </div>

        <script type="text/javascript">
        $('li#passo2 input:radio').click(function(){
          $(this).next('h2').trigger('click');
        });
        $('li#passo2 h2').click(function(){
          $('li#passo2 .area').hide();
          $(this).prev('input').attr('checked',true);
          $(this).next('div').show();
        })
        </script>

      </li><!-- FIM PASSO 2 -->

      <li id="passo3">

        <div class="top">

          <div class="top1"></div>

          <span class="passo">Passo 3.</span>
          <span class="nome">Finalizar Compra</span>

        </div>

        <div class="title">Revisar pedido</div>

        <div class="finalizarcompra top botao"><a href="#">Finalizar compra</a></div>

        <div class="conteudo">

          <table width="250" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="60%" class="produto">Produto</td>
              <td width="20%" class="qtd">Qtd</td>
              <td width="20%" class="preco">Preço</td>
            </tr>
            
            @foreach($cart->get_products() as $product_id)
              <tr>
                <td width="60%" class="produto">
                  <div class="imagemproduto"><img src="{{URL::to("uploads/thumbs/thumb_90x90_".Product::find($product_id)->photos()->first()->file_name)}}" /></div>
                  <div class="nomeproduto">{{Product::find($product_id)->title}}</div>
                </td>
                <td width="20%" class="qtd">
                  <div class="numeroqtd">1</div>
                </td>
                <td width="20%" class="preco">
                  <div class="valorpreco">{{Currency::show_price(Product::find($product_id)->price)}}</div>
                </td>
              </tr>
            @endforeach

            

            <tr class="subtotal">
              <td class="primeiro" width="70%" colspan="2">Subtotal</td>
              <td class="segundo" width="25%">{{Currency::show_price($total_price)}}</td>
            </tr>

            <tr class="frete">
              <td class="primeiro" width="70%" colspan="2">Frete</td>
              <td class="segundo" width="25%">{{Currency::show_price($transport_price)}}</td>
            </tr>

            <tr class="desconto">
              <td class="primeiro" width="70%" colspan="2">Desconto</td>
              <td class="segundo" width="25%">R$ 0,00</td>
            </tr>

            <tr class="total">
              <td class="primeiro" width="70%" colspan="2">Total</td>
              <td class="segundo" width="25%">{{Currency::show_price($total_price + $transport_price)}}</td>
            </tr>

          </table>

        </div>
        <input type="hidden" value="" id="sale_id">
        <div class="finalizarcompra botao bottom"><a href="#" name="pay" id="pay">Finalizar compra</a></div>

        <div id="MoipWidget"
          data-token=""
          callback-method-success="success"
          callback-method-error="failReturn">
        </div>

      </li><!-- FIM PASSO 3 -->

    </ul>
{{Form::close()}}
  </div><!-- FIM CENTRO -->

</div><!-- FIM CONTAINER -->

</body>
</html>

<!-- UF E CIDADE PAG CADASTRO -->
{{HTML::script('assets/layout/js/cidades-estados.js')}}
{{HTML::script('assets/lib/js/MoipWidget-v2.js',array('charset'=>'ISO-8859-1'))}}

<script type="text/javascript">
new dgCidadesEstados({
  cidade: document.getElementById('cidade'),
  estado: document.getElementById('uf')
});
$('input.enderecodeentrega').click(function(){
  if($(this).is(':checked')){
    $('.enderecodecobranca').hide({});
  }else{
    $('.enderecodecobranca').show({});
  }
})
</script>
<!-- FIM UF E CIDADE PAG CADASTRO -->

<!-- SCRIPTS GERAL DO SITE -->
{{-- HTML::script('assets/layout/js/scripts.js' --)}}
<!-- FIM SCRIPTS GERAL DO SITE -->

