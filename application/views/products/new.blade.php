@section('content')
	<div class="form-cadastro">
		<fieldset>
			<legend>Adicionar Produto</legend>
			{{Form::open('products','POST')}}

				{{Form::token()}}

				@if (Session::has('errors'))
					<div class="error">
			    		<span>{{Session::get('errors')}}</span>
					</div>
				@endif
			
		   		<div class="dados-acesso">
		   			{{Form::label('product_title','Título')}}
		   			{{Form::input('text','product_title',Input::old('product_title'))}}<br>
					
					{{Form::label('product_description','Descrição')}}
		   			{{Form::input('text','product_description',Input::old('product_description'))}}<br>

					{{Form::label('product_category_id','Categoria')}}					
		   			{{Form::select('product_category_id',array(''=>'Selecione','1'=>'Teste'),Input::old('product_category_id'))}}<br>

					{{Form::label('product_weight','Peso')}}
		   			{{Form::input('text','product_weight',Input::old('product_weight'))}}<br>

					{{Form::label('product_origin','Origem')}}
		   			{{Form::input('text','product_origin',Input::old('product_origin'))}}<br>

					{{Form::label('product_condition','Condição')}}
		   			{{Form::input('text','product_condition',Input::old('product_condition'))}}<br>

					{{Form::label('product_original_price','Preço Original')}}
		   			{{Form::input('text','product_original_price',Input::old('product_original_price'))}}<br>

					{{Form::label('product_price','Preço')}}
		   			{{Form::input('text','product_price',Input::old('product_price'))}}<br>

					{{Form::label('product_amount','Quantidade')}}
		   			{{Form::input('text','product_amount',Input::old('product_amount'))}}<br>

					{{Form::hidden('product_user_id', $user_id)}}					
				</div>
				{{Form::submit('Enviar')}}	
			{{Form::close()}}
		</fieldset>
	</div>	

@endsection