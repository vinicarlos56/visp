@layout('layout.main')

@section('content')


  <!-- CENTRO -->
 
   <div class="centralizando content resultadodebusca">
    <!--SIDEBAR-->

    <!-- RIGHT -->
    <div id="right">
      @if(isset($category) and isset($subcategory))
        <h1>Resultado de busca para: <span>{{$category}} {{$subcategory}}</span></h1>
      @elseif(isset($term))
        <h1>Resultado de busca para: <span>{{$term}}</span></h1>
      @endif


      @if($products)
      <ul class="produtos">
        

        @foreach($products as $product)
          <!-- PRODUTO -->
          <li>

            <a href="{{URL::to_route('view_product',array($product->title()))}}">

              @if($product->sold)
                <div class="flag-nunca-usado-thumb">Nunca usado</div>
              @endif
              <div class="foto">
                <img src="{{URL::to("uploads/thumbs/thumb_90x90_{$product->photos()->first()->file_name}")}}" alt="" title="" />
              </div>
            </a>

            <!-- FIM MEIO -->
            <div class="meio">
              <a href="#">
                <h2 class="tituloproduto">{{$product->title}}</h2>
                <div class="ref">ref. {{$product->id}}</div>
              </a>

              <!-- PREÇO -->
              <div id="preco">

                <a href="#">

                  <div class="atual">Por: <span>R$ {{$product->price}}</span></div>

                </a>

              </div><!-- FIM PREÇO -->

            </div><!-- FIM MEIO -->

          </li><!-- FIM PRODUTO -->       
      @endforeach
      </ul>
      @else
        <h1>Nenhum reultado encontrado!</h1>
      @endif
<!-- 
      <div class="exibirmais">
        <div class="mais">exibir mais produtos</div>
      </div> -->

      
    </div><!-- FIM RIGHT -->

@endsection