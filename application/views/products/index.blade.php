@section('div')
	<div class="centralizando content minhavitrine minhasvendas">
@endsection

@section('user_content')


	<!-- RIGHT -->
    <div id="right">
		@if (Session::has('error'))
			<div class="error">
				<span>{{Session::get('error')}}</span>
			</div>
		@elseif(Session::has('success'))
			<div class="success">
				<span>{{Session::get('success')}}</span>
			</div>
		@endif	

      <h1>MINHAS VENDAS</h1>

      <?php /*<p>
			<b>
				{{$product->title}} - 
				{{HTML::link("products/{$product->id}/edit", 'Editar')}}
				{{HTML::link("products/{$product->id}/delete", 'Deletar')}}
			</b>
			
			@foreach($product->photos as $photo)
				<img src="{{URL::to("uploads/thumbs/thumb_190x190_{$photo->file_name}")}}">
			@endforeach
		</p>*/ ?>

      <ul class="produtos">

        <!-- PRODUTO -->
        @foreach($products as $product)		
	        <li>
	          <a href="#">
				@if($product->sold)
	            	<div class="flag-nunca-usado-thumb">Nunca usado</div>	            
	            @endif
	            <div class="foto">
	              @if($product->photos()->first())
	              	<img style="width:94px" src="{{URL::to("uploads/thumbs/thumb_190x190_{$product->photos()->first()->file_name}")}}" alt="" title="" />
	              @endif
	            </div>
	          </a>

	          <!-- FIM MEIO -->
	          <div class="meio">
	            <a href="#">
	              <h2 class="tituloproduto">{{$product->title}}</h2>
	              <div class="ref">ref. {{$product->id}}</div>
	            </a>

	            <!-- PREÇO -->
	            <div id="preco">

	              <a href="#">

	                <!-- <div class="de">de: <span>R$ {{$product->original_price}}</span></div> -->
	                <div class="atual">Por: <span>R$ {{$product->price}}</span></div>

	              </a>

	            </div><!-- FIM PREÇO -->
				@if($product->sold)
	            <!-- RASTREAMENTO -->
	            <div id="rastreamento">

	              <span id="tooltip-rastreamento" class="tooltip-rastreamento">
	                
	                <form>
	                  <div class="data">Data de envio</div>
	                  <input type="text" class="dataenvio" id="dataenvio" name="dataenvio" />

	                  <div class="numero">Número de restreamento</div>
	                  <input type="text" class="numerorastreamento" id="numerorastreamento" name="numerorastreamento" />
	                
	                  <input type="submit" class="enviar" id="enviar" />
	                </form>
	              </span>
	              <div class="icon"></div>

	            </div><!-- FIM RASTREAMENTO -->
	            @endif
	          </div><!-- FIM MEIO -->

	          <!-- DIREITA -->
	          <div class="direita">

	            <!-- EDITAR -->
	            <div id="editar" class="botao">
	            	{{HTML::link("products/{$product->id}/edit", 'Editar')}}           
	            </div><!-- FIM EDITAR -->

	            <!-- DELETAR -->
	            <!-- <div id="close" class="botao"> -->
	            	{{HTML::link("products/{$product->id}/delete", 'Deletar',array('id'=>'close','class'=>'botao','onclick'=>"return confirm('Deseja excluir este produto?')"))}}
	            <!-- </div>FIM DELETAR -->

	          </div><!-- FIM DIREITA -->

	        </li><!-- FIM PRODUTO -->                
		@endforeach
		
      </ul>

      
    </div><!-- FIM RIGHT -->
@endsection