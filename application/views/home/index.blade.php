
@section('content')

<!-- PRATELEIRA -->
<div id="right">
	<h1>produtos em destaque</h1>

	<ul class="prateleira">
		@foreach($products as $product)
			<li id="tooltip-target-{{$product->id}}">
				<span id="tooltip-content-{{$product->id}}" class="tooltip">
					<div class="nome">{{$product->title}}</div>
					<div class="usadoounovo">{{$product->condition()}}</div>
				</span>
				<a href="{{URL::to("produto/{$product->title()}")}}">
					<div class="informacao">
						<div class="tamanho {{$product->subcategory()->get_css_class()}}">{{$product->size}}</div>
						<div class="preco {{$product->subcategory()->get_css_class()}}">R$ {{$product->price}}</div>
					</div>					
					<img src="{{URL::to("uploads/thumbs/thumb_190x190_{$product->photos()->first()->file_name}")}}" alt="" title="" />
				</a>
			</li>
		@endforeach
	</ul>	

	<div class="exibirmais">
		<div class="mais">exibir mais produtos</div>
	</div>
	
</div><!-- FIM PRATELEIRA -->
	
@endsection