<?php

use Services\Sale as Sale;

Session::start('memory');

class TestSalesService extends PHPUnit_Framework_TestCase {

	public static function setUpBeforeClass()
	{
		Tests\Helper::migrate();

		DB::table('users')->delete();
		DB::query('delete from products;');
		DB::query('delete from sqlite_sequence where name=\'products\';');

		self::seedData();
	}

	public function setUp(){
		$this->buyer_cep  = '20050-005';
		$this->correios   = $this->getMock('Correios');		

		$this->cart_array = array(
			'users'=>array(
				1=>array('products' => array(1,2)),
				2=>array('products' => array(3,4)),				
			)
		);

		Session::put('shoppingcart',$this->cart_array);	
	}

	private static function seedData()
	{
		Product::create(array('id'=> 1,'user_id' => 1,'price'=>12));
		Product::create(array('id'=> 2,'user_id' => 1,'price'=>13));
		Product::create(array('id'=> 3,'user_id' => 2,'price'=>14));
		Product::create(array('id'=> 4,'user_id' => 2,'price'=>15));

		User::create(array('id'=> 1)); //sellr
		User::create(array('id'=> 2)); //seller
		User::create(array('id'=> 3)); // buyer

		Address::create(array('user_id'=>'1','cep' => '21645-440'));
		Address::create(array('user_id'=>'2','cep' => '21645-441'));
		Address::create(array('user_id'=>'3','cep' => '20050-005'));
	}

	public function testIfCanCalculateTheTotalTransportPriceFromAShoppingCart()
	{
		$sale = new Sale;
		$this->correios->expects($this->exactly(4))
		   ->method('calculate_transport')
		   ->with($this->equalto(
		   		$this->buyer_cep),
		   		$this->isType('string'),
		   		$this->isInstanceOf('\Product')
		   	)->will($this->returnValue(10));		

		$sale->set_correios($this->correios);		

		$total_price = $sale->calculate_total_transport_price($this->buyer_cep);
		$this->assertEquals(40, $total_price);
	}

	public function testIfReturnNullWhenNoCepIsProvided()
	{
		$sale = new Sale;
		$sale->set_correios($this->correios);

		$total_price  = $sale->calculate_total_transport_price(null);
		$this->assertNull($total_price);

		$total_price  = $sale->calculate_total_transport_price(0);
		$this->assertNull($total_price);

		$total_price  = $sale->calculate_total_transport_price('');
		$this->assertNull($total_price);
	}

	public function testIfWhenCalculateATransportPricesListIsFilled()
	{
		$sale = new Sale;
		$this->correios->expects($this->exactly(4))
		   ->method('calculate_transport')
		   ->will($this->onConsecutiveCalls(10,5,12,6));

		$sale->set_correios($this->correios);		
		$sale->calculate_total_transport_price($this->buyer_cep);

		$this->assertEquals(array(
			1=>15,
			2=>18
		), $sale->get_transport_prices_list());
	}

	public function testIfCanCalculateTheTotalPriceFormCart()
	{		
		$cart = new \ShoppingCart;
		$this->assertEquals(54, Sale::get_total_selling_price($cart));
	}

	public function testIfCanGetTheTotalPriceBySellerOnCart()
	{		
		$this->assertEquals(25, Sale::get_price_by_seller(1));
		$this->assertEquals(29, Sale::get_price_by_seller(2));
	}

	public function testShoulReturnNullWhenNoSellerIdIsProvided()
	{
		$this->assertNull(Sale::get_price_by_seller(null));
		$this->assertNull(Sale::get_price_by_seller(0));
		$this->assertNull(Sale::get_price_by_seller(false));
	}

	public function testShouldReturnNullWhenShoppingCartIsEmpty()
	{
		Session::put('shoppingcart',array());
		$this->assertNull(Sale::get_price_by_seller(1));
		$this->assertNull(Sale::get_price_by_seller(2));
	}

	
}