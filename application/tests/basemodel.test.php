<?php

class TestBaseModel extends PHPUnit_Framework_TestCase {
	
	public function testInstance(){
		$base = new Base_Model;
		$this->assertInstanceOf('Base_Model', $base);
	}

	public function testIfExtractCanExtractTheModelFromAPost(){		
		$post = $this->provider();
		
		$extracted = Onemodel::extract($post,array('onemodel','relatedmodel'));
		$this->assertEquals(array(
			'onemodel' =>array(
				'name'				=>'Carlos',
				'email'				=>'vinicarlos56@hotmail.com',
				'email_confirmation'=>'vinicarlos56@hotmail.com',
				'nickname'			=>'vinicarlos56'
			),
			'relatedmodel' => array(
				'logradouro'=>'Rua Oliveira Bueno',
				'cep'		=>'21645440',
				'city'		=>'Rio de Janeiro'
			)
		), $extracted);
	}

	public function testIfExtractBehaviorNotChangeWithAnEmptyInput(){
		$post = $this->provider();
		$post['onemodel_name'] = '';
		$post['relatedmodel_cep'] = '';

		$extracted = Onemodel::extract($post,array('onemodel','relatedmodel'));
		$this->assertEquals(array(
			'onemodel' =>array(
				'name'				=>'',
				'email'				=>'vinicarlos56@hotmail.com',
				'email_confirmation'=>'vinicarlos56@hotmail.com',
				'nickname'			=>'vinicarlos56'
			),
			'relatedmodel' => array(
				'logradouro'=>'Rua Oliveira Bueno',
				'cep'		=>'',
				'city'		=>'Rio de Janeiro'
			)
		), $extracted);
	}

	public function testIfValidateCanValidateTheOneModel(){
		$post = array(
			'onemodel_name'=>'Carlos',
			'onemodel_email'=>'vinicarlos56@hotmail.com',
			'onemodel_email_confirmation'=>'vinicarlos56@hotmail.com',
			'onemodel_nickname'=>'vinicarlos56'
		);		
		
		$result = Onemodel::validate($post,array('onemodel'));
		$this->assertTrue($result);

		$post['onemodel_name'] = '';
		$result = Onemodel::validate($post,array('onemodel'));
		$this->assertFalse($result);

		$post['onemodel_email_confirmation'] = '';
		$result = Onemodel::validate($post,array('onemodel'));
		$this->assertFalse($result);
	}

	public function testIfValidateCanValidateOneModelAndTheRalatedodel(){
		$post = $this->provider();

		$result = Onemodel::validate($post,array('onemodel','relatedmodel'));		
		$this->assertTrue($result);

	}

	public function testIfValidateCanGenerateAnArrayWithTheErrorsFound(){
		$post = $this->provider();
		$post['onemodel_name'] = '';		
		$result = Onemodel::validate($post,array('onemodel','relatedmodel'));		
		$this->assertEquals(array('O campo nome deve ser preenchido.'),Onemodel::$validation_errors);

		$post['onemodel_name'] = '';
		$post['onemodel_email_confirmation'] = '';		
		$result = Onemodel::validate($post,array('onemodel','relatedmodel'));		
		$this->assertEquals(array(
			'O campo nome deve ser preenchido.',
			'O email confirmação não coincide.'
		),Onemodel::$validation_errors);
	}

	public function testIfErrorsToStringCanCreateAnStringWithTheErrors(){
		$post = $this->provider();
		$post['onemodel_name'] = '';
		$post['onemodel_email_confirmation'] = '';		
		$result = Onemodel::validate($post,array('onemodel','relatedmodel'));		
		$this->assertEquals('O campo nome deve ser preenchido.<br>O email confirmação não coincide.',
		Onemodel::errors_to_string());
	}

	public function provider(){
		return  array(
			'onemodel_name'					=>'Carlos',
			'onemodel_email'				=>'vinicarlos56@hotmail.com',
			'onemodel_email_confirmation'	=>'vinicarlos56@hotmail.com',
			'onemodel_nickname'				=>'vinicarlos56',
			'relatedmodel_logradouro'		=>'Rua Oliveira Bueno',
			'relatedmodel_cep'				=>'21645440',
			'relatedmodel_city'				=>'Rio de Janeiro'
		);
	}

}

class Onemodel extends Base_Model{	
	public static $validation_rules = array(
		'name' 		=>'required',
		'email'		=>'required|confirmed',
		'nickname'  =>'required'
	);

	public static $validation_messages = array(
		'name_required'=>'O campo nome deve ser preenchido.'
	);
	
}

class Relatedmodel	extends Base_Model{

	public static $validation_rules = array(
		'cep' 		=>'required',
		'logradouro'=>'required',
		'city'      =>'required'
	);	
}