<?php

class TestUserModel extends PHPUnit_Framework_TestCase {

	/*
	 Should run migrations before run tests
	*/
	public static function setUpBeforeClass()
	{
		Tests\Helper::migrate();
		Tests\Helper::use_sessions();
	}

	/*
	 Clean table between every test
	*/
	public function setUp()
	{
		DB::table('users')->delete();		
	}

	public function testIsWorking()
	{
		$user = User::create(array('name'=>'Carlos'));
		$this->assertEquals('Carlos', $user->name);
	}

	

}
	