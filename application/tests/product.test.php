<?php

Session::start('memory');

class TestProoduct extends PHPUnit_Framework_TestCase {

	public static function setUpBeforeClass()
	{
		Tests\Helper::migrate();
		DB::query('delete from products;');
		DB::query('delete from sqlite_sequence where name=\'products\';');
		self::seedData();
	}

	private static function seedData()
	{
		Product::create(array('id'=> 1,'user_id' => 1,'price'=>12));
		Product::create(array('id'=> 2,'user_id' => 1,'price'=>13));
		Product::create(array('id'=> 3,'user_id' => 2,'price'=>14));
		Product::create(array('id'=> 4,'user_id' => 2,'price'=>15));
	}

	public function setUp(){	

		$this->cart_array = array(
			'users'=>array(
				1=>array('products' => array(1,2)),
				2=>array('products' => array(3,4)),				
			)
		);

		Session::put('shoppingcart',$this->cart_array);	
	}

	public function testIfCanLoadProductsInstancesFromAUserInCart()
	{
		$cart    = new ShoppingCart;
		$result  = Product::load_by_user($cart,1);
		$this->assertInstanceOf('Product', $result[0]);
		$this->assertInstanceOf('Product', $result[1]);
	}

	public function testIfLoadTheInstancesCorrectlyFromCart()
	{		
		$cart    = new ShoppingCart;
		$result  = Product::load_by_user($cart,1);
		$this->assertEquals(1, $result[0]->id);
		$this->assertEquals(2, $result[1]->id);
	}
}
