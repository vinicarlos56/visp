<?php

Session::start('memory');

class TestShoppingCart extends PHPUnit_Framework_TestCase {

	public function setUp()
	{		
		$this->shoppingcart = new ShoppingCart();
	}

	public function tearDown()
	{
		Session::put('shoppingcart',array());
	}
	
	public function testInstance(){
		$base = new ShoppingCart;
		$this->assertInstanceOf('ShoppingCart', $base);
	}

	public function testIfCartPropertieStartsEmptyInAnewInstance()
	{
		$this->assertEmpty($this->shoppingcart->get_cart());
	}

	public function testIfAddProductAddTheUserAndTheProductCorrectlyInTHeCart()
	{

		$product = $this->newProduct(1,2);

		$this->shoppingcart->add_product($product);
		
		$this->assertEquals(array(
			'users'=>array(
				2=>array('products'=>array(1)),
			),
		),$this->shoppingcart->get_cart());

		$product2 =$this->newProduct(2,2);

		$this->shoppingcart->add_product($product2);
	
		$this->assertEquals(array(
			'users'=>array(
				2=>array('products'=>array(1,2)),
			),
		),$this->shoppingcart->get_cart());

	}

	public function testIfGetProductsCanGetTheProductsOfCartToAnArray()
	{	
		$this->addProducts($this->shoppingcart);

		$this->assertEquals(array(
			1,2,3,4
		), $this->shoppingcart->get_products());

	}

	public function testIfRemoveMethodRemoveOneProductCorrectly()
	{
		$this->addProducts($this->shoppingcart);

		$product = $this->newProduct(1,1);

		$this->shoppingcart->remove_product($product);

		$this->assertEquals(array(
			'users'=>array(
				1=>array('products' => array(2)),
				2=>array('products' => array(3,4)),				
			)
		), $this->shoppingcart->get_cart());

		$product2 = $this->newProduct(3,2);

		$this->shoppingcart->remove_product($product2);

		$this->assertEquals(array(
			'users'=>array(
				1=>array('products' => array(2)),
				2=>array('products' => array(4)),				
			)
		), $this->shoppingcart->get_cart());
	}

	public function testIfTheUserIsRemovedWHenThereisNoMoreProductsOfHim()
	{
		$this->addProducts($this->shoppingcart);

		$product  = $this->newProduct(1,1);
		$product2 = $this->newProduct(2,1);

		$this->shoppingcart->remove_product($product);
		$this->shoppingcart->remove_product($product2);

		$this->assertEquals(array(
			'users'=>array(				
				2=>array('products' => array(3,4)),				
			)
		), $this->shoppingcart->get_cart());
	}

	public function testIfCountProductsCountTheProductsCorrectly()
	{
		$this->addProducts($this->shoppingcart);
		$this->assertEquals(4,$this->shoppingcart->count_products());
	}

	public function testIfCartIsEmptyWhenAllProductsAreRemoved()
	{
		$this->addProducts($this->shoppingcart);

		$product1 = $this->newProduct(1,1);
		$product4 = $this->newProduct(2,1);
		$product2 = $this->newProduct(3,2);
		$product3 = $this->newProduct(4,2);

		$this->shoppingcart->remove_product($product1);
		$this->shoppingcart->remove_product($product2);
		$this->shoppingcart->remove_product($product3);
		$this->shoppingcart->remove_product($product4);

		$cart = $this->shoppingcart->get_cart();

		$this->assertTrue(empty($cart));
	}

	public function testIfGetSellersReturnTheUsersCorrectly()
	{
		$this->addProducts($this->shoppingcart);

		$sellers = $this->shoppingcart->get_sellers();
		$this->assertEquals(array(1,2), $sellers);
	}

	public function testIfCanGetTheProductsBySellerId()
	{
		$this->addProducts($this->shoppingcart);
		$this->assertEquals(array(
			1,2
		), $this->shoppingcart->get_products_by_user(1));

		$this->assertEquals(array(
			3,4
		), $this->shoppingcart->get_products_by_user(2));
	}

	public function testIfreturnNullWhenNoUserIdIsProvided()
	{		
		$this->addProducts($this->shoppingcart);
		$this->assertNull($this->shoppingcart->get_products_by_user(false));
	}

	private function getCart(){
		return array(
			'users'=>array(
				1=>array('products' => array(1,2)),
				2=>array('products' => array(3,4)),				
			)
		);
	}

	private function addProducts($shoppingcart){
		$product1 = $this->newProduct(1,1);
		$product4 = $this->newProduct(2,1);
		$product2 = $this->newProduct(3,2);
		$product3 = $this->newProduct(4,2);

		$shoppingcart->add_product($product1);
		$shoppingcart->add_product($product2);
		$shoppingcart->add_product($product3);
		$shoppingcart->add_product($product4);
	}

	private function newProduct($product_id,$user_id) {
		$product = new Product;
		$product->id = $product_id;
		$product->user_id = $user_id;
		return $product;
	}
}