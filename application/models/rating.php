<?php 

class Rating extends Base_Model
{
	public static $timestamps = true;

	public function product(){
		return $this->belongs_to('Product');
	}

}