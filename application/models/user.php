<?php 

class User extends Base_Model
{
	public static $timestamps = true;

	public static $validation_rules = array(		
		// 'user_id'   => 'required', 
		// 'name'      => 'required',
		// 'password'	=> 'required|confirmed',			
		// 'email'     => 'required|email|confirmed|unique:users',
		// 'nickname'  => 'required|unique:users',
		// 'birthday' 	=> 'required',
		// 'sex'		=> 'required',
		// 'cpf'		=> 'required|unique:users',
		// 'cell_phone'=> 'required',
		// 'phone'		=> 'required'
	);

	public function check($input,$default='')
	{
		return isset($input) ? $input : $default;
	}

	// TODO: Criar o Remover childrens

	public function save_user($input,$user_id = false,$childrens=false)
	{
		if(isset($input['user_name']))
			$user_data['name'] = $input['user_name'];

		if(isset($input['user_password']))
			$user_data['password'] = Hash::make($input['user_password']);

		if(isset($input['user_email']))
			$user_data['email'] = $input['user_email'];

		if(isset($input['user_nickname']))
			$user_data['nickname'] = $input['user_nickname'];

		if(isset($input['user_birthday']))
			$user_data['birthday'] = self::store_date($input['user_birthday']);

		if(isset($input['user_sex']))
			$user_data['sex'] = $input['user_sex'];

		if(isset($input['user_cpf']))
			$user_data['cpf'] = $input['user_cpf'];

		if(isset($input['user_cnpj']))
			$user_data['cnpj'] = $input['user_cnpj'];

		if(isset($input['user_cell_phone']))
			$user_data['cell_phone'] = $input['user_cell_phone'];

		if(isset($input['user_phone']))
			$user_data['phone'] = $input['user_phone'];
		
		// FIXME: Ajustar Corretamente a coluna de estatdos (state)

		if(isset($input['address_cep']))
			$address_data['cep'] = $input['address_cep'];

		if(isset($input['address_logradouro']))
			$address_data['logradouro'] = $input['address_logradouro'];

		if(isset($input['address_city']))
			$address_data['city'] = $input['address_city'];

		if(isset($input['address_neighborhood']))
			$address_data['neighborhood'] = $input['address_neighborhood'];

		if(isset($input['address_complement']))
			$address_data['complement'] = $input['address_complement'];

		if(isset($input['address_number']))
			$address_data['number'] = $input['address_number'];

		if(isset($input['address_state']))			
			$address_data['state'] = $input['address_state'];
		

		if ($user_id) {
			$user = User::find($user_id);			
			
			if ($childrens) {

				// TODO: Refatorar, colocar dentro do model children
				for ($i = 0 ; $i < count($childrens['names']) ; $i ++) {
					$children_arr[] = array(
						'name' 		=> !empty($childrens['names'][$i]) ? $childrens['names'][$i] : '',
						'birthday'  => !empty($childrens['birthdays'][$i]) ? User::store_date($childrens['birthdays'][$i]) : '',
						'sex'		=> !empty($childrens['sex'][$i]) ? $childrens['sex'][$i] : '',
						'id'		=> isset($childrens['id'][$i]) ? $childrens['id'][$i] : false,
						'user_id'	=> $user_id
					);
				}

				foreach ($children_arr as $children) {		
					// TODO: colocar Validação
					if (!empty($children['name']) AND !empty($children['birthday']) AND !empty($children['sex'])) {
						if ($children['id']) {
							$children_obj = Children::find($children['id']);
							$children_obj::update($children['id'],$children);
						} else {
							// TODO: Tratar Erros
							Children::create($children);	
						}
					}
				}				
			}			
			// NOTE: encontrar uma outra maneira de atualizar o model
			if(isset($address_data)){
				if($user->address()->update($address_data) AND $user::update($user_id,$user_data))
					return true;				
			}else{
				if($user::update($user_id,$user_data))
					return true;	
			}
		} else {			
			if (!User::create($user_data)) return false;

			$new_user = User::where('nickname','=',$input['user_nickname'])->first();
			if ($new_user->address()->insert($address_data))
				return true;			
		}			
	}

	public function roles()
	{
        return $this->has_many_and_belongs_to('Role', 'role_user');
    }

    public function has_role($key)
    {
        foreach($this->roles as $role)
            if($role->name == $key)
                return true;
        
        return false;
    }

    public function has_any_role($keys)
    {
        if( ! is_array($keys))
            $keys = func_get_args();       

        foreach($this->roles as $role)        
            if(in_array($role->name, $keys))            
                return true;

        return false;
    }

    public function photos()
    {
    	return $this->has_many('Photo','model_id');
    }

	public function address()
	{
		return $this->has_one('Address');
	}

	public function products()
	{
		return $this->has_many('Product');
	}

	public function bookmarks()
	{
		return $this->has_many_and_belongs_to('Product','bookmarks');
	}

	public function buy(ShoppingCart $cart)
	{
		# code...
	}

	public function childrens()
	{
		return $this->has_many('Children');
	}

	// TODO: Refatorar, criar um helper e remover daqui
	public static function store_date($date,$datetime =false)
	{
		$exploded = explode('/',$date);

		$date = @$exploded[2].'-'.@$exploded[1].'-'.@$exploded[0];
		
		//YYYY-MM-DD
		if(!$datetime)
			return $date;
		else
			return $date.' 00:00:00';
	}

	public static function show_date($date,$datetime=true)
	{
		if($datetime)
			$date = substr($date, 0,-8);

		$exploded = explode('-',$date);		

		return trim($exploded[2]).'/'.$exploded[1].'/'.$exploded[0];
	}

	public function get_sales()
	{
		$re = Product::where('user_id','=',$this->id)->where('sold','=',1)->get();

		return count($re);
	}

	public function get_purchases()
	{
		// TODO: remover, duplicidade
		$sales=Sale::where('buyer_id','=',Auth::user()->id)->get();
		
		$products = array();
		foreach ($sales as $sale) {
			foreach ($sale->orders as $order) {
				$products[] = $order->product;
			}
		}	

		return count($products);
	}

	public function ratings()
	{
		return $this->has_many('Rating');
	}

	public function rating_value()
	{
		if ($this->ratings) {
			$value = $count = 0;
			foreach ($this->ratings as $rating) {
				$value += $rating->rating;
				$count++;
			}
			return ceil($value/$count);
		}
	}

	public function count_ratings()
	{
		return count($this->ratings);
	}

	public static function get_fb_gender($gender)
	{
		if($gender == 'female')
			return 'f';
		elseif($gender == 'male')
			return 'm';
	}
	
	public function check_moip_email($email)
	{

		$url  = "https://desenvolvedor.moip.com.br/sandbox/ws/alpha/VerificarConta/{$email}";
		$auth = 'AVMWTOYDQFYWJ7QNBJWPEAR0HN6MFNTG:J62J8TAQGYTSIOPUETXDGAGNVDRUUIL7PUOJSMDD';

		//O HTTP Basic Auth é utilizado para autenticação
		$header[] = "Authorization: Basic " . base64_encode($auth);
		
		//header que diz que queremos autenticar utilizando o HTTP Basic Auth
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);

		curl_setopt($curl, CURLOPT_USERPWD, $auth);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0");
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($curl);
		$result = simplexml_load_string($result);
		$response_status = $result->RespostaVerificarConta->Status;

		if ($response_status == 'Inesistente') {
				return false;
		} else if($response_status == 'Criado'){
			return true;
		}
	}	

	public function get_cep($user_id)
	{
		# code...
	}
}