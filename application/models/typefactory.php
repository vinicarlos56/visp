<?php 

interface TypeFactory{
	public function make($id,$name);
	public function get_name($id);
}