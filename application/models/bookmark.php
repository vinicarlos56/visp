<?php 

class Bookmark extends Base_Model{
	public static $timestamps = true;

	public function add($user_id,$product_id){
		$this->user_id = $user_id;
		$this->product_id = $product_id;
		return $this->save();
	}

	public function remove($user_id,$product_id){
		return $this->where('user_id','=',$user_id)
			 ->where('product_id','=',$product_id)
			 ->delete();

	}

}