<?php

class Base_Model extends Eloquent{

	public static $validation_rules    = array();
	public static $validation_errors   = array();
	public static $validation_messages = array();

	// TODO: Remover a cláusula unique dado um model_id para um update

	public static function validate($input_form,$models){		
		if(empty($input_form) OR empty($models)) return false;
		$errors = array();
		$input_data = self::extract($input_form,$models);
		
		foreach($models as $model_name){
			$model   = ucfirst($model_name);

			$attempt = Validator::make(
				$input_data[$model_name],
				$model::$validation_rules,
				$model::$validation_messages
			);

			if($attempt->fails())
				foreach ($attempt->errors->all() as $erro)
					array_push($errors,$erro);
		}
		self::$validation_errors = $errors;
		return empty($errors);
	}

	public static function extract($form_data,$models){
		if(empty($form_data) OR empty($models)) return;

		$separeted = array();

		foreach ($models as $model) {
			$separeted[$model] = array_map(function($key,$value) use ($model){				
				if(preg_match('/^'.$model.'_/', $key))
					return array(str_replace($model.'_', '', $key)=>$value);				
			},array_keys($form_data), $form_data);

			foreach ($separeted[$model] as $index => $val){
				if(!empty($val)){
					foreach ($val as $finalkey => $finalvalue) {
						$separeted[$model][$finalkey] = $finalvalue;
					}
				}
				unset($separeted[$model][$index]);			
			}
		}
		return $separeted;
	}

	public static function errors_to_string(){		
		return implode('<br>',static::$validation_errors);
	}

}