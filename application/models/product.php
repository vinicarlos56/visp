<?php 

class Product extends Base_Model{

	private $categoryFactory;
	private $subcategoryFactory;	

	public static $timestamps = true;

	public static $validation_rules = array(
		'title'      	=> 'required', //ok
		'description'	=> 'required', //ok
		'category_id'   => 'required', //ok
		'size'			=> 'required', //ok // FIXME: Remover size de dentro do produto e colocar em uma categoria específica
		'color'			=> 'required', //ok // FIXME: Remover size de dentro do produto e colocar em uma categoria específica
		'mark'			=> 'required', //ok // FIXME: Remover size de dentro do produto e colocar em uma categoria específica
		'subcategory_id'=> 'required', //ok
		'weight'  		=> 'required', //ok radio (text) TODO: Ajustar tabela
		'origin' 		=> 'required', //ok radio (text)
		'condition'		=> 'required', //ok radio (text)
		'original_price'=> 'required', //ok
		//'price'			=> 'required', //ok
		'amount'		=> 'required', //ok
		'user_id'		=> 'required'  //ok		
	);

	public function teste()
	{
		# code...
	}

	public function save_product($input,$product_id = false){		

		$product_data = array(
			'title'      	=> $input['product_title'],
			'description'   => $input['product_description'],
			'category_id'   => $input['product_category_id'],
			'size'			=> $input['product_size'],
			'color'			=> $input['product_color'],
			'mark'			=> $input['product_mark'],
			'subcategory_id'=> $input['product_subcategory_id'],
			'weight'      	=> $input['product_weight'],
			'combine'		=> isset($input['combinar']) ? $input['combinar'] : '',
			'origin'      	=> $input['product_origin'],
			'condition'     => $input['product_condition'],
			'original_price'=> Currency::format_price($input['product_original_price']),
			'price'      	=> $this->price($input['product_original_price']),
			'amount'      	=> $input['product_amount'],
			'user_id'      	=> $input['product_user_id'],
			'status'		=> 0,
			'url_title'		=> self::generate_url(array($input['product_title'])),
		);

		if($product_id){
			$product    = Product::find($product_id);			
			
			if($product::update($product_id,$product_data))
				return true;			
		}else{			
			return Product::create($product_data);
		}
	}	

	public function price($price){
		return (string) ceil(Currency::format_price($price) + (Currency::format_price($price) * 0.15));
	}

	public function general_search($input){
		
	}

	public function user(){
		return $this->belongs_to('User');
	}

	public function category(){		
		return $this->categoryFactory
					->make($this->category_id);
	}

	public function subcategory(){
		return $this->subcategoryFactory
					->make($this->subcategory_id);
	}	

	public function set_category_factory(TypeFactory $category){		
		$this->categoryFactory = $category;
	}

	public function set_subcategory_factory(TypeFactory $subcategory){
		$this->subcategoryFactory = $subcategory;
	}

	public function photos(){
		return $this->has_many('Photo','model_id');
	}

	public function bookmarked(){
		return $this->has_many_and_belongs_to('User','bookmarks');
	}

	public function get_others(){
		return $this->where('user_id','=',$this->user->id)->where('id','<>',$this->id)->get();
	}

	public function condition()
	{
		if($this->condition == 'usado')
			return 'Usado';
		else if ($this->condition == 'nuncausado')
			return 'Nunca Usado';

	}

	public function title()
	{
		if (!empty($this->url_title)) {
			return $this->url_title;
		} else {
			$url_title = self::generate_url(array($this->title));
			$this->url_title = $url_title;
			$this->save();
			return $this->url_title;
		}
	}

	public static function generate_url($params = array()){
        if(empty($params)) return;        

        $params = array_filter($params,
            function($param) use ($params){ 
                if(!empty($param))
                    return $param;                    
            });

        $params = array_values($params);
        $params = array_map(function($param,$key){        

          return strtolower($param);

        },$params,array_keys($params));

        $params = utf8_decode(join('-',$params));
        $params = str_replace(' ', '-', $params);

        $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $b = 'AAAAAAACEEEEIIIIDNOOOOOOUUUUYobsaaaaaaaceeeeiiiidnoooooouuuyybyRr';

        $params = strtr($params, utf8_decode($a), $b);

        return utf8_encode($params);
    }

    public static function load_by_user(ShoppingCart $cart,$user_id)
    {
    	$products = $cart->get_products_by_user($user_id);

    	foreach ($products as $id) {
    		$products_array[] = self::find($id);
    	}

    	return $products_array;
    }

}