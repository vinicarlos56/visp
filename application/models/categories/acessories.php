<?php namespace Categories; use Eloquent;


class Acessories  extends Eloquent implements Category{

	public static $timestamps = true;
	public static $table = 'acessories_category';
	private $name = 'Acessórios';

	public function get_css_class(){
		
	}

	public function get_name(){
		return $this->name;	
	}

}