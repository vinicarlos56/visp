<?php namespace Categories; use Eloquent;


class Outfit extends Eloquent implements Category{

	public static $timestamps = true;
	public static $table = 'outfit_category';
	private $name = 'Enxoval';

	public function get_css_class(){
		
	}

	public function get_name(){
		return $this->name;	
	}

}