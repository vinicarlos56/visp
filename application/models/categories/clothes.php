<?php namespace Categories; use Eloquent;

Class Clothes extends Eloquent implements Category{
	public static $timestamps = true;
	public static $table = 'clothes_category';
	private $name = 'Roupas';

	public function get_css_class(){
		
	}

	public function get_name(){
		return $this->name;	
	}

}