<?php namespace Categories; use Eloquent;


class Babystroller extends Eloquent implements Category{

	public static $timestamps = true;
	public static $table = 'babystroller_category';
	private $name = 'Cadeiras e Carrinhos';

	public function get_css_class(){
		
	}

	public function get_name(){
		return $this->name;	
	}

}