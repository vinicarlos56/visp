<?php namespace Categories; use Eloquent;

Class Shoes extends Eloquent implements Category{
	public static $timestamps = true;
	public static $table = 'shoes_category';
	private $name = 'Sapatos';

	public function get_css_class(){
		
	}

	public function get_name(){
		return $this->name;	
	}

}