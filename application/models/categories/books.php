<?php namespace Categories; use Eloquent;


class Books extends Eloquent implements Category{

	public static $timestamps = true;
	public static $table = 'books_category';
	private $name = 'Livros e DVDs';

	public function get_css_class(){
		
	}

	public function get_name(){
		return $this->name;	
	}

}