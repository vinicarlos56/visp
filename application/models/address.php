<?php 

class Address extends Base_Model
{
	public static $timestamps = true;

	// TODO: Ajustar o registro de cidades
	public static $validation_rules = array(
		'cep'          => 'required',
		'logradouro'   => 'required',			
		//'city'         => 'required',
		'neighborhood' => 'required',
		'complement'   => 'required',
		'number'	   => 'required'		
	);

	public function user(){
		return $this->belongs_to('User');
	}	
}