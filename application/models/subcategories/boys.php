<?php namespace Subcategories; use Eloquent;

class Boys extends Eloquent implements Subcategory{
	
	public static $timestamps = true;
	public static $table = 'boys';
	private $name = 'Meninos';	

	public function get_css_class(){
		return 'menino';
	}

	public function get_name(){
		return $this->name;	
	}
	

}