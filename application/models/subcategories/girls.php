<?php namespace Subcategories; use Eloquent;

class Girls extends Eloquent implements Subcategory{
	
	public static $timestamps = true;
	public static $table = 'girls';
	private $name = 'Meninas';	

	public function get_css_class(){
		return 'menina';
	}

	public function get_name(){
		return $this->name;	
	}
	

}