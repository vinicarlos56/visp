<?php namespace Subcategories; use Eloquent;

class Gemini extends Eloquent implements Subcategory{
	
	public static $timestamps = true;
	public static $table = 'gemini';
	private $name = 'Gêmeos';	

	public function get_css_class(){
		return 'gemeos';
	}

	public function get_name(){
		return $this->name;	
	}
	

}