<?php namespace Subcategories; use Eloquent;

class Unisex extends Eloquent implements Subcategory{
	
	public static $timestamps = true;
	public static $table = 'unisex';
	private $name = 'Unissex';	

	public function get_css_class(){
		return 'unisses';
	}

	public function get_name(){
		return $this->name;	
	}
	

}