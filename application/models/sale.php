<?php 

Class Sale extends Eloquent{

	public static $timestamps = true;	
	private $moip;
	private $credential = array(
	    'key' => '8P2DOXWPWY9GXXKQO3AFO5CMMPJFAIRG6B20VJA2',
	    'token' => 'QRDEXKGBCP6IDVL1D7K7QSK3HVPJVOLC'
	);	
	private $shoppingcart;
	private $transport_prices_list = array();

	public function __construct() {
		$this->moip 	= new Moip();
	}

	public function start_transaction($data,$msg = '')
	{
		$this->moip->setEnvironment('test');
		$this->moip->setCredential($this->credential);

		$this->moip->setUniqueID(uniqid()); // COLOCAR O ID DA VENDA $this->id
		$this->moip->setValue($this->total_price); // COLOCAR O VALOR DO PRODUTO  X  QUANTIDADE

		$this->moip->addPaymentWay('creditCard');
		$this->moip->addPaymentWay('billet');
		$this->moip->addPaymentWay('debit');
		// TODO: Colocar informações do boleto aqui
		$this->moip->setBilletConf("2011-04-06",false,array("Primeira linha","Segunda linha","Terceira linha"),"http://seusite.com.br/logo.gif");
		$this->moip->setReason('Compra realizada na ViSP');
		$this->moip->addMessage("Pagamento referente aos produtos: {$msg}");
		$buyer = User::find($this->buyer_id);
		$this->moip->setPayer( // COLOCAR AS INFORMAÇÕES DO BUYER
			array(
				'name' 		=> $data['cobranca_nome'],
		        'email' 	=> $buyer->email,
		        'payerId' 	=> 'id_usuario',
		        'billingAddress' => array(
		        	'address' 		=> $data['cobranca_endereco_nome'],
			        'number' 		=> $data['cobranca_endereco_numero'],
			        'complement' 	=> $data['cobranca_endereco_complemento'],
			        'city' 			=> $data['cobranca_endereco_cidade'],
			        'neighborhood' 	=> $data['entrega_endereco_bairro'],
			        'state' 		=> $data['cobranca_endereco_uf'],
			        'country' 		=> 'BRA',
			        'zipCode' 		=> $data['entrega_endereco_cep'],
			        'phone' 		=> $data['cobranca_telefone']
			    )
			)
		);

		$this->moip->validate('Identification'); // Validação necessária para gerar o checkout transparente

		if($this->moip->send()){
			$response = $this->moip->getAnswer();
			$this->token = $response->token;			
			$this->update($this->id,array('token'=>$response->token));
			return $response;
		}else{
			return false;
		}
	}

	public function finish($sale_id,$sale_data){
		# code...
	}

	public function orders(){
		return $this->has_many('Order');
	}	
	
}