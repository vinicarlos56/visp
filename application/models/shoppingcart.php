<?php 

Class ShoppingCart
{
	private $cart = array();
	private $session;

	public function __construct() 
	{
		$this->cart = Session::get('shoppingcart');		
	}

	public function get_cart()
	{
		return $this->cart;
	}	

	public function add_product(Product $product) 
	{
		if (empty($this->cart)) {
			$this->cart = array(
				'users'=>array(
					$product->user_id => array(
						'products'=> array($product->id)
					)
				)
			);

		} else {
			if (isset($cart['users'][$product->user_id]) AND 
				in_array($product->id, $cart['users'][$product->user_id]['products'])) return;

			$this->cart['users'][$product->user_id]['products'][] = $product->id;
		}

		Session::put('shoppingcart',$this->cart);
	}


    public function get_products()
	{
		if (empty($this->cart)) return;

		foreach ($this->cart['users'] as $seller_id => $content) {
			foreach ($content['products'] as $product_id) {
				$products[] = $product_id;
			}
		}

		return array_values($products);
	}

	public function get_sellers()
	{
		if (empty($this->cart)) return;

		return array_keys($this->cart['users']);
	}

	public function remove_product(Product $product)
	{
		$products_ids  = array_flip($this->cart['users'][$product->user_id]['products']);

		unset($products_ids[$product->id]);

		$refreshed = array_flip($products_ids);

		$this->cart['users'][$product->user_id]['products'] = array_values($refreshed);

		if (empty($this->cart['users'][$product->user_id]['products']))
			unset($this->cart['users'][$product->user_id]);
			
		if(empty($this->cart['users']))
			$this->cart = array();

		Session::put('shoppingcart',$this->cart);		
	}

	public function count_products()
	{
		return count($this->get_products());
	}

	public function get_products_by_user($id)
	{
		if (empty($id)) return;

		foreach ($this->cart['users'] as $user_id => $content) {
			if ($user_id == $id) {
				return $content['products'];
			}
		}
	}

}