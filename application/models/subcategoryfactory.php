<?php 

Class SubcategoryFactory implements TypeFactory{

	public function make($id,$name = false){
		if(empty($id) AND !$name) return;

		if(!$name)
			$name = 'Subcategories\\'.ucfirst($this->get_name($id));
		else
			$name = 'Subcategories\\'.ucfirst($name);

		if(empty($name))
			throw new Exception("This Subcategory doesn't exists.");
			
		return new $name;
	}

	public function get_name($id){
		return DB::table('subcategories')
				   ->where('id', '=', $id)
				   ->only('model_name');
	}
}