<?php

namespace Services;

use Services\Correios;

Class Sale
{

	private $transport_prices_list;

	public function set_correios(Correios $correios_service)
	{
		$this->correios_service = $correios_service;
	}

	public function calculate_total_transport_price($buyer_cep)
	{
		if (empty($buyer_cep)) return;

		$cart  = \Session::get('shoppingcart');
		$total = 0;
		foreach ($cart['users'] as $user_id => $content) {

			$seller_cep = \User::find($user_id)->address->cep;

			foreach ($content['products'] as $product_id) {
				$product 		 = \Product::find($product_id);				
				$transport_price = $this->correios_service
					->calculate_transport($buyer_cep,$seller_cep,$product);

				$total += $transport_price;
				$this->add_to_transport_prices_list($user_id,$transport_price); 
			}
		}
		return $total;
	}

	private function add_to_transport_prices_list($user_id,$transport_price)
	{
		if (!isset($this->transport_prices_list[$user_id])) {
			$this->transport_prices_list[$user_id] = $transport_price;	
		} else {
			$this->transport_prices_list[$user_id] += $transport_price;
		}
	}

	public function get_transport_prices_list()
	{
		return $this->transport_prices_list;
	}

	public static function get_total_selling_price(\ShoppingCart $cart)
	{
		$count = 0;
		$cart  = $cart->get_products();

		if (is_array($cart)) {			
			foreach ($cart as $product_id) {
				$count += \Product::find($product_id)->price;
			}			
		}		
		return ceil($count);
	}

	public static function get_price_by_seller($seller_id)
	{
		$cart = \Session::get('shoppingcart');

		if (empty($seller_id) or empty($cart)) return;

		$count = 0;
		foreach ($cart['users'] as $user_id => $content) {
				
			if ($user_id == $seller_id)	{
				foreach ($content['products'] as $product_id) {
					$count += \Product::find($product_id)->price;
				}				
			}
		}

		return $count;
	}

	// public static function process($cart,$amounts)
	// {
	// 	$count = 0;
	// 	// FIXME: rodar esse foreach só uma vez
	// 	foreach ($cart['users'] as $user_id => $content) {
	// 		foreach ($content['products'] as $product_id) {
	// 			$product_price = (float) Product::find($product_id)->price;				
	// 			$count += ( (float) $amounts['products_amount'][$product_id] * $product_price); // FIXME: Refatorar
	// 		}
	// 	}

	// 	return $count;
	// }



}