<?php

namespace Services;

Class Correios{

	public function calculate_transport($buyer_cep,$seller_cep,$product)
	{
		return 10;
		
		$cep_origem  = str_replace('-','',$seller_cep);
		$cep_destino = str_replace('-','',$buyer_cep);	
		
		// 40010 SEDEX sem contrato.
		// 40045 SEDEX a Cobrar, sem contrato.
		// 40126 SEDEX a Cobrar, com contrato.
		// 40215 SEDEX 10, sem contrato.
		// 40290 SEDEX Hoje, sem contrato.
		// 40096 SEDEX com contrato.
		// 40436 SEDEX com contrato.
		// 40444 SEDEX com contrato.
		// 40568 SEDEX com contrato.
		// 40606 SEDEX com contrato.
		// 41106 PAC sem contrato.
		// 41068 PAC com contrato.
		// 81019 e-SEDEX, com contrato.
		// 81027 e-SEDEX Prioritário, com conrato.
		// 81035 e-SEDEX Express, com contrato.
		// 81868 (Grupo 1) e-SEDEX, com contrato.
		// 81833 (Grupo 2) e-SEDEX, com contrato.
		// 81850 (Grupo 3) e-SEDEX, com contrato.

		// $data['nCdEmpresa'] = '';
		// $data['sDsSenha'] = '';
		$data['sCepOrigem'] = $cep_origem;
		$data['sCepDestino'] = $cep_destino;
		$data['nVlPeso'] = '1'; // em quilogramas
		$data['nCdFormato'] = '1'; //1 para caixa / pacote e 2 para rolo/prisma.
		$data['nVlComprimento'] = '16'; //comprimento, altura, largura e diametro deverá ser informado em centímetros e somente números
		$data['nVlAltura'] = '50';// centímetros
		$data['nVlLargura'] = '15';// centímetros
		$data['nVlDiametro'] = '0';// centímetros
		$data['sCdMaoPropria'] = 'n'; // com confirmação de rg na entrega
		$data['nVlValorDeclarado'] = '0'; // valor declarado da encomenda para possível recuperação (interfere no valor do frete)
		$data['sCdAvisoRecebimento'] = 'n';
		$data['StrRetorno'] = 'xml';
		$data['nCdServico'] = '41106';
		// $data['nCdServico'] = '40010,40045,40215,40290,41106';
		$data = http_build_query($data);

		$url  = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx';
		$curl = curl_init($url . '?' . $data);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($curl);
		$result = simplexml_load_string($result);		
		$result = $result->cServico;
		// foreach($result->cServico as $row) {
		    //Os dados de cada serviço estará aqui
		
		 
	    if($result->Erro == 0) {
	        $re['codigo'] = $result->Codigo;
	        $re['valor'] = $result->Valor;
	        $re['prazo_de_entrega']= $result->PrazoEntrega;
	        $re['valor_mao_propria']= $result->ValorMaoPropria;
	        $re['valor_aviso_recebimento']=$result->ValorAvisoRecebimento;
	        $re['valor_declarado']=$result->ValorValorDeclarado;
	        $re['entrega_domiciliar']=$result->EntregaDomiciliar;
	        $re['entrega_sabado']=$result->EntregaSabado;
	    } else {
	        $re['erro'] = $result->MsgErro;
	    }

		return Currency::format_price($re['valor']);
	}

	// public function calculate_transport($product_cep,$user_cep,$product_data = array())
	// {
	// 	$cep_origem  = str_replace('-','',$product_cep);
	// 	$cep_destino = str_replace('-','',$user_cep);	
		
	// 	// 40010 SEDEX sem contrato.
	// 	// 40045 SEDEX a Cobrar, sem contrato.
	// 	// 40126 SEDEX a Cobrar, com contrato.
	// 	// 40215 SEDEX 10, sem contrato.
	// 	// 40290 SEDEX Hoje, sem contrato.
	// 	// 40096 SEDEX com contrato.
	// 	// 40436 SEDEX com contrato.
	// 	// 40444 SEDEX com contrato.
	// 	// 40568 SEDEX com contrato.
	// 	// 40606 SEDEX com contrato.
	// 	// 41106 PAC sem contrato.
	// 	// 41068 PAC com contrato.
	// 	// 81019 e-SEDEX, com contrato.
	// 	// 81027 e-SEDEX Prioritário, com conrato.
	// 	// 81035 e-SEDEX Express, com contrato.
	// 	// 81868 (Grupo 1) e-SEDEX, com contrato.
	// 	// 81833 (Grupo 2) e-SEDEX, com contrato.
	// 	// 81850 (Grupo 3) e-SEDEX, com contrato.

	// 	// $data['nCdEmpresa'] = '';
	// 	// $data['sDsSenha'] = '';
	// 	$data['sCepOrigem'] = $cep_origem;
	// 	$data['sCepDestino'] = $cep_destino;
	// 	$data['nVlPeso'] = '1'; // em quilogramas
	// 	$data['nCdFormato'] = '1'; //1 para caixa / pacote e 2 para rolo/prisma.
	// 	$data['nVlComprimento'] = '16'; //comprimento, altura, largura e diametro deverá ser informado em centímetros e somente números
	// 	$data['nVlAltura'] = '50';// centímetros
	// 	$data['nVlLargura'] = '15';// centímetros
	// 	$data['nVlDiametro'] = '0';// centímetros
	// 	$data['sCdMaoPropria'] = 'n'; // com confirmação de rg na entrega
	// 	$data['nVlValorDeclarado'] = '0'; // valor declarado da encomenda para possível recuperação (interfere no valor do frete)
	// 	$data['sCdAvisoRecebimento'] = 'n';
	// 	$data['StrRetorno'] = 'xml';
	// 	$data['nCdServico'] = '41106';
	// 	// $data['nCdServico'] = '40010,40045,40215,40290,41106';
	// 	$data = http_build_query($data);

	// 	$url  = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx';
	// 	$curl = curl_init($url . '?' . $data);

	// 	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

	// 	$result = curl_exec($curl);
	// 	$result = simplexml_load_string($result);		
	// 	$result = $result->cServico;
	// 	// foreach($result->cServico as $row) {
	// 	    //Os dados de cada serviço estará aqui
		
		 
	//     if($result->Erro == 0) {
	//         $re['codigo'] = $result->Codigo;
	//         $re['valor'] = $result->Valor;
	//         $re['prazo_de_entrega']= $result->PrazoEntrega;
	//         $re['valor_mao_propria']= $result->ValorMaoPropria;
	//         $re['valor_aviso_recebimento']=$result->ValorAvisoRecebimento;
	//         $re['valor_declarado']=$result->ValorValorDeclarado;
	//         $re['entrega_domiciliar']=$result->EntregaDomiciliar;
	//         $re['entrega_sabado']=$result->EntregaSabado;
	//     } else {
	//         $re['erro'] = $result->MsgErro;
	//     }

	// 	return Currency::format_price($re['valor']);
	// }
}