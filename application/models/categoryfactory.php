<?php 

Class CategoryFactory implements TypeFactory{

	public function make($id,$name=false){
		if(empty($id) AND !$name) return;

		if(!$name)
			$name = 'Categories\\'.ucfirst($this->get_name($id));
		else
			$name = 'Categories\\'.ucfirst($name);

		if(empty($name))
			throw new Exception("This Category doesn't exists.");
		
		return new $name;
	}

	public function get_name($id){
		return DB::table('categories')
				   ->where('id', '=', $id)
				   ->only('model_name');
	}
}