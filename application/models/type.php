<?php 

Class Type{
	
	public static function select_all_categories($default='Selecione'){
		$result = DB::table('categories')->get(array('id','name'));
		foreach ($result as $obj) {
			$retorno[$obj->id] = $obj->name;
		}
		return array(''=>$default)+$retorno;
	}

	public static function select_all_subcategories($default='Selecione'){
		$result = DB::table('subcategories')->get(array('id','name'));
		foreach ($result as $obj) {
			$retorno[$obj->id] = $obj->name;
		}
		return array(''=>$default)+$retorno;
	}
}