<?php 

Class Photo extends Base_Model{
	public static $timestamps = true;
	public static $image_path = 'public/uploads/images/';
	public static $thumb_path = 'public/uploads/thumbs/';
	private $image;
	private $file_name;
	const USER    = 1;
	const PRODUCT = 2;
	const BANNER  = 3;
	
	public function save_photo($data){	
		
		$this->image 	 = WideImage::loadFromUpload($data['input_name']);
		$this->file_name = $data['file_name'];		

		$saved = static::create(array(
			'title' 	 => $data['title'],
			'model_id' 	 => $data['model_id'],
			'type_id'	 => $data['type_id'],
			'file_name'  => $data['file_name']
		));

		if($saved){
			$this->image->saveToFile(self::$image_path.$data['file_name']);
			return $this;
		}else{
			return false;
		}
	}

	public function thumb($width,$height = false){
		if(!empty($this->image)){
			if(!$height) $height = $width;
			$resized = $this->image->resize($width,$height);
			$resized->saveToFile(self::$thumb_path."thumb_{$width}x{$height}_$this->file_name");
			return $this;
		}

	}
	
	public function user(){
		return $this->belongs_to('User');
	}

	public function banner(){
		return $this->belongs_to('Banner');
	}

	public function product(){
		return $this->belongs_to('Product');
	}
}