<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

  <meta http-equiv="content-language" content="pt-br" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="refresh" content="1080" />
  <meta http-equiv="Content-Script-Type" content="text/javascript" />
  <meta http-equiv="Content-Style-Type" content="text/css" />

  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

  <meta name="description" content="A VIsP é um marketplace que apoia o consumo colaborativo de produtos infantis! Venda e Compre todos os produtos para bebês e crianças. Brinquedos, Mamadeiras, Roupas, Sapatos, Cadeirinhas, Berços, etc. Cadastre-se na vispstore.com. É de graça!"/>
  <meta name="keywords" content="VIsP, vispstore, Bebê, Bebês, crianças, Mamãe, Brinquedos, Brinquedo, Carrinho, Cadeirinha, Bebe Conforto, Berço, Babá Eletrônica, roupas, acessórios, sapatinhos, consumo colaborativo, compra, venda"/>

  <title>VIsP | Compre e venda produtos infantis. Viva o consumo colaborativo!</title>

  <meta name="google-site-verification" content="rbYIDrAqExW0dKaoh6HYguSCBidDPeHGgKum2jzZobU" />

  <link rel="stylesheet" href="style.css" />

  <script type="text/javascript" src="jquery.min.js"></script>

</head>

<body>

<div id="container">

  <div class="top"></div>

  <div id="centro">

    <h1><img src="http://200.143.178.146/~visps182/html/images/logo-visp.png" alt="Logo VIsP" title="Logo VIsP" /></h1>

    <div class="frase"><img alt="Já já uma super novidade para a pessoa mais importante do mundo." title="Já já uma super novidade para a pessoa mais importante do mundo." src="frase.png" /></div>

    <!-- NEWSLETTER -->
    <div class="newsletter">
      <form class="news" action="#" onsubmit="enviaFormContato(); return false;">
        <input type="text" name="email" class="email" id="email" placeholder="Digite seu email" />
        <input type="submit" name="url" class="assinar" />
        <div id="repostaFormContato"></div>
      </form>
    </div><!-- FIM NEWSLETTER -->

  </div>

</div>

<script type="text/javascript">
  $('#container').animate({ opacity: 0 }, 0);    
  $('#container').animate({ opacity: 1 }, 2000);   

  function enviaFormContato()
   {
      $('#repostaFormContato').html("Aguarde...");

      email = $( '#email' ).val();   

      $.ajax({
        type: 'POST',
        //url: $(this).attr( 'href' ),
        url: "sendmail/sendMail.php",
        data: { email: email },
        success: function( results ) {    
            $('#repostaFormContato').html( results );
        }
      }); 
   }   

   //GA UA-37404326-1
   var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-37404326-1']);
    _gaq.push(['_trackPageview']);
    (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>

</body>
</html>