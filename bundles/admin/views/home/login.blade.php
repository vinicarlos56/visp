@layout('admin::layout.main')
<!-- TODO: remover esse layout e deixar somente no controller -->
@section('content')
		@if (Session::has('login_errors'))
			<div class="error">
	        	<span>O campo email ou senha estão incorretos!</span>
	        </div>
   		@endif

			<h2>Faça o login</h2>
			{{Form::open('admin/login', 'POST')}}
				{{Form::token()}}
				{{Form::label('login','Login')}}
				{{Form::input('text', 'login')}}<br>
				{{Form::label('password','Senha')}}
				{{Form::input('password', 'senha')}}<br>
				{{Form::submit('Enviar')}}
			{{Form::close()}}
	</div>
@endsection