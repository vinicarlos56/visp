@section('content')
	<h2>Gerenciar produtos</h2>
	<script>
		$(document).ready(function(){
			$('.releaseproduct').on('click',function(e){				
				e.preventDefault();

				var product_id = $(this).attr('data-product-id');
				var li = $(this).parent();
				$.ajax({
					url:'/visp/public/admin/product/'+product_id+'/release',
					type:'POST',
					dataType:'json',					
					success: function(json){
						if(json.status){
							li.remove();
							alert('Produto liberado com sucesso!');
						}else{
							alert(json.errors);
						}
					},
					error: function(error){
						console.log(error);
					}
				});
			});
		});
	</script>
	@if(!empty($products))
		<ul>
		@foreach($products as $product)
			<li><a href="#" data-product-id="{{$product->id}}" class="releaseproduct">Liberar</a> - {{$product->title}}</li>
		@endforeach
		</ul>
	@else
		<p>Nenhum produto novo adicionado</p>
	@endif

@endsection