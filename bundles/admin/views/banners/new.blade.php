
@section('content')
	<h2>Adicionar banner</h2>
	{{Form::open_for_files('admin/banners','POST')}}
		{{Form::token()}}
		
		{{Form::label('title','Título')}}
   		{{Form::input('text','title')}}<br>

   		{{Form::label('alt','Alt')}}
   		{{Form::input('text','alt')}}<br>

   		{{Form::label('type','Tipo')}}
   		{{Form::select('type', array('slider'=>'Slider','side'=>'Lateral'))}}<br>

		{{Form::label('image','Foto')}}
		{{Form::file('image')}}<br>
		<div class="form-actions">
			{{Form::submit('Enviar',array('class'=>'btn btn-primary'))}}				
		</div>
	{{Form::close()}}
@endsection
