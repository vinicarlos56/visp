
@section('content')
	<i class="icon-plus"></i>{{HTML::link_to_route('new_banner', 'Novo Banner')}}

	@if(Session::has('success'))
		<div class="success">
			<span>{{Session::get('success')}}</span>
		</div>
	@elseif(Session::has('errors'))
		<div class="error">
			<span>{{Session::get('errors')}}</span>
		</div>
	@endif

	@foreach($banners as $banner)		
		<p>
			<b>
				{{$banner->title}} - {{$banner->type}} - 
				{{HTML::link("admin/banners/{$banner->id}/edit", 'Editar')}} - 
				{{HTML::link("admin/banner/{$banner->id}/delete", 'Deletar')}}
			</b>

			@if($banner->type == 'slider')
				<img src="{{URL::to("uploads/thumbs/thumb_960x305_{$banner->photo->file_name}")}}">
			@elseif($banner->type == 'side')
				<img src="{{URL::to("uploads/thumbs/thumb_250x90_{$banner->photo->file_name}")}}">
			@endif
		</p>
	@endforeach
@endsection
