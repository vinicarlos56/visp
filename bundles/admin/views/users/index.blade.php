@section('content')
	<h2>Gerenciar usuários</h2>
	
	@if(!empty($users))
		<table class="table table-bordered">
		  <thead>
		    <tr>
		      <th>Nome</th>
		      <th>Email</th>
		      <th>Facebook</th>
		      <th>Ações</th>
		    </tr>
		  </thead>
		  <tbody>
			@foreach($users as $user)
			    <tr>
			      <td>{{$user->name}}</td>
			      <td>{{$user->email}}</td>
			      <td>{{$user->fb_id?'Sim':'Não'}}</td>
			      <td>{{HTML::link_to_route('admin_edit_user', 'Editar', array($user->id))}}</td>
			    </tr>
			@endforeach
		  </tbody>
		</table>	
	@endif

@endsection