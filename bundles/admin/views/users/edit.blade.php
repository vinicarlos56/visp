@section('content')
	
	{{Form::open('admin/users/update', 'PUT',array('class'=>'form form-horizontal'))}}
		{{Form::token()}}
		<div class="control-group">
			{{Form::label('name','Nome',array('class'=>'control-label'))}}
			<div class="controls">				
				{{Form::input('text','user_name',$user->name)}}			
			</div>
		</div>

		<div class="control-group">
			{{Form::label('email','Email',array('class'=>'control-label'))}}
			<div class="controls">				
				{{Form::input('text','user_email',$user->email)}}			
			</div>
		</div>

		<div class="control-group">
			{{Form::label('email_moip','Email do Moip',array('class'=>'control-label'))}}
			<div class="controls">				
				{{Form::input('text','user_email_moip',$user->email_moip)}}			
			</div>
		</div>

		<div class="control-group">
			{{Form::label('password','Nova senha',array('class'=>'control-label'))}}
			<div class="controls">				
				{{Form::input('password','user_password')}}			
			</div>
		</div>

		<div class="control-group">
			{{Form::label('password_confirmation','Confirmar nova senha',array('class'=>'control-label'))}}
			<div class="controls">				
				{{Form::input('password','user_password_confirmation')}}			
			</div>
		</div>

		<div class="form-actions">
			{{Form::hidden('user_id',$user->id)}}
			{{Form::input('submit','Salvar',null,array('class'=>'btn btn-primary'))}}
		</div>
	{{Form::close()}}

@endsection