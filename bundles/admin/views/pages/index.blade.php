@section('content')
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Página</th>
				<th>Editar</th>
			</tr>
		</thead>
		<tbody>
			@foreach($pages as $page)
				<tr>
					<td>{{$page->title}}</td>
					<td>{{HTML::link_to_route('edit-page', 'Editar', array($page->id),array('class'=>'btn'))}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection