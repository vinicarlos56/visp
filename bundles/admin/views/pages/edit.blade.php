@section('content')
	{{HTML::script('bundles/admin/ckeditor/ckeditor.js')}}
	<div class="form-horizontal">
		{{Form::open('admin/page', 'PUT')}}
			{{Form::token()}}
			<div class="control-group">
				{{Form::label('title', 'Título: '.$page->title)}}
				<div class="">
					{{Form::textarea('content', $page->content, array('class'=>'','cols'=>'20','rows'=>'6'))}}
					<script>
		                CKEDITOR.replace( 'content' );
		            </script>
				</div>
				<div class="form-actions">
					{{Form::hidden('page_id', $page->id)}}
					{{Form::submit('Salvar',array('class'=>'btn btn-primary pull-right',))}}
				</div>				
			</div>
		{{Form::close()}}		
	</div>
@endsection