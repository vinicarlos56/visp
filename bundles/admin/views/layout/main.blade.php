<!doctype html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">	
	<title>VISP</title>
	{{HTML::style('bundles/admin/bootstrap/css/bootstrap-spacelab.min.css')}}
	{{HTML::style('bundles/admin/assets/css/admin_estilo.css')}}
	{{HTML::script('assets/lib/js/jquery-1.8.2.min.js')}}

</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
		@section('navigation')
			<a class="brand" href="{{URL::base().'admin';}}">ViSP-Administração</a>
			<ul class="nav">				
				@if (Authority::can('manage','User'))
					<!-- <li><a href="{{URL::to('admin')}}">{{Auth::user()->name;}}</a></li> -->
					<li>{{HTML::link_to_route('banners', 'Gerenciar Banners')}}</li>		
					<li>{{HTML::link_to_route('gerenciar-paginas', 'Gerenciar Páginas')}}</li>
					<li>{{HTML::link('admin/produtos', 'Produtos')}}</li>
					<li>{{HTML::link('admin/usuarios', 'Usuários')}}</li>
					<li class="pull-right">{{HTML::link_to_route('admin_logout', 'Logout')}}</li>
				@else
					<li>{{HTML::link_to_route('admin_login','Login')}}</li>
				@endif				
			<ul>
		@yield_section
		</div>	
	</div>	

	<div class="container" style="margin-top:60px">	

		<div class="row">
			@if(isset($sidebar))
				<div class="span3 well well-small sidebar">

					<ul class="nav nav-list">					
						@yield('sidebar')
					</ul>
				</div>
			@endif

			<div class="span7 well">
				@if (Session::has('errors'))
					<div class="alert alert-error">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
			    		<span>{{Session::get('errors')}}</span>
					</div>
				@elseif(Session::has('success'))
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
			    		<span>{{Session::get('success')}}</span>
					</div>
				@endif

				@yield('content')
			</div>
		</div>
	</div>	
	
</body>
</html>
