@section('content')
	<h2>Gerenciar produtos</h2>
	
	@if(!empty($products))
		<table class="table table-bordered">
		  <thead>
		    <tr>
		      <th>Título</th>
		      <th>Descrição</th>
		      <th>Ações</th>
		    </tr>
		  </thead>
		  <tbody>
			@foreach($products as $product)
			    <tr>
			      <td>{{$product->title}}</td>
			      <td>{{$product->description}}</td>
			      <td>{{HTML::link_to_route('view_product', 'Visualizar', array($product->title()), array('target'=>'_blank'))}}</td>
			    </tr>
			@endforeach
		  </tbody>
		</table>		
	@else
		<p>Nenhum produto novo adicionado</p>
	@endif

@endsection