<?php

class Admin_Create_Pages_Table_And_Insert_Them {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function($table){
			$table->increments('id');
			$table->string('title');
			$table->text('content');
			$table->timestamps();
		});

		DB::table('pages')->insert(array('title'=>'Como Funciona','content'=>''));
		DB::table('pages')->insert(array('title'=>'Quero Vender','content'=>''));
		DB::table('pages')->insert(array('title'=>'Quero Comprar','content'=>''));
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}