<?php 

class Admin_Banners_Controller extends Admin_Base_Controller
{
	public $restful = true;

	public function get_index(){
		return $this->layout->nest('content','admin::banners.index',array(				
				'banners'=>Banner::all()
			)
		);
	}

	public function get_new(){
		return $this->layout->nest('content','admin::banners.new');
	}

	public function post_create(){
		$photo 		= new Photo;		
		$type 		= Input::get('type');
		$type_image = Input::file('image.type');

		if($type_image == 'image/jpeg' OR $type_image != 'image/jpg'){

			if($type == 'side'){
				$image_width  = 250;
				$image_height = 90;
			}elseif($type == 'slider'){
				$image_width  = 960;
				$image_height = 305;
			}

			$banner = Banner::create(array(
				'title' 	=> Input::get('title'),
				'type'		=> $type,
				'status'	=> 0				
			));

			$photo->save_photo(array(
				'input_name'=> 'image',
				'title'		=> 'title',//Input::get('title'),
				'alt'		=> 'title',//Input::get('title'), // TODO: Replace this in the future
				'file_name'	=> Input::file('image.name'),
				'type_id'	=> Photo::BANNER,
				'model_id'	=> $banner->id		
			))->thumb($image_width,$image_height);

			return Redirect::to('admin/banners')->with('success','Banner adicionado com sucesso!');
		}else{

		}
	}

	// public function delete_index($banner_id){
		
	// }

	/**
	 * FIXME: Utilizar o método restful delete
	*/

	public function get_destroy($banner_id){
		$banner = Banner::find($banner_id);	

		if($banner->type == 'slider')
			$path = "public/uploads/thumbs/thumb_960x305_{$banner->photo->file_name}";
		elseif($banner->type == 'side')
			$path = "public/uploads/thumbs/thumb_250x90_{$banner->photo->file_name}";
		
		if(
			(is_file("public/uploads/images/{$banner->photo->file_name}") AND
			unlink("public/uploads/images/{$banner->photo->file_name}")) OR
			(is_file($path) AND unlink($path))
		)$banner->photo->delete();
		else
			exit(); // FIXME: Tratar erro ao deletar banner		
		

		if($banner->delete())
			return Redirect::to_route('banners')->with('success','Banner deletado com sucesso!');
		else
			return Redirect::to_route('banners')->with('errors','Banner não foi deletado!');
	}
}