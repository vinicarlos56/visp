<?php 

class Admin_Users_Controller extends Admin_Base_Controller
{
	public $restful = true;

	public function get_index()
	{
		$users = User::all();
		return $this->layout->nest('content','admin::users.index',array('users'=>$users));
	}

	public function get_edit($user_id)
	{
		$user = User::find($user_id);
		return $this->layout->nest('content','admin::users.edit',array('user'=>$user));
	}

	public function put_update()
	{
		$input 		= Input::all();

		$rules = array(		
			'id'        => 'required', 
			'name'      => 'required',
			'email'      => 'required|email',
			'email_moip' => 'required|email',
			'password'   =>'required|confirmed',
		);

		if (empty($input['passord'])) {
			unset($rules['password']);
		}
		
		User::$validation_rules = $rules;

		$validation = User::validate($input,array('user'));		
		$user_id	= Input::get('user_id');//$input['id']; //modificado para não dar merda no explode

		if (!$validation) {
			return Redirect::to_route('admin_edit_user',array($user_id))->with('errors',User::errors_to_string());
		}else{
			$user = new User;
			$user->save_user($input,$user_id,null);

			return Redirect::to_route('admin_edit_user',array($user_id))
						   ->with('success','Usuário alterado com sucesso!');
		}
	}
}