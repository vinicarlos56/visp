<?php 

class Admin_Pages_Controller extends Admin_Base_Controller
{
	public $restful = true;

	public function get_index(){
		$pages = Page::all();
		return $this->layout->nest('content','admin::pages.index', array('pages'=>$pages));
	}

	public function get_edit($page_id){
		$page = Page::find($page_id);
		return $this->layout->nest('content','admin::pages.edit',array('page'=>$page));
	}

	public function put_update(){
		$page_id = Input::get('page_id');
		$content = Input::get('content');

		Page::update($page_id,array('content'=>$content));

		return $this->get_index();
	}

}
