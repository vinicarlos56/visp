<?php 

class Admin_Home_Controller extends Admin_Base_Controller
{
	public $restful = true;

	public function __construct(){
		parent::__construct();

		$this->filter('before', 'admin_auth')->only('index');
	}
	
	public function get_index(){
		$products = Product::where('status','=','0')->get();
		return $this->layout->nest('content','admin::home.index',array('products'=>$products));
	}

	public function get_login(){
		if(Authority::can('manage','User'))
			return Redirect::to_route('admin_home');
		else
			return View::make('admin::home.login');
	}

	public function get_logout(){
		Auth::logout();
    	return Redirect::to_route('admin_login');
	}

	public function post_login(){
		// get POST data
	    $userdata = array(
	        'username'      => Input::get('login'),
	        'password'      => Input::get('senha')
	    );

	    if(Auth::attempt($userdata)){      
	        return Redirect::to('admin');
	    }else{	        
	        return Redirect::to_route('admin_login')
	            ->with('login_errors', true);
	    }
	}
	
}