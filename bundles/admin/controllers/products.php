<?php 

class Admin_Products_Controller extends Admin_Base_Controller
{
	public $restful = true;

	public function get_index()
	{
		$products = Product::all();
		return $this->layout->nest('content','admin::products.index',array('products'=>$products));
	}

	public function post_release($product_id){
		
		$json_response['status'] = 0;

		$product = Product::find($product_id);
		$product->status = 1;
		if($product->save()){
			$json_response['status'] = 1;
		}else{
			$json_response['errors'] = 'Não foi possível liberar o produto.';
		}

		return json_encode($json_response);
	}
}