<?php
//Route::get('(:bundle)','admin::home@index');
// Route::controller('admin::home');


// Route::get('(:bundle)/teste',function(){
// 	return Hash::make('1234');
// });

Route::get('(:bundle)',array('as'=>'admin_home','uses'=>'admin::home@index'));
Route::get('(:bundle)/gerenciar-paginas', array('as'=>'gerenciar-paginas','uses'=>'admin::pages@index'));
Route::get('(:bundle)/pages/(:num)/edit', array('as'=>'edit-page','uses'=>'admin::pages@edit'));
// TODO: Refatorar realizar o login em outro controller
Route::get('(:bundle)/login',array('as'=>'admin_login','uses'=>'admin::home@login'));
Route::get('(:bundle)/logout',array('as'=>'admin_logout','uses'=>'admin::home@logout'));
Route::get('(:bundle)/banners',array('as'=>'banners','uses'=>'admin::banners@index'));
Route::get('(:bundle)/banners/new',array('as'=>'new_banner','uses'=>'admin::banners@new'));
Route::get('(:bundle)/produtos',array('uses'=>'admin::products@index'));
Route::get('(:bundle)/usuarios',array('uses'=>'admin::users@index'));
Route::get('(:bundle)/usuarios/(:num)/editar',array('as'=>'admin_edit_user','uses'=>'admin::users@edit'));

// FIXME: refatorar usar o http delete
Route::get('(:bundle)/banner/(:num)/delete',array('uses'=>'admin::banners@destroy'));



Route::post('(:bundle)/login', array('before'=>'csrf','uses'=>'admin::home@login'));
Route::post('(:bundle)/banners', array('before'=>'csrf','uses'=>'admin::banners@create'));
Route::post('(:bundle)/product/(:num)/release', array('uses'=>'admin::products@release'));

Route::put('(:bundle)/page',array('before'=>'csrf','uses'=>'admin::pages@update'));
Route::put('(:bundle)/users/update',array('before'=>'csrf','uses'=>'admin::users@update'));


Route::filter('admin_auth', function()
{
	if (Authority::cannot('manage','User')) return Redirect::to('admin/login');
});