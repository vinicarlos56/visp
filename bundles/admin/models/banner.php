<?php 

class Banner extends Base_Model{
	public static $timestamps = true;
	
	public function photo(){
		return $this->has_one('Photo','model_id');
	}
}